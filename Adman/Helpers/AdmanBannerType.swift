//
//  AdmanBannerType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

public enum AdmanBannerType: Int {
    case t320x480 = 0,
         t320x50
}
