//
//  VACNetworkManager.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

var AdmanRecognitionUrl: String?

import Foundation

class VACNetworkManager: NSObject, URLSessionWebSocketDelegate, URLSessionTaskDelegate {

    var delegate: VACNetworkManagerDelegate?
    var headerSent = false
    var connectionOptions: [String: Any]?
    var connection: URLSessionWebSocketTask?
    var isConnectionInitialized = false
    var audioMessage: URLSessionWebSocketTask.Message?
    
    static func setWebSocketUrl(url: String) {
        AdmanRecognitionUrl = url
    }
    
    static func getWebSocketUrl() -> String {
        return AdmanRecognitionUrl ?? ""
    }
    
    func openConnection(options: [String: Any]) {
        print("voice server url: " + (AdmanRecognitionUrl ?? ""))
        guard let AdmanRecognitionUrl = AdmanRecognitionUrl else { return }
        var request = URLRequest(url: URL(string: AdmanRecognitionUrl)!)
        if #available(iOS 12.0, *) { request.networkServiceType = .background }
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        connection = urlSession.webSocketTask(with: request)
        headerSent = false
        connectionOptions = options
        connection!.resume()
    }
    
    func webSocketDidOpen() {
        guard let connectionOptions = connectionOptions else { return }
        let siteid = String(connectionOptions["site_id"] as! Int)
        let vad = String((connectionOptions["vad"] as! Bool) ? 1 : 0)
        let requestData = "{\"partner_id\": 1, \"site_id\": " + siteid + ", \"ad_id\": \"" + String(connectionOptions["ad_id"] as! String) + "\", \"response_delay\": " + String(connectionOptions["response_delay"] as? Int ?? -1) + ", \"device_info\": \"" + String(connectionOptions["device_info"] as! String) + "\", \"idfa\": \"" + String(connectionOptions["idfa"] as! String) + "\", \"vad\": " + vad + "}"
        let msgData = toMessage(msg: "content-type: application/json\r\npath: request\r\n\r\n", samples: requestData.data(using: .utf8))
        let message = URLSessionWebSocketTask.Message.data(msgData)
        connection?.send(message) { [self] error in
            print("Sending request message, body: {" + requestData + "}")
            readMessage()
        }
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        webSocketDidOpen()
    }
    
    func readMessage()  {
        connection?.receive { [self] result in
            readMessage()
            isConnectionInitialized = true
            audioMessage = nil
            switch result {
            case .failure(let error):
                if connection?.state == .running {
                    delegate?.networkManagerDidFail(manager: self, error: error as NSError)
                }
            case .success(let message):
                switch message {
                case .string(let text):
                    self.receivedMessage(message: text)
                case .data(let data):
                    print("Received binary message: \(data)")
                @unknown default:
                    fatalError()
                }
            }
            if connection?.state == .canceling {}
        }
    }
        
    func closeConnection() {
        connection?.cancel(with: .normalClosure, reason: nil)
        connection = nil
        headerSent = false
        isConnectionInitialized = false
    }

    func isOpen() -> Bool {
        if connection?.state == .canceling || connection?.state == .completed || connection?.state == .suspended || connection?.state == .none { return false }
        return true
    }
    
    func send(data: Data) {
        let dataMessage = URLSessionWebSocketTask.Message.data(data)
        if isConnectionInitialized {
            connection?.send(dataMessage, completionHandler: { _ in })
        }
    }
    
    func sendData(data: Data) {
        audioMessage = URLSessionWebSocketTask.Message.data(toMessage(msg: "content-type: audio/wav\r\npath: audio.data\r\n", samples: data))
        if isConnectionInitialized && headerSent {
            connection?.send(audioMessage!, completionHandler: { _ in })
        }
    }
    
    func toMessage(msg: String, samples: Data?) -> Data {
        //print("toMessage: " + msg)
        var request = Data()
        let mSize = msg.data(using: .utf8)?.count
        let bSize: [UInt8] = [UInt8((mSize! >> 8) & 0xff), UInt8(mSize! & 0xff)]
        request.append(contentsOf: bSize)
        request.append(msg.data(using: .utf8)!)
        if let samples = samples { request.append(samples) }
        return request
    }
    
    func receivedMessage(message: String) {
        let result = message.components(separatedBy: "\r\n\r\n")
        var headers = [String: Any]()
        for header in result[0].components(separatedBy: "\r\n") {
            let list = header.components(separatedBy: ":")
            headers[list[0].lowercased()] = list[1].trimmingCharacters(in:CharacterSet.whitespaces)
        }
        if headers["path"] as! String == "audio.start" {
            print("Send wav header to server")
            let message = URLSessionWebSocketTask.Message.data(toMessage(msg: "content-type: audio/wav\r\npath: audio.data\r\n", samples: VACRecorder.wavHeader()))
            connection?.send(message, completionHandler: { [self] error in
                headerSent = true
            })
        } else if headers["path"] as! String == "audio.stop" {
            delegate?.networkManagerCommandReceived(manager: self, command: headers["path"] as! String)
        } else if headers["content-type"] as? String == "application/json" || headers["content-type"] as? String == "text/json" {
            let obj = try? JSONSerialization.jsonObject(with: result[1].data(using: .utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if headers["path"] as! String == "voice.transcript" && (obj as! [String: Any])["transcript"] != nil {
                if let obj = (obj as? [String: String]),
                   let tr = obj["transcript"],
                   tr.count > 0 {
                    delegate?.networkManagerResponseReceived(manager: self, response: obj)
                }
            } else if headers["path"] as! String == "voice.result" {
                delegate?.networkManagerSessionEnded(manager: self, response: obj as! [String : Any])
                closeConnection()
            } else if headers["path"] as! String == "error" {
                delegate?.networkManagerDidFail(manager: self, error: NSError(domain: NetService.errorDomain, code: NSURLErrorCannotDecodeRawData, userInfo: obj as? [String : Any]))
                closeConnection()
            }
        }
    }
    /*
    - (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(nullable NSString *)reason wasClean:(BOOL)wasClean {
        NSLog(@"Connection closed with reason %@", reason);
    }
*/
}
