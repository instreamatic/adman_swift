//
//  VACRecorderDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 21/02/2022.
//

import Foundation

protocol VACRecorderDelegate {
    func processSampleWithCapacity(audioDataBytesCapacity: UInt32, audioData: NSData)
}
