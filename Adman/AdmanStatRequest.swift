//
//  AdmanStatRequest.swift
//  Adman_swift
//
//  Created by Barnet Birday on 19/01/2022.
//

import Foundation

class AdmanStatRequest: NSObject {
    
    var onComplete: (() -> ())?
    var onFail: ((Error) -> ())?
    var links = [String]()
    var isReported = false
    
    override init() { links = [String]() }
    
    init(URIs: [String]) {
        super.init()
        links = URIs
    }
    
    func addURL(URL: String) {
        links.append(URL)
    }
    
    func report() {
        if !isReported {
            for link in links {
                request(url: link)
            }
        }
    }
    
    func request(url: String?) {
        guard let url = url else { return }
        if isReported { return }
        isReported = true
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = TIMEOUT_INTERVAL_STATS_REQUEST
        var request = URLRequest(url: URL(string: url)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = TIMEOUT_INTERVAL_STATS_REQUEST
        let session = URLSession(configuration: config)
        
        let dataTask = session.dataTask(with: request) { [weak self] data, response, error in
            if data != nil {
                self?.onComplete?();
            } else if let error = error {
                self?.onFail?(error)
            }
        }
        dataTask.resume()
    }
}
