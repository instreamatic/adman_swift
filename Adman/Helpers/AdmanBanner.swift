//
//  AdmanBanner.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation
import UIKit

open class AdmanBanner: NSObject {
    var adId: String?
    var adSystem: String?
    var expirationTime = AD_EXPIRATION_TIME_DEFAULT
    var sourceUrl: String?
    var introUrl: String?
    var bitrate: Int?
    var duration: Int?
    var minVolume = 0
    var adText: String?
    var url: String?
    var urlToNavigateOnClick: String?
    var media = [AdmanMediaFile]()
    var companionAds = [String: AdmanCreative]()
    open var bannerImage: UIImage?
    var adCache: Data?
    var statData = AdmanStats()
    var responses = [String: [AdmanInteraction]]()
    var responseTime = 0
    open var assets = [String: Any]()
    var showControls = false
    var isClickable = true
    var companion: Companion?
    var trackingEvents = [TrackingEvent]()
    var mediaFiles: [MediaFile]?
    var videoClicks: VideoClicks?
    var extensionResponses = [Response]()
    var responseUrls = [ResponseUrl]()
    
    override init() { super.init() }
    
    func isVoiceAd() -> Bool {
        return responseTime > 1
    }
}
