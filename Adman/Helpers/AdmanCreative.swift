//
//  AdmanCreative.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

struct AdmanCreative {
    var tracking: [String: [String]]?
    var staticResource: String?
    var urlToNavigateOnClick: String?
}
