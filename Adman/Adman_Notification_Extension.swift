//
//  Adman_Notifications.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

extension Adman {
    
    @objc func triggerActionAdmanControl(notification: NSNotification) {
        if state == .readyForPlayback || state == .error || state == .adNone || state == .playbackCompleted || state == .initial {
            return
        }
        let dict: [AnyHashable: Any]? = notification.userInfo
        let message = dict?[NOTIFICATION_ADMAN_CONTROL_KEY] as? NotificationAdmanControl
        guard let message = message else { return }
        
        print(TAG + " triggerActionAdmanControl: state: " + NotificationAdmanControl.getEventString(event: message.event ?? .prepare))
        switch message.event! {
        case .prepare:
            break
        case .start:
            resume()
        case .pause:
            pause()
        case .resume:
            rewind()
        case .skip:
            viewClosed()
        case .click:
            bannerTouched(urlToNavigate: adsList[activeAd].urlToNavigateOnClick)
        case .swipeBack:
            stop()
        case .positive:
            intentTapped = true
            positiveIntent()
        case .negative:
            intentTapped = true
            negativeIntent()
        default:
            break
        }
    }
    
    func positiveIntent() {
        adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventResponseOpenButton)
        processIntent(dict: ["transcript": "Yes", "action": "positive", "playMicOff": "No", "source": "ui"])
    }

    func negativeIntent() {
        adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventResponseNegativeButton)
        processIntent(dict: ["transcript": "No", "action": "negative", "playMicOff": "No", "source": "ui"])
    }
    
    @objc func triggerActionAdmanPlayer(notification: NSNotification) {
        if state == .readyForPlayback || state == .error || state == .adNone || state == .playbackCompleted || state == .initial {
            return
        }
        
        let dict: [AnyHashable: Any]? = notification.userInfo
        let message = dict?[NOTIFICATION_ADMAN_PLAYER_KEY]
        guard let message = message else {
            log(logString: "Warning. Message of triggerActionAdmanPlayer is nil")
            return
        }
        guard let message = message as? NotificationAdmanPlayer else { return }
        switch (message.event) {
        case .ready:
            NotificationAdmanBase.sendEvent(event: .ready)
        case .complete:
            NotificationAdmanBase.sendEvent(event: .completed)
        default:
            break;
        }
    }
}
