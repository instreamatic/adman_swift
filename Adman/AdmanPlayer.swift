//
//  AdmanPlayer.swift
//  Adman_swift
//
//  Created by Barnet Birday on 19/01/2022.
//

import AVKit
import AVFAudio

typealias AdmanOnCompleteType = ((NSObject?) -> ())?
typealias AdmanOnFailType = ((NSErrorPointer?) -> ())?
typealias AdmanOnFail = ((Error?) -> ())?

open class AdmanPlayer: AVPlayer, AVAudioPlayerDelegate {
    var delegate: AdmanPlayerDelegate?
    var userData: Any?
    var isPlaying = false
    var currentUrl: String?
    var isRotate: Bool?
    var media = [AdmanMediaFile]()
    var sourceURLPortrait: String?
    var sourceURLLandscape: String?
    var currentStatus = AVPlayer.TimeControlStatus.paused
    var currentMediaIndex: Int?
    var isSubtitles: Bool?
    open var outroImageP: UIImage?
    open var outroImageL: UIImage?
    open var introImageP: UIImage?
    open var introImageL: UIImage?
    var currentStepID: String?
    var curTime: CMTime?
    var adID: Int?
    var itemType: Int?
    var isObserved = false
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(audioInterruptionSel), name: NSNotification.Name(AVAudioSession.interruptionNotification.rawValue), object: nil)
        currentMediaIndex = 0
        adID = 0
        itemType = 0
        isRotate = false
        isSubtitles = true
        //isClosedCaptionDisplayEnabled = true
        appliesMediaSelectionCriteriaAutomatically = true
        automaticallyWaitsToMinimizeStalling = false
        addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    func preloadAsset(url: String, onComplete: AdmanOnCompleteType, onFail: AdmanOnFailType) {
        DispatchQueue.global().async {
            let asset = AVURLAsset(url: URL(string: url)!)
            let keys = [playable_STRING, tracks_STRING, duration_STRING]
            asset.loadValuesAsynchronously(forKeys: keys) {
                for thisKey in keys {
                    let error = NSErrorPointer(nil)
                    let keyStatus = asset.statusOfValue(forKey: thisKey, error: error)
                    if keyStatus == AVKeyValueStatus.failed {
                        DispatchQueue.main.sync {
                            if thisKey == keys.last { onFail?(error) }
                        }
                    } else {
                        DispatchQueue.main.sync {
                            if thisKey == keys.last {
                                onComplete?(asset) }
                        }
                    }
                }
            }
        }
    }
    
    func setPreloadMode(mode: AdmanPreloadMode) { preloadMode = mode }
    
    func getPreloadMode() -> AdmanPreloadMode { return preloadMode }
    
    @objc func playbackFinished(notification: NSNotification?) {
        if (notification?.object as? AdmanPlayerItem) == currentItem {
            removeStateListeners()
            NotificationCenter.default.removeObserver(currentItem as Any)
            if observationInfo != nil {
                NotificationCenter.default.removeObserver(self)
            }
            itemPlayed()
        }
    }
    
    func removeAllItems() {
        removeStateListeners()
        NotificationCenter.default.removeObserver(currentItem as Any)
        if observationInfo != nil {
            NotificationCenter.default.removeObserver(self)
        }
        replaceCurrentItem(with: nil)
    }
    
    func addAsset(asset: AVAsset, adID: Int, itemType: AdmanPlayerItemType) {
        let playerItem = AdmanPlayerItem(asset: asset, adID: adID, itemType: itemType)
        
        if let assetURL = playerItem.asset as? AVURLAsset {
            if assetURL.url.description.hasSuffix("m3u8") {
                if let group = playerItem.asset.mediaSelectionGroup(forMediaCharacteristic: AVMediaCharacteristic.legible) {
                    let option: AVMediaSelectionOption? = group.options.first
                    let key: Bool = UIDevice.current.orientation.isPortrait ? true : false
                    let rule: AVTextStyleRule = AVTextStyleRule(textMarkupAttributes: [String(kCMTextMarkupAttribute_OrthogonalLinePositionPercentageRelativeToWritingDirection): (key ? 77 : 60)])!
                    playerItem.textStyleRules = [rule]
                    if option != nil && isSubtitles ?? false {
                        playerItem.select(option, in: group)
                    } else { playerItem.select(nil, in: group) }
                }
            }
        }
        playerItem.preferredForwardBufferDuration = 5
            automaticallyWaitsToMinimizeStalling = false
        playerItem.addObserver(self, forKeyPath: status_STRING, options: NSKeyValueObservingOptions.new, context: nil)
        playerItem.addObserver(self, forKeyPath: playbackBufferFull_STRING, options: NSKeyValueObservingOptions.new, context: nil)
        playerItem.addObserver(self, forKeyPath: playbackLikelyToKeepUp_STRING, options: NSKeyValueObservingOptions.new, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playbackFinished), name: NSNotification.Name(NSNotification.Name.AVPlayerItemDidPlayToEndTime.rawValue), object: nil)
        isObserved = true
        replaceCurrentItem(with: playerItem)
        if isPlaying && timeControlStatus != .paused { //&& currentStatus != .paused {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.play()
            }
        }
        if itemType != .micOff && itemType != .micOn {
            delegate?.log(data: "Buffering " + (playerItem.asset as! AVURLAsset).url.absoluteString)
        }
    }
    
    func addUrl(url: String, adID: Int, itemType: AdmanPlayerItemType) {
        addAsset(asset: AVAsset(url: URL(string: url)!), adID: adID, itemType: itemType)
    }
    
    func addUrl(url: String, adID: Int, itemType: AdmanPlayerItemType, isRecognize: Bool) {
        DispatchQueue.main.async {
            if let sourceURL = self.getCurrentUrl(url: url, itemType: itemType, isRecognize: isRecognize) {
                let asset = AVAsset(url: URL(string: sourceURL)!)
                self.adID = adID
                self.itemType = itemType.rawValue
                self.addAsset(asset: asset, adID: adID, itemType: itemType)
            } else {
                self.addAsset(asset: AVAsset(url: URL(string: url)!), adID: adID, itemType: itemType)
            }
        }
    }
    
    func getCurrentUrl(url: String, itemType: AdmanPlayerItemType, isRecognize: Bool) -> String? {
        isRotate = false
        var stepId: String? = nil
        for item in media { if item.url == url { stepId = item.stepId } }
        
        for item in media {
            if item.stepId == stepId {
                if item.orientation == "landscape" {
                    sourceURLLandscape = item.url
                    if let outro = item.outro {
                        getOutroImageL(url: outro)
                    }
                    if let intro = item.intro {
                        getIntroImageL(url: intro)
                    }
                } else if item.orientation == "portrait" {
                    sourceURLPortrait = item.url
                    if let outro = item.outro {
                        getOutroImageP(url: outro)
                    }
                    if let intro = item.intro {
                        getIntroImageP(url: intro)
                    }
                }
            }
        }
        
        if UIDevice.current.orientation.isLandscape {
            currentUrl = sourceURLLandscape
        } else {
            currentUrl = sourceURLPortrait
        }
        return currentUrl
    }
    
    func rotationByOrientation(toInterfaceOrientation: UIDeviceOrientation) {
        if let item = currentItem as? AdmanPlayerItem {
            let key = item.itemType == .micOff || item.itemType == .micOn
            if key { return }
            if toInterfaceOrientation.isPortrait { rotateVertical() }
            else { rotateHorizontal() }
        }
    }
    
    func rotateVertical() {
        if currentUrl != sourceURLPortrait && isPlaying {
            isRotate = true
            isPlaying = true
            currentUrl = sourceURLPortrait
            let playerItem = currentItem
            let group = playerItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .legible)!
            playerItem?.select(nil, in: group!)
            currentStatus = timeControlStatus
            let currentTime: CMTime = currentTime()
            curTime = currentTime
            let asset = AVAsset(url: URL(string: currentUrl!)!)
            addAsset(asset: asset, adID: adID!, itemType: AdmanPlayerItemType(rawValue: itemType!) ?? .unknown)
            if currentStatus == .paused {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) { [self] in
                    pause()
                    NotificationAdmanPlayer.sendEvent(event: .pause, userData: self)
                }
            }
            if curTime!.value > 0 {
                seek(to: curTime!, toleranceBefore: .zero, toleranceAfter: .zero)
            }
        }
    }
    
    func rotateHorizontal() {
        if currentUrl != sourceURLPortrait && isPlaying {
            isRotate = true
            isPlaying = true
            currentUrl = sourceURLLandscape
            let playerItem = currentItem
            let group = playerItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .legible)!
            playerItem?.select(nil, in: group!)
            currentStatus = timeControlStatus
            let currentTime: CMTime = currentTime()
            curTime = currentTime
            let asset = AVAsset(url: URL(string: currentUrl!)!)
            addAsset(asset: asset, adID: adID!, itemType: AdmanPlayerItemType(rawValue: itemType!) ?? .unknown)
            if currentStatus == .paused {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) { [self] in
                    pause()
                    NotificationAdmanPlayer.sendEvent(event: .pause, userData: self)
                }
            }
            if curTime!.value > 0 {
                seek(to: curTime!, toleranceBefore: .zero, toleranceAfter: .zero)
            }
        }
    }
    
    @objc func appEnterForeground(note: Notification) {
        if currentStatus == .paused { return }
        if currentStatus == .playing {
            isPlaying = true
            let playerItem = currentItem
            if let group = playerItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .legible) {
                playerItem?.select(nil, in: group)
            }
            if let currentUrl = currentUrl {
                let asset = AVAsset(url: URL(string: currentUrl)!)
                addAsset(asset: asset, adID: adID!, itemType: AdmanPlayerItemType(rawValue: itemType!) ?? .unknown)
            }
            if curTime!.value > 0 {
                seek(to: curTime!)
            }
        }
    }
    
    @objc func appEnterBackground(note: Notification) {
        currentStatus = timeControlStatus
        if timeControlStatus == .playing {
            pause()
            let playerItem = currentItem
            if let group = playerItem?.asset.mediaSelectionGroup(forMediaCharacteristic: .legible) {
                playerItem?.select(nil, in: group)
            }
            let currentTime = currentTime()
            let timeToAdd = CMTimeMakeWithSeconds(1, preferredTimescale: 1)
            curTime = CMTimeAdd(currentTime, timeToAdd)
            replaceCurrentItem(with: nil)
        }
    }
    
    open override func play() {
        guard let currentItem = currentItem else { return }
        let logString = "Playing asset " + (currentItem.asset as! AVURLAsset).url.absoluteString
        delegate?.log(data: logString)
        DispatchQueue.main.async {
            UIApplication.shared.beginReceivingRemoteControlEvents()
        }
        isPlaying = true
        super.play()
    }
    
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if currentItem == nil { return }
        if keyPath == status_STRING {
            var status = AVPlayerItem.Status.unknown
            let statusNumber = change?[NSKeyValueChangeKey.newKey]
            if let statusNumber = statusNumber as? Int {
                status = AVPlayerItem.Status.init(rawValue: statusNumber)!
            }
            
            let item = currentItem as! AdmanPlayerItem
            //let status: AVPlayer.Status
            //if let statusNumber = change?[.newKey] as? NSNumber {
            //    status = AVPlayer.Status(rawValue: statusNumber.intValue)!
            //} else { status = .unknown }
            switch status {
            case .readyToPlay, .unknown:
                if item.itemType == .micOff || item.itemType == .micOn {
                    itemLoaded() }
                else {
                    item.preferredForwardBufferDuration = 0
                    automaticallyWaitsToMinimizeStalling = false
                    super.play()
                }
            case .failed:
                isPlaying = false
                removeStateListeners()
                delegate?.itemPlaybackDidFail(adID: item.adID!, itemType: item.itemType!, err: item.error)
                delegate?.log(data: "admanPlayer: failed to load audio")
            default:
                break
            }
        } else if keyPath == "playbackBufferFull" || keyPath == "playbackLikelyToKeepUp" {
            let item = currentItem as! AdmanPlayerItem
            if item.itemType == .micOff || item.itemType == .micOn { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                super.pause()
            }
            itemLoaded()
        } else if keyPath == "rate" {
            if let rate = change?[NSKeyValueChangeKey.newKey] as? Float {
                let item = currentItem as! AdmanPlayerItem
                let key = item.itemType == .micOff || item.itemType == .micOn
                if rate == 0.0 && timeControlStatus == .playing && !key {
                    delegate?.timeControlStatusWaitingToPlayAtSpecifiedRate(state: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.play()
                    }
                } else {
                    delegate?.timeControlStatusWaitingToPlayAtSpecifiedRate(state: false)
                }
            }
        }
    }
    
    @objc func audioInterruptionSel(notification: NSNotification) {
        guard currentItem != nil else { return }
        guard isPlaying == true else { return }
        let interruptionType: AVAudioSession.InterruptionType = notification.userInfo![AVAudioSessionInterruptionTypeKey] as! AVAudioSession.InterruptionType
        switch interruptionType {
        case AVAudioSession.InterruptionType.began:
            super.pause()
        case AVAudioSession.InterruptionType.ended:
            if notification.userInfo![AVAudioSessionInterruptionOptionKey] as! AVAudioSession.InterruptionOptions == AVAudioSession.InterruptionOptions.shouldResume {
                super.play()
            }
        default:
            break
        }
    }
    
    func removeStateListeners() {
        isPlaying = false
        guard let currentItem = currentItem,
              currentItem.observationInfo != nil else { return }
        
        if !isObserved { return }
        currentItem.removeObserver(self, forKeyPath: status_STRING)
        currentItem.removeObserver(self, forKeyPath: playbackLikelyToKeepUp_STRING)
        currentItem.removeObserver(self, forKeyPath: playbackBufferFull_STRING)
        isObserved = false
    }
    
    func itemLoaded() {
        let item: AdmanPlayerItem = currentItem as! AdmanPlayerItem
        if !item.isLoaded {
            item.isLoaded = true
            removeStateListeners()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                delegate?.itemDidLoad(adID: item.adID!, itemType: item.itemType!)
            }
        }
    }
    
    func itemPlayed() {
        if let item = currentItem as? AdmanPlayerItem {
            if !item.isPlayed {
                item.isPlayed = true
                isPlaying = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                    delegate?.itemPlaybackDidFinish(adID: item.adID!, itemType: item.itemType!)
                }
            }
        }
    }
       /*
    func subtitlesPresent() -> Bool {
        if let playerItem = currentItem {
                let group = playerItem.asset.mediaSelectionGroup(forMediaCharacteristic: .legible)
                return (group?.options.first != nil)
        }
        return false
    }
    */
    func getOutroImageP(url: String) {
        DispatchQueue.global(qos: .background).async {
            guard let data = try? Data(contentsOf: URL(string: url)!) else { return }
            DispatchQueue.main.async {
                self.outroImageP = UIImage(data: data)
            }
        }
    }
    
    func getOutroImageL(url: String) {
        DispatchQueue.global(qos: .background).async {
            guard let data = try? Data(contentsOf: URL(string: url)!) else { return }
            DispatchQueue.main.async {
                self.outroImageL = UIImage(data: data)
            }
        }
    }
    
    func getIntroImageP(url: String) {
        DispatchQueue.global(qos: .background).async {
            guard let data = try? Data(contentsOf: URL(string: url)!) else { return }
            DispatchQueue.main.async {
                self.introImageP = UIImage(data: data)
                NotificationAdmanPlayer.sendEvent(event: .loadImage, userData: self)
            }
        }
    }
    
    func getIntroImageL(url: String) {
        DispatchQueue.global(qos: .background).async {
            guard let data = try? Data(contentsOf: URL(string: url)!) else { return }
            DispatchQueue.main.async {
                self.introImageL = UIImage(data: data)
            }
        }
    }
}
