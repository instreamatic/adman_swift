//
//  AdmanRecognitionBackend.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

public enum AdmanRecognitionBackend: Int {
    case auto = 0,
         yandex,
         microsoft,
         nuanceDev,
         houndify,
         nuanceRelease
}
