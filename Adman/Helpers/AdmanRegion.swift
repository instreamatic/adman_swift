//
//  AdmanRegion.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

public enum AdmanRegion: Int {
    case EU = 0,
         EUWest,
         US,
         global,
         demo,
         demoEu,
         voice,
         India,
         custom
}

func getServerStatForRegion(region: AdmanRegion) -> String {
    switch region {
    case .global:
        return AdmanRegionGlobal_STR
    case .voice:
        return AdmanRegionVoice_STR
    case .demo:
        return AdmanRegionDemo_STR
    default:
        return AdmanRegionOther_STR
    }
}

func getServerForRegion(region: AdmanRegion, admanProperties: AdmanProperties) -> String {
    switch (region) {
    case .global:
        return "x3." + INSTREAMATIC_SERVER
    case .voice:
        return "voice." + INSTREAMATIC_SERVER
    case .demo:
        return "d1." + INSTREAMATIC_SERVER
    case .demoEu:
        return "test." + INSTREAMATIC_SERVER
    case .India:
        return "x1-india." + INSTREAMATIC_SERVER
    case .US:
        return "d2975e0yphvxch.cloudfront.net"
    case .custom:
        return admanProperties.customAdServer ?? ("x3." + INSTREAMATIC_SERVER)
    case .EU:
        return "x." + INSTREAMATIC_SERVER
    case .EUWest:
        return "x-eu." + INSTREAMATIC_SERVER
    }
}

func getVoiceServer(region: AdmanRegion, location: AdmanRecognitionBackendLocation) -> String {
    switch (location) {
    case .EU:
        return getVoiceServer(region: .EU)
    case .USA:
        return getVoiceServer(region: .global)
    case .demo:
        return getVoiceServer(region: .demo)
        default:
            return getVoiceServer(region: region)
    }
}

func getVoiceServer(region: AdmanRegion) -> String {
    switch (region) {
    case .global:
        return "wss://v3.instreamatic.com"
    case .voice:
            return "wss://v3.instreamatic.com"
    case .demo:
            return "wss://voice.instreamatic.com"
        default:
            return "wss://v2.instreamatic.com"
    }
}

func getRecognitionBackend(backend: AdmanRecognitionBackend) -> String {
    switch (backend) {
    case .nuanceDev:
        return "/nuance"
    case .nuanceRelease:
        return "/nuance/prod"
    case .houndify:
        return "/houndify"
    case .yandex:
        return "/yandex"
    case .microsoft:
        return "/microsoft"
    case .auto:
        return "";
    }
}
