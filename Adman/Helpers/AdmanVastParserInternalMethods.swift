//
//  AdmanVastParserMethods.swift
//  Adman_swift
//
//  Created by Barnet Birday on 22/01/2022.
//

import Foundation
import UIKit
/*
extension AdmanVastParser {
    
    func adSystemInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.adSystem?.text = text
        } else {
            vastElement?.adElement?.inLineElement?.adSystem?.text = text
        }
    }
    
    func trackingInternal(text: String) {
        if isWrapper {
            trackingWrapperInternal(text: text)
        } else {
            trackingInLineInternal(text: text)
        }
    }
    
    func trackingWrapperInternal(text: String) {
        switch linearNonLinearCompanion {
        case LinearNonLinearCompanion.Linear:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.trackingEvents[trackingLinearLastElement].text = text
        case LinearNonLinearCompanion.NonLinearAds:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.trackingEvents[trackingNonLinearLastElement].text = text
        case LinearNonLinearCompanion.CompanionAds:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].trackingEvents[trackingCompanionLastElement].text = text
        }
    }
    
    func trackingInLineInternal(text: String) {
        switch linearNonLinearCompanion {
        case LinearNonLinearCompanion.Linear:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.trackingEvents[trackingLinearLastElement].text = text
        case LinearNonLinearCompanion.NonLinearAds:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.trackingEvents[trackingNonLinearLastElement].text = text
        case LinearNonLinearCompanion.CompanionAds:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].trackingEvents[trackingCompanionLastElement].text = text
        }
    }
    
    func mediaFileInternal(text: String) {
        let mediaFilesCount = vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.mediaFiles?.count ?? 1
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.mediaFiles?[mediaFilesCount-1].text = text
    }
    
    func staticResourceInternal(text: String) {
        if isWrapper {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].staticResource?.text = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].staticResource?.text = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].staticResource?.text = text
            }
        } else {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].staticResource?.text = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].staticResource?.text = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].staticResource?.text = text
            }
        }
    }
    
    func adTitleInternal(text: String) {
        vastElement?.adElement?.inLineElement?.adTitle = text
    }
    
    func vASTAdTagURIInternal(text: String) {
        vastElement?.adElement?.wrapperElement?.VASTAdTagURI = text
    }
    
    func durationInternal(text: String) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.duration = text
    }
    
    func errorInternal(text: String) {
        if vastElement?.adElement == nil {
            vastElement?.errorElement = text
        } else if isWrapper {
            vastElement?.adElement?.wrapperElement?.errorElement = text
        } else {
            vastElement?.adElement?.inLineElement?.errorElement = text
        }
    }
    
    func iFrameResourceInternal(text: String) {
        if isWrapper {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].iFrameResource = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].iFrameResource = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].iFrameResource = text
            }
        } else {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].iFrameResource = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].iFrameResource = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].iFrameResource = text
            }
        }
    }
    
    func htmlResourceInternal(text: String) {
        if isWrapper {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].htmlResource = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].hTMLResource = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].hTMLResource = text
            }
        } else {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].htmlResource = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].hTMLResource = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].hTMLResource = text
            }
        }
    }
    
    func creativeExtensionInternal(text: String) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].creativeExtensions.append(CreativeExtension(text: text))
    }
    
    func companionClickTrackingInternal(text: String) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].companionClickTracking?.text = text
    }
    
    func companionClickThroughInternal(text: String) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].companionClickThrough = text
    }
    
    func extensionInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].text = text
        } else {
            vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].text = text
        }
    }
    
    func customClickInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.videoClicks?.customClick?.text = text
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks?.customClick?.text = text
        }
    }
    
    func clickTrackingInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.videoClicks?.clickTracking?.text = text
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks?.clickTracking?.text = text
        }
    }
    
    func clickThroughInternal(text: String) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks?.clickTracking?.text = text
    }
    
    func admanValueInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].responses?[responsesLastElement].admanValues?[admanValuesLastElement].text = text
        } else {
            vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].responses?[responsesLastElement].admanValues?[admanValuesLastElement].text = text
        }
    }
    
    func pricingInternal(text: String) {
        vastElement?.adElement?.inLineElement?.pricing?.text = text
    }
    
    func impressionInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.impression?.text = text
        } else {
            vastElement?.adElement?.inLineElement?.impression?.text = text
        }
    }
    
    func adParametersInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].adParameters?.text = text
        } else {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.adParameters?.text = text
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].adParameters?.text = text
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].adParameters?.text = text
            }
        }
    }
    
    func nonLinearClickTrackingInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].nonLinearClickTracking?.text = text
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].nonLinearClickTracking?.text = text
        }
    }
    
    func iconsInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons = [Icon]()
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons = [Icon]()
        }
    }
    
    func iconInternal(text: String) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].text = text
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].text = text
        }
    }
}
*/
