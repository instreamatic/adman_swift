//
//  AdmanVastParserMethods.swift
//  Adman_swift
//
//  Created by Barnet Birday on 22/01/2022.
//

import Foundation
import UIKit
/*
extension AdmanVastParser {
    
    func vastMethod(attributeDict: [String: String]) {
        vastElement = VastElement(version: attributeDict[version_STRING])
    }
    
    func adSystem(attributeDict: [String: String]) {
        let adSystem = AdSystem(version: attributeDict[version_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.adSystem = adSystem
        } else {
            vastElement?.adElement?.inLineElement?.adSystem = adSystem
        }
    }
    
    func inLineMethod() {
        vastElement?.adElement?.inLineElement = InLineElement()
        isWrapper = false
    }
    
    func wrapperMethod() {
        vastElement?.adElement?.wrapperElement = WrapperElement()
        isWrapper = true
    }
    
    func adMethod(attributeDict: [String: String]) {
        vastElement?.adElement = AdElement(idElement: attributeDict[id_STRING], sequenceElement: attributeDict[sequence_STRING])
    }
    
    func creatives() {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives = [CreativeElement]()
        } else {
            vastElement?.adElement?.inLineElement?.creatives = [CreativeElement]()
        }
    }
    
    func creative(attributeDict: [String: String]) {
        let creative = CreativeElement(id: attributeDict[id_STRING], sequence: attributeDict[sequence_STRING], adID: attributeDict[adID_STRING], apiFramework: attributeDict[apiFramework_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?.append(creative)
        } else {
            vastElement?.adElement?.inLineElement?.creatives?.append(creative)
        }
        creativesLastElement += 1
    }
    
    func linear(attributeDict: [String: String]) {
        let linear = Linear(skipoffset: attributeDict[skipoffset_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear = linear
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear = linear
        }
        linearNonLinearCompanion = linearNonLinearCompanion(elementName: elementName)
    }
    
    func trackingEvents() {
        trackingCompanionLastElement = -1
        trackingLinearLastElement = -1
        trackingNonLinearLastElement = -1
    }
    
    func tracking(attributeDict: [String: String]) {
        if isWrapper {
            trackingWrapper(attributeDict: attributeDict)
        } else {
            trackingInLine(attributeDict: attributeDict)
        }
    }
    
    func trackingWrapper(attributeDict: [String: String]) {
        let trackingEvent = TrackingEvent(event: attributeDict[event_STRING]!)
        switch linearNonLinearCompanion {
        case LinearNonLinearCompanion.Linear:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.trackingEvents.append(trackingEvent)
            trackingLinearLastElement += 1
        case LinearNonLinearCompanion.NonLinearAds:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.trackingEvents.append(trackingEvent)
            trackingNonLinearLastElement += 1
        case LinearNonLinearCompanion.CompanionAds:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].trackingEvents.append(trackingEvent)
            trackingCompanionLastElement += 1
        }
    }
    
    func trackingInLine(attributeDict: [String: String]) {
        let trackingEvent = TrackingEvent(event: attributeDict[event_STRING]!)
        switch linearNonLinearCompanion {
        case LinearNonLinearCompanion.Linear:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.trackingEvents.append(trackingEvent)
            trackingLinearLastElement += 1
        case LinearNonLinearCompanion.NonLinearAds:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.trackingEvents.append(trackingEvent)
            trackingNonLinearLastElement += 1
        case LinearNonLinearCompanion.CompanionAds:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].trackingEvents.append(trackingEvent)
            trackingCompanionLastElement += 1
        }
    }
    
    func videoClicks() {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.videoClicks = VideoClicks()
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks = VideoClicks()
        }
    }
    
    func mediaFiles() {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.mediaFiles = [MediaFile]()
    }
    
    func mediaFile(attributeDict: [String: String]) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.mediaFiles?.append(MediaFile(id: attributeDict[id_STRING], delivery: attributeDict[delivery_STRING], type: attributeDict[type_STRING], bitrate: attributeDict[bitrate_STRING], minBitrate: attributeDict[minBitrate_STRING], maxBitrate: attributeDict[maxBitrate_STRING], codec: attributeDict[codec_STRING], mantainAspectRatio: attributeDict[maintainAspectRatio_STRING], scalable: attributeDict[scalable_STRING], width: attributeDict[width_STRING], height: attributeDict[height_STRING], apiFramework: attributeDict[apiFramework_STRING]))
    }
    
    func staticResource(attributeDict: [String: String]) {
        if isWrapper {
            staticResourceWrapper(attributeDict: attributeDict)
        } else {
            staticResourceInLine(attributeDict: attributeDict)
        }
    }
    
    func staticResourceWrapper(attributeDict: [String: String]) {
        let staticResource = StaticResource(creativeType: attributeDict[creativeType_STRING])
        switch linearNonLinearCompanion {
        case LinearNonLinearCompanion.Linear:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].staticResource = staticResource
        case LinearNonLinearCompanion.NonLinearAds:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].staticResource = staticResource
        case LinearNonLinearCompanion.CompanionAds:
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].staticResource = staticResource
        }
    }
    
    func staticResourceInLine(attributeDict: [String: String]) {
        let staticResource = StaticResource(creativeType: attributeDict[creativeType_STRING])
        switch linearNonLinearCompanion {
        case LinearNonLinearCompanion.Linear:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons?[iconsLastElement].staticResource = staticResource
        case LinearNonLinearCompanion.NonLinearAds:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].staticResource = staticResource
        case LinearNonLinearCompanion.CompanionAds:
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].staticResource = staticResource
        }
    }
    
    func companionAds(attributeDict: [String: String]) {
        let companionAds = CompanionAds(required: attributeDict[required_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds = companionAds
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds = companionAds
        }
        linearNonLinearCompanion = linearNonLinearCompanion(elementName: elementName)
        companionsLastElement = -1
    }
    
    func customClick(attributeDict: [String: String]) {
        let customClick = CustomClick(id: attributeDict[id_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.videoClicks?.customClick = customClick
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks?.customClick = customClick
        }
    }
    
    func clickTracking(attributeDict: [String: String]) {
        let clickTracking = ClickTracking(id: attributeDict[id_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.videoClicks?.clickTracking = clickTracking
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks?.clickTracking = clickTracking
        }
    }
    
    func clickThrough(attributeDict: [String: String]) {
        vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.videoClicks?.clickTracking = ClickTracking(id: attributeDict[id_STRING])
    }
    
    func companion(attributeDict: [String: String]) {
        let companion = Companion(id: attributeDict[id_STRING], width: attributeDict[width_STRING], height: attributeDict[height_STRING], assetWidth: attributeDict[assetWidth_STRING], assetHeight: attributeDict[assetHeight_STRING], expandedWidth: attributeDict[expandedWidth_STRING], expandedHeight: attributeDict[expandedHeight_STRING], apiFramework: attributeDict[apiFramework_STRING], adSlotID: attributeDict[adSlotID_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion.append(companion)
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion.append(companion)
        }
        companionsLastElement += 1
    }
    
    func nonLinear(attributeDict: [String: String]) {
        let nonLinear = NonLinear(id: attributeDict[id_STRING], width: attributeDict[width_STRING], height: attributeDict[height_STRING], expandedWidth: attributeDict[expandedWidth_STRING], expandedHeight: attributeDict[expandedHeight_STRING], scalable: attributeDict[scalable_STRING], maintainAspectRatio: attributeDict[maintainAspectRatio_STRING], minSuggestedDuration: attributeDict[minSuggestedDuration_STRING], apiFramework: attributeDict[apiFramework_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear.append(nonLinear)
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear.append(nonLinear)
        }
        nonLinearLastElement += 1
    }
    
    func responses() {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].responses = [Response]()
        } else {
            vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].responses = [Response]()
        }
    }
    
    func response(attributeDict: [String: String]) {
        let response = Response(action: attributeDict[action_STRING], type: attributeDict[type_STRING], priority: attributeDict[priority_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].responses?.append(response)
        } else {
            vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].responses?.append(response)
        }
        responsesLastElement += 1
    }
    
    func admanExtension(attributeDict: [String: String]) {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.extensions.append(Extension(type: attributeDict[type_STRING]))
        } else {
            vastElement?.adElement?.inLineElement?.extensions.append(Extension(type: attributeDict[type_STRING]))
        }
        extensionsLastElement += 1
    }
    
    func admanValues() {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].responses?[responsesLastElement].admanValues = [AdmanValue]()
        } else {
            vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].responses?[responsesLastElement].admanValues = [AdmanValue]()
        }
        admanValuesLastElement = -1
    }
    
    func admanValue(attributeDict: [String: String]) {
        let admanValue = AdmanValue(type: attributeDict[type_STRING], sequence: attributeDict[sequence_STRING])
        if isWrapper {
            if previousElementName == Extension_STRING {
                vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].admanValue = admanValue
            } else {
                vastElement?.adElement?.wrapperElement?.extensions[extensionsLastElement].responses?[responsesLastElement].admanValues?.append(admanValue)
                admanValuesLastElement += 1
            }
        } else {
            if previousElementName == Extension_STRING {
                vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].admanValue = admanValue
            } else {
                vastElement?.adElement?.inLineElement?.extensions[extensionsLastElement].responses?[responsesLastElement].admanValues?.append(admanValue)
                admanValuesLastElement += 1
            }
        }
    }
    
    func pricing(attributeDict: [String: String]) {
        vastElement?.adElement?.inLineElement?.pricing = Pricing(model: attributeDict[model_STRING], currency: attributeDict[currency_STRING])
    }
    
    func impression(attributeDict: [String: String]) {
        let impression = Impression(idElement: attributeDict[id_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.impression = impression
        } else {
            vastElement?.adElement?.inLineElement?.impression = impression
        }
    }
    
    func nonLinearAds() {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds = NonLinearAds()
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds = NonLinearAds()
        }
        linearNonLinearCompanion = linearNonLinearCompanion(elementName: elementName)
        nonLinearLastElement = -1
    }
    
    func adParameters(attributeDict: [String: String]) {
        let adParameters = AdParameters(xmlEncoded: attributeDict[xmlEncoded_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].adParameters = adParameters
        } else {
            switch linearNonLinearCompanion {
            case LinearNonLinearCompanion.Linear:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.adParameters = adParameters
            case LinearNonLinearCompanion.NonLinearAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].adParameters = adParameters
            case LinearNonLinearCompanion.CompanionAds:
                vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].adParameters = adParameters
            }
        }
    }
    
    func nonLinearClickTracking(attributeDict: [String: String]) {
        let nonLinearClickTracking = NonLinearClickTracking(id: attributeDict[id_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].nonLinearClickTracking = nonLinearClickTracking
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].nonLinearAds?.nonLinear[nonLinearLastElement].nonLinearClickTracking = nonLinearClickTracking
        }
    }
    
    func companionClickTracking(attributeDict: [String: String]) {
        let companionClickTracking = CompanionClickTracking(id: attributeDict[id_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].companionClickTracking = companionClickTracking
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].companionAds?.companion[companionsLastElement].companionClickTracking = companionClickTracking
        }
    }
    
    func icons() {
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons = [Icon]()
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons = [Icon]()
        }
    }
    
    func icon(attributeDict: [String: String]) {
        let icon = Icon(program: attributeDict[program_STRING], width: attributeDict[width_STRING], height: attributeDict[height_STRING], xPosition: attributeDict[xPosition_STRING], yPosition: attributeDict[yPosition_STRING], duration: attributeDict[duration_STRING], offset: attributeDict[offset_STRING], apiFramework: attributeDict[apiFramework_STRING])
        if isWrapper {
            vastElement?.adElement?.wrapperElement?.creatives?[creativesLastElement].linear?.icons?.append(icon)
        } else {
            vastElement?.adElement?.inLineElement?.creatives?[creativesLastElement].linear?.icons?.append(icon)
        }
        iconsLastElement += 1
    }
}
*/
