//
//  ResponseUrl.swift
//  Adman_swift
//
//  Created by Barnet Birday on 21/04/2022.
//

import Foundation

class ResponseUrl: NSObject {
    var responseUrl: String?
    var stepID: String?
    
    init(url: String, stepId: String) {
        super.init()
        responseUrl = url
        self.stepID = stepId
    }
}
