//
//  AdmanUIEndVideoType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 08/04/2022.
//

import Foundation

enum AdmanUIEndVideoType: Int {
    case mediaSilence = 0,
         mediaYes,
         mediaNo
}
