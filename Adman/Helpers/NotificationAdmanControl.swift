//
//  NotificationAdmanControl.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

class NotificationAdmanControl {
    
    var event: AdmanControlEventType?
    var userData: Any?
    
    init(event: AdmanControlEventType, userData: Any?) {
        self.event = event
        self.userData = userData
    }
    
    init() {}
    
    static func sendEvent(event: AdmanControlEventType) {
        sendEvent(event: event, userData: nil)
    }
    
    static func sendEvent(event: AdmanControlEventType, userData: Any?) {
        let message = NotificationAdmanControl(event: event, userData: userData)
        var dict = [String: Any]()
        dict[NOTIFICATION_ADMAN_CONTROL_KEY] = message
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_ADMAN_CONTROL), object: nil, userInfo: dict)
    }
    
    static func getEventString(event: AdmanControlEventType) -> String {
        switch (event) {
        case .prepare:
            return "AdmanControlEventPrepare (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .start:
            return "AdmanControlEventStart (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .pause:
            return "AdmanControlEventPause (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .resume:
            return "AdmanControlEventResume (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .skip:
            return "AdmanControlEventSkip (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .click:
            return "AdmanControlEventClick (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .link:
            return "AdmanControlEventLink (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .swipeBack:
            return "AdmanControlEventSwipeBack (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .positive:
            return "AdmanControlEventPositive (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .negative:
            return "AdmanControlEventNegative (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .subShow:
            return "AdmanControlEventSubShow (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .subHide:
            return "AdmanControlEventSubHide (" + String(AdmanControlEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        }
    }
}
