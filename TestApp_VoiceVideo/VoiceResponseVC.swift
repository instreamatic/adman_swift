//
//  VoiceResponseVC.swift
//  TestApp_VoiceVideo
//
//  Created by Barnet Birday on 03/04/2022.
//

import UIKit
import Adman_swift
import CoreLocation
import AVFoundation

let BUILD_MODE_TRITON = 4004
let BUILD_MODE_FULL = 4005
let BUILD_MODE_VIDEO = 4006
let ADMAN_BUILD_MODE = BUILD_MODE_VIDEO

enum AdmanDemoMode: UInt {
    case simple = 0,
         Default,
         DefaultRUEu,
         PG,
         video
}

class VoiceResponseVC: UIViewController, AdmanDelegate, CLLocationManagerDelegate, AdmanUIExtensionDelegate {
    
    @IBOutlet var launchDemo: UIButton?

    @IBOutlet var introTitle: UILabel?
    @IBOutlet var voiceActionsText: UITextView?
    @IBOutlet var voiceHelp: UITextView?
    @IBOutlet var debugView: UITextView?
    @IBOutlet var debugScrpllView: UIScrollView?
    @IBOutlet var endInteractionLabel: UILabel?

    @IBOutlet var toggleDemo: UIButton?
    @IBOutlet var voiceIntentsView: UIScrollView?
    @IBOutlet var loadingGif: UIImageView?
    
    var mainAdsManager: Adman?
    var admanUI: AdmanUIExtension?
    var demoMode: AdmanDemoMode?
    var activeAdPlayer: Adman?
    var campaigns: [Adman]?
    var demoLang: String?
    var region = AdmanRegion.custom
    
    var loggingTimer: Timer?
    var pauseNextAd: Bool?
    var reaction: String?
    var stuckOnPreload = false
    var lastState: AdmanState?
    
    func initAdmanVideo() {
        mainAdsManager = Adman(siteId: 1985, region: .global, testMode: false)
        mainAdsManager?.setCreativeId(creativeId: 22811)
        //mainAdsManager?.setCreativeId(creativeId: 625)
        //mainAdsManager?.setCampaignId(campaignId: 386)
        mainAdsManager?.delegate = self
        admanUI = AdmanUIExtension()
        admanUI?.delegate = self
        demoMode = .video
    }
    
    func initAdmanFull() {
        region = .demo
        let ads_region = UserDefaults.standard.string(forKey: "ads_region")
        var siteId = 1043
        if ads_region == "us_demo" {
            region = .US
        } else if ads_region == "eu_demo" {
            region = .demoEu
            siteId = 777
        } else if ads_region == "global" { region = .global }
        
        demoMode = .simple
        let demo_mode = UserDefaults.standard.string(forKey: "demo_mode")
        if demo_mode == "default" {
            demoMode = .Default
            if region == .US || region == .global {
                siteId = demoLang == "en" && region == .global ? 1217 : 1043
            } else if demo_mode == "pg" {
                demoMode = .PG
                if region == .demo {
                    siteId = 1044
                }
            }
        }
        if demoLang == "ru" { siteId = 1014 }
        else if demoLang == "fr" { siteId = 1024 }
        else if demoLang == "ja" { siteId = 1039 }
        else if demoLang == "ja_cu" {
            siteId = 1076
            region = .global
            demoLang = "ja"
        }
        if demoMode == .Default && region == .demoEu && demoLang == "ru" {
            demoMode = .DefaultRUEu
            region = .EU
            siteId = 332
        }
        // _mainAdsManager = [Adman sharedManagerWithSiteId:912 region:AdmanRegionEU testMode:FALSE];
        // Initialization example without static object
        mainAdsManager = Adman(siteId: siteId, region: region, testMode: false)
        mainAdsManager?.delegate = self
        admanUI = AdmanUIExtension()
        admanUI?.delegate = self
        // [_adsManager overrideIP:@"45.58.124.166"];
        
        
        
        let backendLocation = UserDefaults.standard.string(forKey: "backend_loc")
        if backendLocation == "usa" {
            mainAdsManager?.setRecognitionBackendLocation(location: .USA)
        } else if backendLocation == "eu" {
            mainAdsManager?.setRecognitionBackendLocation(location: .EU)
        } else if backendLocation == "demo" {
            mainAdsManager?.setRecognitionBackendLocation(location: .demo)
        }
        
        var backend: AdmanRecognitionBackend?
        if UserDefaults.standard.string(forKey: "backend")?.count ?? 0 > 0 {
            let backendInt = UserDefaults.standard.integer(forKey: "backend")
            if let backendConst = AdmanRecognitionBackend(rawValue: backendInt) {
                mainAdsManager?.setRecognitionBackend(backend: backendConst)
                backend = backendConst
            }
        }
        
        var delay: Float?
        if let responseDelay = UserDefaults.standard.string(forKey: "response_delay"),
            let responseFloat = Float(responseDelay) {
            delay = abs(responseFloat)
            if let delay = delay {
                if delay > 0 {
                    mainAdsManager?.setResponseDelay(delay: Int(delay) * (-1))
                    print("Current voice response delay:" + String(delay))
                }
            }
        }
        var timeout: Int?
        if let time = UserDefaults.standard.string(forKey: "server_timeout"),
           let timeOutInt = Int(time) {
            timeout = timeOutInt
            mainAdsManager?.setServerTimeout(delay: timeOutInt)
        }
        let responeTime = UserDefaults.standard.integer(forKey: "response_time")
        mainAdsManager?.setResponseTime(duration: responeTime)
        
        let vad = UserDefaults.standard.bool(forKey: "vad")
        mainAdsManager?.setServerVoiceActivityDetection(state: vad)
            
        mainAdsManager?.setSiteVariable(key: "consent_string", value: "BOEFEAyOEFEAyAHABDENAI4AAAB9vABAASA")
        
        //automaticallyAdjustsScrollViewInsets = false
        
        if demoMode != .simple {
            campaigns = [Adman]()
            for _ in 0...5 {
                campaigns?.append(Adman(siteId: siteId, region: region, testMode: false))
                campaigns?.last?.setRecognitionBackendLocation(location: mainAdsManager!.recognitionBackendLocation())
                if let backend = backend {
                    campaigns?.last?.setRecognitionBackend(backend: backend)
                }
                if let delay = delay {
                    if delay > 0 { campaigns?.last?.setResponseDelay(delay: Int(delay) * (-1)) }
                }
                if let timeout = timeout { campaigns?.last?.setServerTimeout(delay: timeout) }
                campaigns?.last?.setResponseTime(duration: UserDefaults.standard.integer(forKey: "response_time"))
                campaigns?.last?.setServerVoiceActivityDetection(state: UserDefaults.standard.bool(forKey: "vad"))
                campaigns?.last?.setSiteVariable(key: "consent_string", value: "BOEFEAyOEFEAyAHABDENAI4AAAB9vABAASA")
            }
            setUpDemoModeCampaigns()
        }
    }
    
    func initAdmanBase() {
        mainAdsManager = Adman(siteId: 1220, region: .global, testMode: false)
        mainAdsManager?.delegate = self
        mainAdsManager?.setDefaultBannerSize(size: "300x300")
        admanUI = AdmanUIExtension()
        admanUI?.delegate = self
    }
    
    @objc func orientationChangedEvent(notify: Notification) {
        rotateByOrientation()
    }

    func rotateByOrientation() {
        //var toInterfaceOrientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChangedEvent), name: UIDevice.orientationDidChangeNotification, object: UIDevice.current)
        
        var icon = UIImage()
        var images = [UIImage]()
        for i in 1...40 {
            if i < 10 { icon = UIImage(named: "SF/loading/loading_1000" + String(i) + ".gif")! }
            else { icon = UIImage(named: "SF/loading/loading_100" + String(i) + ".gif")! }
            images.append(icon)
        }
        loadingGif?.animationImages = images
        loadingGif?.animationDuration = 1.0
        loadingGif?.animationRepeatCount = 0
        loadingGif?.startAnimating()
        demoLang = UserDefaults.standard.string(forKey: "lang")
        if demoLang == nil || demoLang?.count == 0 {
            demoLang = Locale.current.languageCode
        }
        if ADMAN_BUILD_MODE == BUILD_MODE_TRITON { initAdmanBase() }
        if ADMAN_BUILD_MODE == BUILD_MODE_VIDEO { initAdmanVideo() }
        else { initAdmanFull() }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /*
        _voiceActionsText.text = NSLocalizedStringFromTable(@"mtR-xC-RQP.text", @"Main",  @"mtR-xC-RQP.text");
        _voiceHelp.text = NSLocalizedStringFromTable(@"PDf-y7-rHG.text", @"Main",  @"PDf-y7-rHG.text");
*/
        let fontName = voiceHelp?.font?.fontName
        if demoLang == "ru" || Locale.current.languageCode == "ja" {
            introTitle?.font = UIFont(name: fontName!, size: 13)
            voiceHelp?.font = UIFont(name: fontName!, size: 19)
            endInteractionLabel?.font = UIFont(name: fontName!, size: 13)
        }
        
        if Locale.current.languageCode == "ja" {
            introTitle?.font = UIFont(name: fontName!, size: 15)
        }

        if UserDefaults.standard.bool(forKey: "debug") {
            debugView?.isHidden = false
            debugView?.layer.borderWidth = 0
            debugView?.layer.borderColor = UIColor.black.cgColor
            voiceIntentsView?.isHidden = true
        }
        if ADMAN_BUILD_MODE == BUILD_MODE_TRITON { voiceIntentsView?.isHidden = true }
        if AVAudioSession.sharedInstance().recordPermission == .granted || ADMAN_BUILD_MODE == BUILD_MODE_TRITON {
            prepare()
        } else {
            showMicrophoneDialog()
            
        }
    }

    func presentNetworkErrorPopup() {
        let restartView = UIAlertController(title: "Network error", message: "Received network/server error, please try again next time", preferredStyle: .alert)
        activeAdPlayer?.stop()
        restartView.addAction(UIAlertAction(title: "Restart", style: .default, handler: { [self] action in
            toggleDemo?.isHidden = false
            playbackSeekToStart()
            restartView.removeFromParent()
        }))
        restartView.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [self] action in
            toggleDemo?.isHidden = false
            restartView.removeFromParent()
        }))
        present(restartView, animated: true, completion: nil)
    }
    
    func showMicrophoneDialog() {
        if AVAudioSession.sharedInstance().recordPermission == .denied || UserDefaults.standard.object(forKey: "lastMicroPermissionNotifyDate") != nil {
            let lastNotifyDate = UserDefaults.standard.object(forKey: "admanLastMicroPermissionNotifyDate") as? Date
            if let lastNotifyDate = lastNotifyDate {
                if let diff = Calendar.current.dateComponents([.day], from: lastNotifyDate, to: Date()).day {
                    if diff <= 6 {
                        navigationController?.pushViewController(self, animated: true)
                        return
                    }
                }
            }
            showMessageAboutDisabledMicro()
            return
        }
        
        let infoDialog = UIAlertController(title: NSLocalizedString("Microphone request title", tableName: "Main", bundle: Bundle.main, value: "Microphone request title", comment: "Microphone request title"), message: NSLocalizedString("Microphone access", tableName: "Main", bundle: Bundle.main, value: "Microphone access", comment: "Microphone access"), preferredStyle: UIAlertController.Style.alert)
        
        let noButton = UIAlertAction(title: NSLocalizedString("No", tableName: "Main", bundle: Bundle.main, value: "No", comment: "No"), style: UIAlertAction.Style.default) { [self] action in
            let defaults = UserDefaults.standard
            let today = Date()
            defaults.set(today, forKey: "lastMicroPermissionNotifyDate")
            defaults.synchronize()
            navigationController?.pushViewController(self, animated: true)
            showMessageAboutDisabledMicro()
        }
        let yesButton = UIAlertAction(title: NSLocalizedString("Yes", tableName: "Main", bundle: Bundle.main, value: "Yes", comment: "Yes"), style: UIAlertAction.Style.default) { [self] action in
            navigationController?.pushViewController(self, animated: true)
            AVAudioSession.sharedInstance().requestRecordPermission {
                permission in
                if !permission {
                    self.showMessageAboutDisabledMicro() } else {
                        self.prepare()
                    }
            }
        }
        infoDialog.addAction(noButton)
        infoDialog.addAction(yesButton)

        present(infoDialog, animated: false, completion: nil)
    }
    
    func showMessageAboutDisabledMicro() {
        let alert = UIAlertController(title: NSLocalizedString("Microphone notify", tableName: "Main", bundle: Bundle.main, value: "Microphone notify", comment: "Microphone request title"), message: "Please allow the microphone access in order to activate voice control. That includes skipping ads, learning more and other useful commands. Do you want to enable voice control?", preferredStyle: UIAlertController.Style.alert)
        //let alert = UIAlertController(title: "title", message: "message", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in }))
        //present(alert, animated: false, completion: nil)
        //navigationController?.pushViewController(self, animated: true)
    }
    
    func prepareAdditionalCampaign(timer: Timer) {
        let index = timer.userInfo
        let instance = campaigns?[index as! Int]
        instance?.prepare(format: .any, type: .voice)
    }
    
    func prepare() {
        activeAdPlayer = mainAdsManager
        admanUI?.preloadSplashScreen()
        
        DispatchQueue.main.async { [self] in
            let adsCountStr = UserDefaults.standard.string(forKey: "ads_count") ?? "0"
            let adsCount = Int(adsCountStr) ?? 0
            mainAdsManager?.prepare(format: .any, type: .voiceVideo, maxDuration: 0, adsCount: adsCount)
        }
        if demoMode != .simple && demoMode != .video {
            campaigns?.first?.prepare(format: .any, type: .voice)
            if campaigns?.count ?? 0 > 1 { campaigns?[1].prepare(format: .any, type: .voice) }
        }
    }
    
    func getCurrentCampaignNumber() -> Int {
        if activeAdPlayer == mainAdsManager { return 0 }
        return activeAdPlayer == nil ? -1 : ((campaigns?.firstIndex(of: activeAdPlayer!))! + 1)
    }
    
    func setUpDemoModeCampaigns() {
        if demoMode == .Default {
            if region == .global {
                mainAdsManager?.setCampaignId(campaignId: 173)
                mainAdsManager?.setCreativeId(creativeId: 151)
                campaigns?.first?.setCampaignId(campaignId: 174)
                campaigns?.first?.setCreativeId(creativeId: 152)
                
                campaigns?[1].setCampaignId(campaignId: 174)
                campaigns?[1].setCreativeId(creativeId: 153)
                
                campaigns?[2].setCampaignId(campaignId: 175)
                campaigns?[2].setCreativeId(creativeId: 154)
                
                campaigns?[3].setCampaignId(campaignId: 175)
                campaigns?[3].setCreativeId(creativeId: 155)
                
                campaigns?[4].setCampaignId(campaignId: 175)
                campaigns?[4].setCreativeId(creativeId: 156)
                
                campaigns?.last?.setCampaignId(campaignId: 175)
                campaigns?.last?.setCreativeId(creativeId: 157)
            } else {
                mainAdsManager?.setCampaignId(campaignId: 191)
                
                campaigns?.first?.setCampaignId(campaignId: 192)
                campaigns?.first?.setCreativeId(creativeId: 120)
                
                campaigns?[1].setCampaignId(campaignId: 192)
                campaigns?[1].setCreativeId(creativeId: 121)
                
                campaigns?[2].setCampaignId(campaignId: 193)
                campaigns?[2].setCreativeId(creativeId: 122)
                
                campaigns?[3].setCampaignId(campaignId: 193)
                campaigns?[3].setCreativeId(creativeId: 123)
                
                campaigns?[4].setCampaignId(campaignId: 193)
                campaigns?[4].setCreativeId(creativeId: 124)
                
                campaigns?.last?.setCampaignId(campaignId: 193)
                campaigns?.last?.setCreativeId(creativeId: 125)
            }
        } else if demoMode == .PG {
            mainAdsManager?.setCampaignId(campaignId: 194)
            campaigns?.first?.setCampaignId(campaignId: 195)
            campaigns?.first?.setCreativeId(creativeId: 127)
            campaigns?[1].setCampaignId(campaignId: 195)
            campaigns?[1].setCreativeId(creativeId: 128)
        } else if demoMode == .DefaultRUEu {
            mainAdsManager?.setCampaignId(campaignId: 8567)
            mainAdsManager?.setCreativeId(creativeId: 19079)

            campaigns?.first?.setCampaignId(campaignId: 8567)
            campaigns?.first?.setCreativeId(creativeId: 19081)

            campaigns?[1].setCampaignId(campaignId: 8567)
            campaigns?[1].setCreativeId(creativeId: 19082)
        }
    }
    
    @IBAction func onDemoLaunched(_ sender: Any) {
        launchDemo?.isHidden = true
        loadingGif?.isHidden = true
        voiceIntentsView?.isHidden = true
        toggleDemo?.isHidden = true

        if mainAdsManager?.state == .readyForPlayback {
            play()
        }
    }

    func stop() {
        activeAdPlayer?.stop()
        activeAdPlayer?.cancelLoading()
    }
    
    @IBAction func onDemoRepeat(_ sender: Any) {
        if loggingTimer != nil {
            loggingTimer?.invalidate()
            loggingTimer = nil
        }
        if toggleDemo?.currentTitle == NSLocalizedString("Stop playback", tableName: "Main", bundle: Bundle.main, value: "Stop playback", comment: "Stop playback") {
            toggleDemo?.setTitle(NSLocalizedString("Start playback", tableName: "Main", bundle: Bundle.main, value: "Start playback", comment: "Start playback"), for: .normal)
            stop()
        } else {
            toggleDemo?.setTitle(NSLocalizedString("Stop playback", tableName: "Main", bundle: Bundle.main, value: "Stop playback", comment: "Stop playback"), for: .normal)
            endInteractionLabel?.isHidden = true
            if activeAdPlayer?.state == .readyForPlayback {
                play() } else {
                toggleDemo?.isHidden = true
                loadingGif?.isHidden = false
                prepare()
            }
        }
    }

    @IBAction func onPlaybackDown(_ sender: Any) {
        //loggingTimer = Timer(timeInterval: 3, target: self, selector: #selector(enableDebugging), userInfo: nil, repeats: false)
        if let loggingTimer = loggingTimer {
            RunLoop.main.add(loggingTimer, forMode: .common)
        }
    }
    
    @IBAction func onPlaybackUpOut(_ sender: Any) {
        if loggingTimer != nil {
            loggingTimer?.invalidate()
            loggingTimer = nil
        }
    }
    
    @IBAction func moreAction(_ sender: Any) {
        var request: URLRequest = URLRequest(url: URL(string: "url")!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        session.dataTask(with: request)
    }
    
    @IBAction func skipAction(_ sender: Any) {}
    
    func refresh() { playbackSeekToStart() }
    
    func playbackSeekToStart() {
        activeAdPlayer?.stop()
        admanUI?.hide()
        prepare()
        toggleDemo?.setTitle(NSLocalizedString("Start Over", tableName: "Main", bundle: Bundle.main, value: "Start Over", comment: "Start Over"), for: .normal)
        toggleDemo?.isHidden = false
    }
    
    func admanStateDidChange(sender: Adman) {
        switch (sender.state) {
        case .playing:
            DispatchQueue.main.async { [self] in
                toggleDemo?.setTitle(NSLocalizedString("Stop playback", tableName: "Main", bundle: Bundle.main, value: "Stop playback", comment: "Stop playback"), for: .normal)
            }
            if let pauseNextAd = pauseNextAd { if !pauseNextAd { break } }
            let numCampaign = getCurrentCampaignNumber()
            print("admanStateDidChange, current campaign index: " + String(numCampaign))
            if demoMode == .Default {
                if numCampaign == 1 {//([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 120) {
                    campaigns?[2].prepare(format: .any, type: .voice)
                    campaigns?[3].prepare(format: .any, type: .voice)
                } else if numCampaign == 2 {//([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 121) {
                    campaigns?[4].prepare(format: .any, type: .voice)
                    campaigns?[5].prepare(format: .any, type: .voice)
                }
                if numCampaign > 0 {//([self.activeAdPlayer campaignId] >= 192) {
                    pauseNextAd = false
                    sender.pause()
                    DispatchQueue.main.async { [self] in
                        admanUI?.showPlayButton(tapGestureRecognizer: nil)
                    }
                }
            } else if demoMode == .PG && numCampaign >= 1 {//(_demoMode == AdmanDemoModePG && [_activeAdPlayer campaignId] >= 195) {
                pauseNextAd = false
                sender.pause()
                DispatchQueue.main.async { [self] in
                    admanUI?.showPlayButton(tapGestureRecognizer: nil)
                }
            } else if demoMode == .DefaultRUEu && numCampaign >= 1 {
                pauseNextAd = false
                sender.pause()
                DispatchQueue.main.async { [self] in
                    admanUI?.showPlayButton(tapGestureRecognizer: nil)
                }
            }
        case .playbackCompleted:
            if demoMode != .simple { choosePlaybackCompletedAction() }
            else {
                setHidden(hidden: false, viewToHide:toggleDemo)
                adNone()
            }
        case .fetchingInfo:
            if launchDemo?.isHidden ?? false && loadingGif?.isHidden ?? false {
                setHidden(hidden: false, viewToHide: toggleDemo)
            }
            debugView?.text = ""
        case .speechRecognizerStarted, .speechRecognizerStopped, .voiceInteractionStarted, .voiceInteractionEnded, .voiceResponsePlaying:
               break
        case .stopped:
            toggleDemo?.setTitle(NSLocalizedString("Start playback", tableName: "Main", bundle: Bundle.main, value: "Start playback", comment: "Start playback"), for: .normal)
            toggleDemo?.isHidden = false
            setHidden(hidden: false, viewToHide: toggleDemo)
        case .readyForPlayback:
            if stuckOnPreload {
                loadingGif?.isHidden = false
                play()
                break
            }
            if launchDemo?.isHidden ?? false && loadingGif?.isHidden ?? false {
                play() }
            else {
                loadingGif?.isHidden = true
                launchDemo?.isHidden = false
            }
        case .error:
            setHidden(hidden: false, viewToHide: toggleDemo)
            sender.delegate = nil
            if let error = sender.error as NSError? {
                if error.code == 504 { presentNetworkErrorPopup() }
            }
            activeAdPlayer = mainAdsManager
            activeAdPlayer?.delegate = self
        case .adNone:
            adNone()
            setHidden(hidden: true, viewToHide: loadingGif)
        default:
                break
        }
        lastState = sender.state
    }
    
    func choosePlaybackCompletedAction() {
        let numCampaign = getCurrentCampaignNumber()
        print("choosePlaybackCompletedAction, current campaign index: " + String(numCampaign))
        if demoMode == .Default && numCampaign >= 3 {
            // creativeId >= 122
            showTryAgainView()
            return
        }
        if demoMode == .PG {
            // campaignId == 195
            if numCampaign > 0 && numCampaign < 3 {
                showTryAgainView()
                return
            }
        }
        
        if demoMode == .video {
            if numCampaign == 0 {
                showTryAgainView()
                return
            }
        }
        
        if demoMode == .DefaultRUEu {
            if numCampaign == 1 {
                showTryAgainView()
                return
            }
        }
        activeAdPlayer?.delegate = nil
        toggleDemo?.setTitle(NSLocalizedString("Start playback", tableName: "Main", bundle: Bundle.main, value: "Start playback", comment: "Start playback"), for: .normal)

        if demoMode == .Default {
            if numCampaign == 0 {
                //([_activeAdPlayer campaignId] == 191) {
                if reaction == "positive" { activeAdPlayer = (campaigns?.first)! }
                else { activeAdPlayer = (campaigns?[1])! }
            } else if numCampaign == 1 {
                //([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 120) {
                if reaction == "positive" { activeAdPlayer = (campaigns?[2])! }
                else { activeAdPlayer = (campaigns?[3])! }
            } else if numCampaign == 2 {
                //([_activeAdPlayer campaignId] == 192 && [_activeAdPlayer creativeId] == 121) {
                if reaction == "positive" { activeAdPlayer = (campaigns?[4])! }
                else { activeAdPlayer = (campaigns?[5])! }
            }
        } else if demoMode == .PG {
            if numCampaign == 0 {
                // campaignId == 194
                if reaction == "positive" { activeAdPlayer = (campaigns?.first)! }
                else { activeAdPlayer = (campaigns?[1])! }
            }
        } else if demoMode == .DefaultRUEu {
            if numCampaign == 0 {
                // creativeID == 19079
                if reaction == "positive" { activeAdPlayer = (campaigns?.first)! }
                else { activeAdPlayer = (campaigns?[1])! }
            } else if numCampaign == 2 { activeAdPlayer = (campaigns?.first)! }//creatId = 19082
        }
        activeAdPlayer?.delegate = self
        
        
        if activeAdPlayer?.state == .preloading || activeAdPlayer?.state == .fetchingInfo {
            stuckOnPreload = true
            loadingGif?.isHidden = false
        } else {
            play() }
    }
    
    func showTryAgainView() {
        activeAdPlayer?.delegate = nil
        activeAdPlayer = mainAdsManager
        activeAdPlayer?.delegate = self
        toggleDemo?.setTitle(NSLocalizedString("Start Over", tableName: "Main", bundle: Bundle.main, value: "Start Over", comment: "Start Over"), for: .normal)
        toggleDemo?.isHidden = false
        if UserDefaults.standard.bool(forKey: "debug") == false {
            endInteractionLabel?.isHidden = false
        }
    }
    
    func setHidden(hidden: Bool, viewToHide: UIView?) {
        guard let viewToHide = viewToHide else { return }
        DispatchQueue.main.async {
            viewToHide.isHidden = hidden
        }
    }
    
    func play() {
        let micOnAsset = AVAsset(url: URL(fileURLWithPath: Bundle.main.path(forResource: "Mic_on_new", ofType: "mp3")!))
        let micOffAsset = AVAsset(url: URL(fileURLWithPath: Bundle.main.path(forResource: "Mic_off_new", ofType: "mp3")!))

        guard let activeAdPlayer = activeAdPlayer else { return }
        
        for ad in activeAdPlayer.adsList {
            ad.assets[AdmanBannerAssetMicOnUrl] = (micOnAsset as! AVURLAsset).url.absoluteString
            ad.assets[AdmanBannerAssetMicOnCache] = micOnAsset
            ad.assets[AdmanBannerAssetMicOffUrl] = (micOffAsset as! AVURLAsset).url.absoluteString
            ad.assets[AdmanBannerAssetMicOffCache] = micOffAsset
        }
        toggleDemo?.isHidden = false

        log(message: "\nCreative loaded")
        let delayString = UserDefaults.standard.string(forKey: "response_delay") ?? "0"
        let delay = abs(delayString as? Double ?? -1)
        if delay > 0 {
            log(message: "Voice response delayed for " + String(delay) + " seconds")
        }
        if launchDemo!.isHidden && ((loadingGif?.isHidden) != nil) {
            activeAdPlayer.reportAdEvent(eventName: "can_show")
            //[_activeAdPlayer reportAdEvent:AdmanStatEventCanShow];
            pauseNextAd = true

            toggleDemo?.isHidden = true
            //admanUI.manager = activeAdPlayer
            //((AdmanUIBase*)_admanUI).manager = _activeAdPlayer;
            admanUI?.show(rootVC: self)
            activeAdPlayer.play()
            view.bringSubviewToFront(toggleDemo!)

            if UserDefaults.standard.bool(forKey: "debug") {
                toggleDemo?.isHidden = true
                view.bringSubviewToFront(debugScrpllView!)
            }
        }
    }
    
    func adNone() {
        DispatchQueue.main.async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [self] in
                log(message: "\nDemo app stopped")
                toggleDemo?.setTitle(NSLocalizedString("Start playback", tableName: "Main", bundle: Bundle.main, value: "Start playback", comment: "Start playback"), for: .normal)
            }
        }
        launchDemo?.isHidden = true
        loadingGif?.isHidden = true
        voiceIntentsView?.isHidden = true
        toggleDemo?.isHidden = true
        showTryAgainView()
    }
    
    func log(message: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss:SSS"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) { [self] in
            debugView?.text += "\n" + formatter.string(from: Date()) + ": " + message
        }
    }
    
    func phraseRecognized(result: [String: String]) {
        reaction = result["action"]
    }

    func errorReceived(error: Error) {
        log(message: error.localizedDescription)
    }
}
