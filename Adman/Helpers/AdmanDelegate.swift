//
//  AdmanDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

public protocol AdmanDelegate: NSObject {
    func admanStateDidChange(sender: Adman)
    
    func phraseRecognized(result: [String: Any])
    func customVoiceIntentHandler()
    func bannerTouched(urlToNavigate: String?)
    func viewClosed()
    func errorReceived(error: Error)
    func dtmfIn()
    func dtmfOut()
    func log(message: String)
}

public extension AdmanDelegate {
    func phraseRecognized(result: [String: Any]) {}
    func customVoiceIntentHandler() {}
    func bannerTouched(urlToNavigate: String?) {}
    func viewClosed() {}
    func errorReceived(error: Error) {}
    func dtmfIn() {}
    func dtmfOut() {}
    func log(message: String) {}
}
