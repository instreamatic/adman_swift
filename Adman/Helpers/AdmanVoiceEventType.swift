//
//  AdmanVoiceEventType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

public enum AdmanVoiceEventType: Int {
    case responsePlaying = 1,
         interactionStarted,
         interactionEnded,
         error,
         recognizerStarted,
         recognizerStopped
}
