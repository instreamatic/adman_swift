//
//  AdmanDtmfDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

protocol AdmanDtmfDelegate {
    func dtmfIn()
    func dtmfOut()
}
