//
//  ServerVoiceMessageType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

enum ServerVoiceMessageType: Int {
    case audioStart,
         transcript,
         result
}
