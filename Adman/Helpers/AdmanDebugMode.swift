//
//  AdmanDebugMode.swift
//  Adman_swift
//
//  Created by Barnet Birday on 27/01/2022.
//

import Foundation

enum AdmanDebugMode: Int {
    case banner = 11,
         audio = 12,
         voiceAd = 41,
         none = 0
}
