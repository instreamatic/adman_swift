//
//  AdmanControlEventType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

enum AdmanControlEventType: Int {
    case prepare=1,
         start,
         pause,
         resume,
         skip,
         click,
         link,
         swipeBack,
         positive,
         negative,
         subShow,
         subHide
}
