//
//  AdmanPreloadMode.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

var preloadMode = AdmanPreloadMode.progressive

public enum AdmanPreloadMode: Int {
    case progressive,
         full
}
