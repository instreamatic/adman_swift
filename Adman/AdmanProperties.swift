//
//  AdmanProperties.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation
import EventKit
import AVFAudio
import Network
import CoreTelephony

struct AdmanProperties {
    var idfa = ""
    var isTrackingEnabled = false
    var useGeolocationTargeting = false
    var latitude = 0
    var longitude = -1
    var isAudioSessionSetupEnabled = true
    var debugMode = AdmanDebugMode.none
    var dtmfAdsDetection = false
    var dtmfStationKey: String?
    var siteId = DEFAULT_SITE_ID
    var zoneId = DEFAULT_ZONE_ID
    var userId: String?
    var campaignId: Int?
    var creativeId: Int?
    var admanType = AdmanType.any
    var region = AdmanRegion.EU
    var format = AdmanFormat.any
    var maxAdBlockDuration: Int?
    var maxAdsCount: Int?
    var clientIP: String?
    var backend = AdmanRecognitionBackend.auto
    var recognitionBackendLocation = AdmanRecognitionBackendLocation.auto
    var serverVoiceActivityDetection = false
    var clientVoiceActivityEnded = false
    var voiceResponseDelay = -1
    var serverTimeout = 4
    var customVoiceResponseDelay = 0
    var customAdResponseTime = 0
    var siteVariables = [String: String]()
    var customAdServer: String?
    var requestURL: String?
    var lastFetchTime: Date?
    var slot = AdmanAdSlot.adSlotDefault
    
    init() {
        let adIdentManager = ASIdentifierManager.init(advertisingIdentifier: NSUUID())
        idfa = adIdentManager.advertisingIdentifier.uuidString
        isTrackingEnabled = adIdentManager.advertisingTrackingEnabled
    }
    
    func requestParameters(admanProperties: AdmanProperties) -> [String: String] {
        var requestParams = [String: String]()
        requestParams["idfa"] = admanProperties.idfa
        requestParams["advertising_tracking_enabled"] = admanProperties.isTrackingEnabled ? "1" : "0"
        requestParams["version"] = AdmanVersion
        requestParams["slot"] = admanAdSlotString(slot: admanProperties.slot)
        if admanProperties.admanType == .voice || admanProperties.admanType == .any || admanProperties.admanType == .voiceMultipleAds || admanProperties.admanType == .voiceVideo {
            requestParams["microphone"] = AdmanMicrophone().isMicrophoneEnabledString()
        }
        if EKEventStore.authorizationStatus(for: EKEntityType.event) == EKAuthorizationStatus.authorized {
            requestParams["calendar"] = "1"
        }
        //requestParams["preview"] = String(admanProperties.debugMode.rawValue)
        if let ip = admanProperties.clientIP { requestParams["ip"] = ip }
        if let campaignId = admanProperties.campaignId { requestParams["campaign_id"] = String(campaignId) }
        if let creativeId = admanProperties.creativeId { requestParams["creative_id"] = String(creativeId) }
        requestParams["zone_id"] = String(admanProperties.zoneId)
        if let userId = admanProperties.userId { requestParams["user_id"] = userId }
        requestParams["format"] = String(admanProperties.format.rawValue)
        if let maxAdsCount = admanProperties.maxAdsCount { requestParams["ads_count"] = String(maxAdsCount) }
        if let maxAdBlockDuration = admanProperties.maxAdBlockDuration {
            requestParams["duration"] = String(maxAdBlockDuration)
        }
        if admanProperties.useGeolocationTargeting {
            requestParams["lat"] = String(admanProperties.latitude)
            requestParams["long"] = String(admanProperties.longitude)
        }
        for key in admanProperties.siteVariables {
            requestParams[key.key] = admanProperties.siteVariables[key.value]
        }
        requestParams["lang"] = String(NSLocale.preferredLanguages.first?.description ?? "")
        AdmanBackground().isInBackground(synced: true) { appIsInBG in
            switch admanProperties.admanType {
            case .any:
                requestParams["type"] = "any"
                if #available(iOS 12.0, *) {
                    if appIsInBG {
                        requestParams["type"] = "radio"
                    } else {
                        requestParams["type"] = "any"
                    }
                }
            case .audioOnly:
                requestParams["type"] = "radio"
            case .audioPlus:
                requestParams["type"] = "digital"
            case .voice:
                requestParams["type"] = "voice"
                if #available(iOS 12.0, *) {
                    if appIsInBG {
                        requestParams["type"] = "radio"
                    } else {
                        requestParams["type"] = "voice"
                    }
                }
            case .voiceVideo:
                requestParams["type"] = "voice-video"
            default:
                break
            }
        }
        
        let networks_2g = ["CTRadioAccessTechnologyGPRS", "CTRadioAccessTechnologyEdge", "CTRadioAccessTechnologyCDMA1x"]
        let networks_4g = ["CTRadioAccessTechnologyLTE"]
        let networkStatus = NWPathMonitor()
        if networkStatus.currentPath.usesInterfaceType(.wifi) {
            requestParams["network_type"] = "wifi"
        } else {
            let info = CTTelephonyNetworkInfo()
            var carrierType = ""
            if #available(iOS 12.1, *) {
                let carriers = info.serviceCurrentRadioAccessTechnology
                if let carrierName = carriers?.keys.first { carrierType = carriers?[carrierName] ?? "" }
            } else { carrierType = info.currentRadioAccessTechnology ?? "" }
            
            if networks_2g.contains(carrierType) { requestParams["network_type"] = "2g"
            } else if networks_4g.contains(carrierType) { requestParams["network_type"] = "4g"
            } else { requestParams["network_type"] = "3g" }
        }
        
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        let output = currentRoute.outputs.first
        if output?.portType ==  AVAudioSession.Port.bluetoothA2DP {
            requestParams["audio_output"] = "bluetootha2dp"
        } else if output?.portType ==  AVAudioSession.Port.bluetoothLE {
            requestParams["audio_output"] = "bluetootha2dp"
        } else if output?.portType ==  AVAudioSession.Port.headphones {
            requestParams["audio_output"] = "headphones"
        } else if output?.portType ==  AVAudioSession.Port.headphones {
            requestParams["audio_output"] = "speakerphone"
        } else { requestParams["audio_output"] = "speakerphone" }
        return requestParams
    }
    
    mutating func setResponseTime(duration: Float) { customAdResponseTime = Int(duration) }
    
    mutating func setSiteVariable(key: String, value: String) { siteVariables[key] = value }
}
