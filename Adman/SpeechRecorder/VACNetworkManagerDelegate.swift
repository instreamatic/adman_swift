//
//  VACNetworkManagerDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 21/02/2022.
//

import Foundation

protocol VACNetworkManagerDelegate {
    func networkManagerConnectionDidOpened(manager: VACNetworkManager)
    func networkManagerResponseReceived(manager: VACNetworkManager, response: [String: Any])
    func networkManagerDidFail(manager: VACNetworkManager, error: NSError)
    func networkManagerSessionEnded(manager: VACNetworkManager, response: [String: Any])
    func networkManagerCommandReceived(manager: VACNetworkManager, command: String)
}
