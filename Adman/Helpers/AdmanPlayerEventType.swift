//
//  AdmanPlayerEventType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

public enum AdmanPlayerEventType: Int {
    case prepare = 1,
         ready,
         failed,
         playing,
         play,
         pause,
         progress,
         complete,
         closeable,
         skippable,
         waitingToPlayAtSpecifiedRate,
         loadImage
}
