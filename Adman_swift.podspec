Pod::Spec.new do |spec|

  spec.name         = "Adman_swift"
  spec.version      = "0.0.7"
  spec.swift_version = '5.0'
  spec.summary      = "Adman is used to inject ads, to control audio ad playback and to serve a companion display banner"

  spec.description  = <<-DESC
Voice-Activated Ads allow users to engage with ads using voice commands.
While listening to an audio, an ad user can speak various voice commands which will be introduced to users within the ad creative. When a user says ‘skip it’, ‘next’, ‘cancel’, ‘not interested’, etc, we stop serving the current ad and play the next one if there is one scheduled. 

When a user engages with the ad, we perform the requested action, e.g. open browser, playing a successive audio with ‘more details’, etc.

Actions can be ‘positive’ when users want to ‘know more’ or make a purchase or ‘negative’ when users say ‘no’ or ‘I’m not interested’.
                   DESC
  spec.homepage     = "https://help.instreamatic.com/instreamatic-dialogue-ads/manual-for-swift-apps"
  spec.license      = "GNU AGPLv3"
  spec.author             = { "Instreamatic" => "support@instreamatic.com" }
  spec.platform     = :ios, "14.0"
  spec.source       = { :git => "https://bitbucket.org/instreamatic/adman_swift", :tag => "#{spec.version}" }
  spec.source_files  = [
                           "Adman/**/*.{h,m,swift}"
                       ]
  spec.exclude_files = "Classes/Exclude"

end
