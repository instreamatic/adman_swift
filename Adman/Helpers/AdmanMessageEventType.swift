//
//  AdmanMessageEventType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

enum AdmanMessageEventType: Int {
    case fetchingInfo = 1,
         preloading,
         none,
         error,
         ready,
         started,
         stopped,
         almostCompleted,
         completed,
         skipped
}
