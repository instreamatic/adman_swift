import Foundation
import CoreMedia

// MARK: - VastElement
struct VastElement {
    var version: String?//attribute
    var adElement: AdElement?
    var errorElement: String?
    
    init(dict: [String: Any]) {
        if let vast = dict["VAST"] as? [String: Any] {
            version = xmlAttribute(dict: dict, fieldName: "version")
            if let adDict = vast["Ad"] as? [String: Any] {
                adElement = AdElement(dict: adDict)
            }
            if let dic = vast["Error"] as? [String: Any] {
                if let err = dic["xml_internal_text"] as? String {
                    errorElement = err
                } else if let err = dic["Error"] as? String {
                    errorElement = err
                }
            }
        }
    }
}

// MARK: - Ad
struct AdElement {
    var idElement: String?//attribute
    var sequenceElement: String?//attribute
    var inLineElement: InLineElement?
    var wrapperElement: WrapperElement?
    
    init(dict: [String: Any]) {
        idElement = xmlAttribute(dict: dict, fieldName: "id")
        sequenceElement = xmlAttribute(dict: dict, fieldName: "sequence")
        if let inLineDic = dict["InLine"] as? [String: Any] {
            self.inLineElement = InLineElement(dict: inLineDic)
        }
        if let wrapperElementDic = dict["Wrapper"] as? [String: Any] {
            self.wrapperElement = WrapperElement(dict: wrapperElementDic)
        }
    }
}

// MARK: - Wrapper
struct WrapperElement {
    var adSystem: AdSystem?
    var VASTAdTagURI: String?
    var errorElement: String?
    var impression: Impression?
    var creatives: Creatives?
    var extensions: ExtensionsElements?
        
    init(dict: [String: Any]) {
        if let adSystem = dict["AdSystem"] as? [String: Any] {
            self.adSystem = AdSystem(dict: adSystem)
        }
        
        if let dic = dict["Error"] as? [String: Any] {
            if let text = dic["xml_internal_text"] as? String {
                self.errorElement = text
            }
        } else if let text = dict["Error"] as? String {
            self.errorElement = text
        }
        if let dic = dict["Impression"] as? [String: Any] {
            self.impression = Impression(dict: dic)
        } else if let dic = dict["Impression"] as? String {
            self.impression = Impression(text: dic)
        }
        if let dic = dict["Creatives"] as? [String: Any] {
            self.creatives = Creatives(dict: dic)
        }
        if let dic = dict["Extensions"] as? [String: Any] {
            self.extensions = ExtensionsElements(dict: dic)
        }
        if let dic = dict["VASTAdTagURI"] as? [String: Any] {
            if let text = dic["xml_internal_text"] as? String {
                self.VASTAdTagURI = text
            }
        } else if let text = dict["VASTAdTagURI"] as? String {
            self.VASTAdTagURI = text
        }
    }
}

// MARK: - InLine
struct InLineElement {
    var adSystem: AdSystem?
    var adTitle: String?
    var description: String?
    var advertiser: String?
    var pricing: Pricing?
    var survey: String?
    var errorElement: String?
    var impression: Impression?
    var creatives: Creatives?
    var extensions: ExtensionsElements?
        
    init(dict: [String: Any]) {
        if let adSystem = dict["AdSystem"] as? [String: Any] {
            self.adSystem = AdSystem(dict: adSystem)
        }
        if let adTitleDic = dict["AdTitle"] as? [String: Any] {
            if let text = adTitleDic["xml_internal_text"] as? String {
                self.adTitle = text
            }
        } else if let text = dict["AdTitle"] as? String {    
            self.adTitle = text
        }
            
        if let descDic = dict["Description"] as? [String: Any] {
            if let text = descDic["xml_internal_text"] as? String {
                self.description = text
            }
        } else if let text = dict["Description"] as? String {
            self.description = text
        }
        
        if let advDic = dict["Advertiser"] as? [String: Any] {
            if let text = advDic["xml_internal_text"] as? String {
                self.advertiser = text
            }
        } else if let text = dict["Advertiser"] as? String {
            self.advertiser = text
        }
        
        if let priceDic = dict["Pricing"] as? [String: Any] {
            self.pricing = Pricing(dict: priceDic)
        }
        
        if let surDic = dict["Survey"] as? [String: Any] {
            if let text = surDic["xml_internal_text"] as? String {
                self.survey = text
            }
        } else if let text = dict["Survey"] as? String {
            self.survey = text
        }
        
        if let dic = dict["Error"] as? [String: Any] {
            if let text = dic["xml_internal_text"] as? String {
                self.errorElement = text
            }
        } else if let text = dict["Error"] as? String {
            self.errorElement = text
        }
        
        if let dic = dict["Impression"] as? [String: Any] {
            self.impression = Impression(dict: dic)
        } else if let dic = dict["Impression"] as? String {
            self.impression = Impression(text: dic)
        }
        if let dic = dict["Creatives"] as? [String: Any] {
            self.creatives = Creatives(dict: dic)
        }
        if let dic = dict["Extensions"] as? [String: Any] {
            self.extensions = ExtensionsElements(dict: dic)
        }
    }
}

// MARK: - Impression
struct Impression {
    var idElement: String?//attribute
    var text: String?
    
    init(dict: [String: Any]) {
        idElement = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "Impression")
    }
    init(text: String) { self.text = text }
}

// MARK: - Creatives
struct Creatives {
    var creatives = [CreativeElement]()
    
    init(dict: [String: Any]) {
        if let arr = dict["Creative"] as? [[String: Any]] {
            for creative in arr {
                creatives.append(CreativeElement(dict: creative))
            }
        } else if let arr = dict["Creative"] as? [String: Any] {
            creatives.append(CreativeElement(dict: arr))
        }
    }
}

struct TrackingEvents {
    var trackingEvents = [TrackingEvent]()
    
    init(dict: [String: Any]) {
        if let arr = dict["Tracking"] as? [[String: Any]] {
            for track in arr {
                trackingEvents.append(TrackingEvent(dict: track))
            }
        } else if let arr = dict["Tracking"] as? [String: Any] {
            trackingEvents.append(TrackingEvent(dict: arr))
        }
    }
}

struct NonLinears {
    var nonlinears = [NonLinear]()
    
    init(dict: [String: Any]) {
        if let arr = dict["NonLinear"] as? [[String: Any]] {
            for nonlin in arr {
                nonlinears.append(NonLinear(dict: nonlin))
            }
        } else if let arr = dict["NonLinear"] as? [String: Any] {
            nonlinears.append(NonLinear(dict: arr))
        }
    }
}

struct MediaFiles {
    var mediaFiles = [MediaFile]()
    
    init(dict: [String: Any]) {
        if let arr = dict["MediaFile"] as? [[String: Any]] {
            for media in arr {
                mediaFiles.append(MediaFile(dict: media))
            }
        } else if let arr = dict["MediaFile"] as? [String: Any] {
            mediaFiles.append(MediaFile(dict: arr))
        }
    }
}

// MARK: - Pricing
struct Pricing {
    var model: String?//attribute
    var currency: String?//attribute
    var text: String?
    
    init(dict: [String: Any]) {
        model = xmlAttribute(dict: dict, fieldName: "model")
        currency = xmlAttribute(dict: dict, fieldName: "currency")
        text = xmlInternalText(dict: dict, fieldName: "Pricing")
    }
    init(text: String) { self.text = text }
}

// MARK: - Extension
struct Extension {
    var type: String?//attribute
    var text: String?
    var responses: Responses?
    var admanValue: AdmanValue?
    
    init(dict: [String: Any]) {
        type = xmlAttribute(dict: dict, fieldName: "type")
        if let text = dict["xml_internal_text"] as? String {
            self.text = text
        } else if let text = dict["Extension"] as? String {
            self.text = text
        }
        if let dic = dict["Responses"] as? [String: Any] {
            self.responses = Responses(dict: dic)
        }
        if let val = dict["Value"] as? [String: Any] {
            self.admanValue = AdmanValue(dict: val)
        } else if let val = dict["Value"] as? String {
            self.admanValue = AdmanValue(text: val)
        }
    }
    init(text: String) { self.text = text }
}

struct ExtensionsElements {
    var extensions = [Extension]()

    init(dict: [String: Any]) {
        if let arr = dict["Extension"] as? [[String: Any]] {
            for extEl in arr {
                extensions.append(Extension(dict: extEl))
            }
        } else if let arr = dict["Extension"] as? [String: Any] {
            extensions.append(Extension(dict: arr))
        }
    }
}

struct Responses {
    var responses = [Response]()

    init(dict: [String: Any]) {
        if let arr = dict["Response"] as? [[String: Any]] {
            for resp in arr {
                responses.append(Response(dict: resp))
            }
        } else if let arr = dict["Response"] as? [String: Any] {
            responses.append(Response(dict: arr))
        }
    }
}

// MARK: - ExtensionValue
struct ExtensionValue {
    var type: String?//attribute
    var sequence: String?//attribute
    var text: String?
    var mediaFiles: MediaFiles?
    var trackingEvents: TrackingEvents?
    
    init(dict: [String: Any]) {
        type = xmlAttribute(dict: dict, fieldName: "type")
        sequence = xmlAttribute(dict: dict, fieldName: "sequence")
        if let dic = dict["MediaFiles"] as? [String: Any] {
            self.mediaFiles = MediaFiles(dict: dic)
        }
        if let dic = dict["TrackingEvents"] as? [String: Any] {
            self.trackingEvents = TrackingEvents(dict: dic)
        }
        text = xmlInternalText(dict: dict, fieldName: "Value")
    }
    init(text: String) { self.text = text }
}

// MARK: - Response
struct Response {
    var action: String?//attribute
    var type: String?//attribute
    var priority: String?//attribute
    var admanValues: AdmanValues?
    
    init(dict: [String: Any]) {
        action = xmlAttribute(dict: dict, fieldName: "action")
        type = xmlAttribute(dict: dict, fieldName: "type")
        priority = xmlAttribute(dict: dict, fieldName: "priority")
        if let dic = dict["Values"] as? [String: Any] {
            self.admanValues = AdmanValues(dict: dic)
        }
    }
}

// MARK: - AdmanValue
struct AdmanValue {
    var type: String?//attribute
    var sequence: String?//attribute
    var text: String?
    var mediaFiles: MediaFiles?
    var trackingEvents: TrackingEvents?
    
    init(text: String) { self.text = text }
    
    init(dict: [String: Any]) {
        type = xmlAttribute(dict: dict, fieldName: "type")
        sequence = xmlAttribute(dict: dict, fieldName: "sequence")
        if let dic = dict["MediaFiles"] as? [String: Any] {
            self.mediaFiles = MediaFiles(dict: dic)
        }
        if let dic = dict["TrackingEvents"] as? [String: Any] {
            self.trackingEvents = TrackingEvents(dict: dic)
        }
        text = xmlInternalText(dict: dict, fieldName: "Value")
    }
}

// MARK: - AdSystem
struct AdSystem {
    var version/*attribute*/, text: String?
    
    init(dict: [String: Any]) {
        version = xmlAttribute(dict: dict, fieldName: "version")
        text = xmlInternalText(dict: dict, fieldName: "AdSystem")
    }
}

// MARK: - CreativeElement
struct CreativeElement {
    var id, sequence, adID, apiFramework: String? //attribute
    var linear: Linear?
    var companionAds: CompanionAds?
    var creativeExtensions: CreativeExtensions?
    var nonLinearAds: NonLinearAds?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        sequence = xmlAttribute(dict: dict, fieldName: "sequence")
        adID = xmlAttribute(dict: dict, fieldName: "adID")
        apiFramework = xmlAttribute(dict: dict, fieldName: "apiFramework")
        if let linear = dict["Linear"] as? [String: Any] {
            self.linear = Linear(dict: linear)
        }
        if let companionAds = dict["CompanionAds"] as? [String: Any] {
            self.companionAds = CompanionAds(dict: companionAds)
        }
        if let creativeExtensions = dict["CreativeExtensions"] as? [String: Any] {
            self.creativeExtensions = CreativeExtensions(dict: creativeExtensions)
        }
        if let nonLinearAds = dict["NonLinearAds"] as? [String: Any] {
            self.nonLinearAds = NonLinearAds(dict: nonLinearAds)
        }
    }
}

// MARK: - NonLinearAds
struct NonLinearAds {
    var nonLinears: NonLinears?
    var trackingEvents: TrackingEvents?
        
    init(dict: [String: Any]) {
        if let dic = dict["NonLinear"] as? [String: Any] {
            self.nonLinears = NonLinears(dict: dic)
        }
        if let dic = dict["TrackingEvents"] as? [String: Any] {
            self.trackingEvents = TrackingEvents(dict: dic)
        }
    }
}

struct CreativeExtensions {
    var creativeExtensions = [CreativeExtension]()
        
    init(dict: [String: Any]) {
        if let arr = dict["CreativeExtension"] as? [[String: Any]] {
            for extens in arr {
                creativeExtensions.append(CreativeExtension(dict: extens))
            }
        } else if let arr = dict["CreativeExtension"] as? [String: Any] {
            creativeExtensions.append(CreativeExtension(dict: arr))
        }
    }
}

// MARK: - CompanionAds
struct CompanionAds {
    var required: String?
    var companions = [Companion]()
    
    init(dict: [String: Any]) {
        required = xmlAttribute(dict: dict, fieldName: "required")
        if let arr = dict["Companion"] as? [[String: Any]] {
            for companion in arr {
                companions.append(Companion(dict: companion))
            }
        } else if let arr = dict["Companion"] as? [String: Any] {
            companions.append(Companion(dict: arr))
        }
    }
}

// MARK: - NonLinear
struct NonLinear {
    var id, width, height, expandedWidth, expandedHeight, scalable, maintainAspectRatio, minSuggestedDuration, apiFramework: String? //attribute
    var staticResource: StaticResource?
    var iFrameResource: IFrameResource?
    var hTMLResource: HtmlResource?
    var nonLinearClickTracking: NonLinearClickTracking?
    var nonLinearClickThrough: NonLinearClickThrough?
    var adParameters: AdParameters?
    
    init(dict: [String: Any]) {
        self.id = xmlAttribute(dict: dict, fieldName: "id")
        width = xmlAttribute(dict: dict, fieldName: "width")
        height = xmlAttribute(dict: dict, fieldName: "height")
        expandedWidth = xmlAttribute(dict: dict, fieldName: "expandedWidth")
        expandedHeight = xmlAttribute(dict: dict, fieldName: "expandedHeight")
        scalable = xmlAttribute(dict: dict, fieldName: "scalable")
        maintainAspectRatio = xmlAttribute(dict: dict, fieldName: "maintainAspectRatio")
        apiFramework = xmlAttribute(dict: dict, fieldName: "apiFramework")
        minSuggestedDuration = xmlAttribute(dict: dict, fieldName: "minSuggestedDuration")
        if let dic = dict["StaticResource"] as? [String: Any] {
            self.staticResource = StaticResource(dict: dic)
        }
        
        if let dic = dict["IFrameResource"] as? [String: Any] {
            self.iFrameResource = IFrameResource(dict: dic)
        }
        
        if let dic = dict["HTMLResource"] as? [String: Any] {
            self.hTMLResource = HtmlResource(dict: dic)
        }
        
        if let dic = dict["AdParameters"] as? [String: Any] {
            self.adParameters = AdParameters(dict: dic)
        }
        
        if let dic = dict["NonLinearClickTracking"] as? [String: Any] {
            self.nonLinearClickTracking = NonLinearClickTracking(dict: dic)
        }
        
        if let dic = dict["NonLinearClickThrough"] as? [String: Any] {
            self.nonLinearClickThrough = NonLinearClickThrough(dict: dic)
        }
    }
}

// MARK: - CreativeExtension
struct CreativeExtension {
    var text: String?
    
    init(text: String) { self.text = text }
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "CreativeExtension")
    }
}

// MARK: - Companion
struct Companion {
    var id, width, height, assetWidth, assetHeight, expandedWidth, expandedHeight, apiFramework, adSlotID: String?
    var staticResource: StaticResource?
    var iFrameResource: IFrameResource?
    var hTMLResource: HtmlResource?
    var adParameters: AdParameters?
    var altText: String?
    var companionClickThrough: CompanionClickThrough?
    var companionClickTracking: CompanionClickTracking?
    var trackingEvents: TrackingEvents?
    
    init(dict: [String: Any]) {
        self.id = xmlAttribute(dict: dict, fieldName: "id")
        width = xmlAttribute(dict: dict, fieldName: "width")
        height = xmlAttribute(dict: dict, fieldName: "height")
        assetWidth = xmlAttribute(dict: dict, fieldName: "assetWidth")
        assetHeight = xmlAttribute(dict: dict, fieldName: "assetHeight")
        expandedWidth = xmlAttribute(dict: dict, fieldName: "expandedWidth")
        expandedHeight = xmlAttribute(dict: dict, fieldName: "expandedHeight")
        apiFramework = xmlAttribute(dict: dict, fieldName: "apiFramework")
        adSlotID = xmlAttribute(dict: dict, fieldName: "adSlotID")
        if let dic = dict["StaticResource"] as? [String: Any] {
            self.staticResource = StaticResource(dict: dic)
        }
        
        if let dic = dict["IFrameResource"] as? [String: Any] {
            self.iFrameResource = IFrameResource(dict: dic)
        }
        
        if let dic = dict["HTMLResource"] as? [String: Any] {
            self.hTMLResource = HtmlResource(dict: dic)
        }
        
        if let dic = dict["AdParameters"] as? [String: Any] {
            self.adParameters = AdParameters(dict: dic)
        }
        
        if let dic = dict["CompanionClickTracking"] as? [String: Any] {
            self.companionClickTracking = CompanionClickTracking(dict: dic)
        } else if let dic = dict["CompanionClickTracking"] as? String {
            self.companionClickTracking = CompanionClickTracking(text: dic)
        }
        
        if let dic = dict["TrackingEvents"] as? [String: Any] {
            self.trackingEvents = TrackingEvents(dict: dic)
        }
        if let dic = dict["AltText"] as? [String: Any] {
            if let text = dic["xml_internal_text"] as? String {
                self.altText = text
            } else if let text = dict["AltText"] as? String {
                self.altText = text
            }
        }
        if let dic = dict["AltText"] as? [String: Any] {
            if let text = dic["xml_internal_text"] as? String {
                self.altText = text
            } else if let text = dict["AltText"] as? String {
                self.altText = text
            }
        }
        
        if let dic = dict["CompanionClickThrough"] as? [String: Any] {
            self.companionClickThrough = CompanionClickThrough(dict: dic)
        } else if let dic = dict["CompanionClickThrough"] as? String {
            self.companionClickThrough = CompanionClickThrough(text: dic)
        }
    }
}

// MARK: - CompanionClickTracking
struct CompanionClickTracking {
    var id: String?
    var text: String?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "CompanionClickTracking")
    }
    init(text: String) { self.text = text }
}

// MARK: - CompanionClickThrough
struct CompanionClickThrough {
    var text: String?
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "CompanionClickThrough")
    }
    init(text: String) { self.text = text }
}

// MARK: - Linear
struct Linear {
    var skipoffset: String? //attribute
    var trackingEvents: TrackingEvents?
    var videoClicks: VideoClicks?
    var duration: String?
    var mediaFiles: MediaFiles?
    var adParameters: AdParameters?
    var icons: Icons?
    
    init(dict: [String: Any]) {
        skipoffset = xmlAttribute(dict: dict, fieldName: "skipoffset")
        if let trackingEvents = dict["TrackingEvents"] as? [String: Any] {
            self.trackingEvents = TrackingEvents(dict: trackingEvents)
        }
        if let videoClicks = dict["VideoClicks"] as? [String: Any] {
            self.videoClicks = VideoClicks(dict: videoClicks)
        }
        if let dic = dict["Duration"] as? String {
            self.duration = dic
        }
        if let dic = dict["MediaFiles"] as? [String: Any] {
            self.mediaFiles = MediaFiles(dict: dic)
        }
        if let dic = dict["AdParameters"] as? [String: Any] {
            self.adParameters = AdParameters(dict: dic)
        }
        if let dic = dict["Icons"] as? [String: Any] {
            self.icons = Icons(dict: dic)
        }
    }
}

// MARK: - AdParameters
struct AdParameters {
    var xmlEncoded: String?
    var text: String?
    
    init(dict: [String: Any]) {
        xmlEncoded = xmlAttribute(dict: dict, fieldName: "xmlEncoded")
        text = xmlInternalText(dict: dict, fieldName: "AdParameters")
    }
    init(text: String) { self.text = text }
}

// MARK: - Icons
struct Icons {
    var icons = [Icon]()
    
    init(dict: [String: Any]) {
        if let arr = dict["Icon"] as? [[String: Any]] {
            for icon in arr {
                icons.append(Icon(dict: icon))
            }
        } else if let arr = dict["Icon"] as? [String: Any] {
            icons.append(Icon(dict: arr))
        }
    }
}

// MARK: - AdmanValues
struct AdmanValues {
    var admanValues = [AdmanValue]()
    
    init(dict: [String: Any]) {
        if let arr = dict["Value"] as? [[String: Any]] {
            for el in arr {
                admanValues.append(AdmanValue(dict: el))
            }
        } else if let arr = dict["Value"] as? [String: Any] {
            admanValues.append(AdmanValue(dict: arr))
        }
    }
}

// MARK: - Icon
struct Icon {
    var program, width, height, xPosition: String? //attribute
    var yPosition, duration, offset, apiFramework: String? //attribute
    var staticResource: StaticResource?
    var iFrameResource: IFrameResource?
    var htmlResource: HtmlResource?
    var iconClicks: IconClicks?
    var iconViewTracking: IconViewTracking?
    var text: String?
    
    init(dict: [String: Any]) {
        program = xmlAttribute(dict: dict, fieldName: "program")
        width = xmlAttribute(dict: dict, fieldName: "width")
        height = xmlAttribute(dict: dict, fieldName: "height")
        xPosition = xmlAttribute(dict: dict, fieldName: "xPosition")
        yPosition = xmlAttribute(dict: dict, fieldName: "yPosition")
        duration = xmlAttribute(dict: dict, fieldName: "duration")
        offset = xmlAttribute(dict: dict, fieldName: "offset")
        apiFramework = xmlAttribute(dict: dict, fieldName: "apiFramework")
        text = xmlInternalText(dict: dict, fieldName: "Icon")
        
        if let dic = dict["StaticResource"] as? [String: Any] {
            self.staticResource = StaticResource(dict: dic)
        }
        
        if let dic = dict["IconClicks"] as? [String: Any] {
            self.iconClicks = IconClicks(dict: dic)
        }
        
        if let dic = dict["IconViewTracking"] as? [String: Any] {
            self.iconViewTracking = IconViewTracking(dict: dic)
        }
        
        if let dic = dict["IFrameResource"] as? [String: Any] {
            self.iFrameResource = IFrameResource(dict: dic)
        }
        
        if let dic = dict["HTMLResource"] as? [String: Any] {
            self.htmlResource = HtmlResource(dict: dic)
        }
    }
    init(text: String) { self.text = text }
}

// MARK: - IFrameResource
struct IFrameResource {
    var text: String?
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "IFrameResource")
    }
    init(text: String) { self.text = text }
}

// MARK: - HtmlResource
struct HtmlResource {
    var text: String?
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "HTMLResource")
    }
    init(text: String) { self.text = text }
}

// MARK: - IconClicks
struct IconClicks {
    var text: String?
    var iconClickThrough: IconClickThrough?
    var iconClickTracking: IconClickTracking?
        
    init(dict: [String: Any]) {
        if let dic = dict["IconClickThrough"] as? [String: Any] {
            self.iconClickThrough = IconClickThrough(dict: dic)
        }
        if let dic = dict["IconClickTracking"] as? [String: Any] {
            self.iconClickTracking = IconClickTracking(dict: dic)
        }
        text = xmlInternalText(dict: dict, fieldName: "IconClicks")
    }
    init(text: String) { self.text = text }
}

// MARK: - IconClickTracking
struct IconClickTracking {
    var text: String?
    var id: String?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "IconClickTracking")
    }
    init(text: String) { self.text = text }
}

// MARK: - IconClickThrough
struct IconClickThrough {
    var text: String?
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "IconClickThrough")
    }
    init(text: String) { self.text = text }
}

// MARK: - IconViewTracking
struct IconViewTracking {
    var text: String?
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "IconViewTracking")
    }
    init(text: String) { self.text = text }
}

// MARK: - StaticResource
struct StaticResource {
    var creativeType: String?
    var text: String?
    
    init(dict: [String: Any]) {
        creativeType = xmlAttribute(dict: dict, fieldName: "creativeType")
        text = xmlInternalText(dict: dict, fieldName: "StaticResource")
    }
    init(text: String) { self.text = text }
}

// MARK: - MediaFile
struct MediaFile {
    var id, delivery, type, bitrate, minBitrate, maxBitrate, width: String?//attribute
    var height, scalable, mantainAspectRatio, codec, apiFramework: String?//attribute
    var text: String?
    
    init(dict: [String: Any]) {
        self.id = xmlAttribute(dict: dict, fieldName: "id")
        delivery = xmlAttribute(dict: dict, fieldName: "delivery")
        type = xmlAttribute(dict: dict, fieldName: "type")
        bitrate = xmlAttribute(dict: dict, fieldName: "bitrate")
        minBitrate = xmlAttribute(dict: dict, fieldName: "minBitrate")
        maxBitrate = xmlAttribute(dict: dict, fieldName: "maxBitrate")
        apiFramework = xmlAttribute(dict: dict, fieldName: "apiFramework")
        codec = xmlAttribute(dict: dict, fieldName: "codec")
        mantainAspectRatio = xmlAttribute(dict: dict, fieldName: "mantainAspectRatio")
        scalable = xmlAttribute(dict: dict, fieldName: "scalable")
        width = xmlAttribute(dict: dict, fieldName: "width")
        height = xmlAttribute(dict: dict, fieldName: "height")
        text = xmlInternalText(dict: dict, fieldName: "MediaFile")
    }
    init(text: String) { self.text = text }
}

// MARK: - TrackingEvent
struct TrackingEvent {
    var event: String?
    var text: String?
    
    init(dict: [String: Any]) {
        event = xmlAttribute(dict: dict, fieldName: "event")
        text = xmlInternalText(dict: dict, fieldName: "Tracking")
    }
    init(text: String) { self.text = text }
}

// MARK: - VideoClicks
struct VideoClicks {
    var clickTracking: ClickTracking?
    var clickThrough: ClickThrough?
    var customClick: CustomClick?
        
    init(dict: [String: Any]) {
        if let dic = dict["ClickTracking"] as? [String: Any] {
            self.clickTracking = ClickTracking(dict: dic)
        } else if let dic = dict["ClickTracking"] as? String {
            self.clickTracking = ClickTracking(text: dic)
        }
        if let dic = dict["ClickThrough"] as? [String: Any] {
            self.clickThrough = ClickThrough(dict: dic)
        } else if let dic = dict["ClickThrough"] as? String {
            self.clickThrough = ClickThrough(text: dic)
        }
        if let dic = dict["CustomClick"] as? [String: Any] {
            self.customClick = CustomClick(dict: dic)
        } else if let dic = dict["CustomClick"] as? String {
            self.customClick = CustomClick(text: dic)
        }
    }
}

// MARK: - ClickThrough
struct ClickThrough {
    var id: String?
    var text: String?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "ClickThrough")
    }
    init(text: String) { self.text = text }
}

// MARK: - ClickTracking
struct ClickTracking {
    var id: String?
    var text: String?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "ClickTracking")
    }
    init(text: String) { self.text = text }
}

// MARK: - CustomClick
struct CustomClick {
    var id: String?
    var text: String?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "CustomClick")
    }
    init(text: String) { self.text = text }
}

// MARK: - NonLinearClickTracking
struct NonLinearClickTracking {
    var id: String?
    var text: String?
    
    init(dict: [String: Any]) {
        id = xmlAttribute(dict: dict, fieldName: "id")
        text = xmlInternalText(dict: dict, fieldName: "NonLinearClickTracking")
    }
    init(text: String) { self.text = text }
}

// MARK: - NonLinearClickThrough
struct NonLinearClickThrough {
    var text: String?
    
    init(dict: [String: Any]) {
        text = xmlInternalText(dict: dict, fieldName: "NonLinearClickThrough")
    }
    init(text: String) { self.text = text }
}

func xmlInternalText(dict: [String: Any], fieldName: String) -> String? {
    if let text = dict["xml_internal_text"] as? String {
        return text
    } else if let text = dict[fieldName] as? String {
        return text
    }
    return nil
}

func xmlAttribute(dict: [String: Any], fieldName: String) -> String? {
    if let attributeDic = dict["xml_attribute"] as? [String: Any] {
        if let text = attributeDic[fieldName] as? String {
            return text
        }
    }
    return nil
}
