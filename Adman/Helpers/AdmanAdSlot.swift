//
//  AdmanSlot.swift
//  Adman_swift
//
//  Created by Barnet Birday on 19/01/2022.
//

import Foundation

public enum AdmanAdSlot: Int {
    case preroll,
         midroll,
         postroll,
         adSlotDefault
}

func admanAdSlotString(slot: AdmanAdSlot) -> String {
    switch (slot) {
    case .preroll:
        return "preroll"
    case .midroll:
        return "midroll"
    case .postroll:
        return "postroll";
    default:
        return "instreamatic";
    }
}
