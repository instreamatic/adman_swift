//
//  AdmanFormat.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

public enum AdmanType: Int {
    case any = 1,
         audioOnly = 7,
         audioPlus = 6,
         voice = 12,
         voiceVideo,
         voiceMultipleAds
}

func admanTypeString(type: AdmanType) -> String {
    switch (type) {
    case .any:
        return "any"
    case .voiceMultipleAds:
        return "any"
    case .voice:
        return "voice"
    case .voiceVideo:
        return "voice-video"
    default:
        return "audio"
    }
}
