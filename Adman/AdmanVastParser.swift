//
//  AdmanVastParser.swift
//  Adman_swift
//
//  Created by Barnet Birday on 19/01/2022.
//

import Foundation
import UIKit

class AdmanVastParser: NSObject, XMLParserDelegate {
    
    public let TextKey = "xml_internal_text"
    public let AttributesKey = "xml_attribute"
    public var attributesAsStandaloneDic = true
    var admanStructure = [[String: Any]]()
    public var delegate: AdmanVastParserDelegate?
    var dicStack = [NSMutableDictionary]()
    var textInProcess = ""
    var xml = ""
    
    var adsList = [AdmanBannerWrapper]()
    var attributes = [[String: String]]()
    var rootTracking = true
    var outString = ""
    var companionTracking = [String: [String]]()
    var staticResourceSize = ""
    var response: AdmanInteraction?
    var elementName = ""
    var dataTask: URLSessionDataTask?
    
    var isLink = false
    var isPhone = false
    var stepID = ""
    var typeID: String?
    
    override init() {}
    
    func sendRequest(url: String, params: [String: String], isInBackground: Bool, onComplete: ((_ ads: [AdmanBannerWrapper]) -> Void)?) {
        var urlComponent: URLComponents = URLComponents(string: url)!
        
        var queryItems = [URLQueryItem]()
        for key in params.keys {
            queryItems.append(URLQueryItem(name: key, value: params[key]))
        }
        urlComponent.queryItems = queryItems
        
        let sessionConfig = URLSessionConfiguration.default
        var request = URLRequest(url: urlComponent.url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: sessionConfig)
        dataTask = session.dataTask(with: request) { [weak self] data, response, error in
            if let data = data {
                print("VAST received from ad server: ", response?.url?.absoluteString ?? "nothing received")
                
                let parser = XMLParser(data: data)
                parser.delegate = self
                if parser.parse() == true {
                    if let item = self!.adsList.last {
                        if let url = item.redirectUrl {
                            self!.sendRequest(url: url, params: params, isInBackground: isInBackground) { _ in }
                        } else {
                            if isInBackground {
                                onComplete?(self?.adsList ?? [])
                            } else {
                                self!.delegate?.creativeDidParsed(ads: self!.adsList)
                            }
                        }
                    }
                }
            } else if let error = error {
                print(error)
            }
        }
        dataTask?.resume()
    }
    
    func cancel() { dataTask?.cancel() }
    /*
    func sendWrapperRequest(params: [String: String]) {
        if let vast = admanStructure.last,
           let vastEl = vast["VAST"] as? [String: Any],
           let adEl = vastEl["Ad"] as? [String: Any],
           let wrap = adEl["Wrapper"] as? [String: Any],
           let wrapUrl = wrap["VASTAdTagURI"] as? String {
            sendRequest(url: wrapUrl, params: params)
        } else {
            delegate?.creativeDidParsed(admanStructure: admanStructure) }
    }*/
}




/*
 extension AdmanVastParser {
 static func makeDic(data: Data) -> [String:Any]? {
 let parser = AdmanVastParser()
 return parser.makeDic(data: data)
 }
 
 static func makeDic(string: String) -> [String:Any]? {
 if let data = string.data(using: .utf8) {
 let parser = AdmanVastParser()
 return parser.makeDic(data: data)
 } else {
 return nil
 }
 }
 }
 
 // MARK: - XML To Dic
 extension AdmanVastParser {
 func makeDic(data: Data) -> [String:Any]? {
 
 //reset
 dicStack = [NSMutableDictionary]()
 textInProcess = ""
 dicStack.append(NSMutableDictionary())
 
 let parser = XMLParser(data: data)
 parser.delegate = self
 if parser.parse() == true {
 return dicStack.first as? [String : Any]
 } else {
 return nil
 }
 }
 }
 
 // MARK: - XMLParserDelegate
 extension AdmanVastParser {
 
 public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
 
 //get parent
 guard let parentDic = dicStack.last else {
 fatalError("should not be nil")
 }
 
 // generate current
 let childDic = NSMutableDictionary()
 if attributesAsStandaloneDic {
 //transformed attributes as a standalone dictionary
 //it will help us transform it back to xml
 if attributeDict.count > 0 {
 childDic.setObject(attributeDict, forKey: AttributesKey as NSString)
 }
 } else {
 //attributes as key,value pair
 childDic.addEntries(from: attributeDict)
 }
 
 // if element name appears more than once, they need to be grouped as array
 if let existingValue = parentDic[elementName] {
 
 let array: NSMutableArray
 
 if let currentArray = existingValue as? NSMutableArray {
 array = currentArray
 } else {
 //If there is no array, create a new array, add the original value
 array = NSMutableArray()
 array.add(existingValue)
 //Replace the original value with an array
 parentDic[elementName] = array
 }
 //Add a new element to the array
 array.add(childDic)
 
 } else {
 //unique element, inserted into parent
 parentDic[elementName] = childDic
 dicStack[dicStack.endIndex-1] = parentDic
 }
 
 //add to stack, track it
 dicStack.append(childDic)
 }
 
 public func parser(_ parser: XMLParser, foundCharacters string: String) {
 let append = string.trimmingCharacters(in: .whitespacesAndNewlines)
 textInProcess.append(append)
 }
 
 public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
 
 //Get the dic representing the current element
 guard let value = dicStack.last else {
 fatalError("should not be nil")
 }
 let parent = dicStack[dicStack.endIndex - 2]
 
 if textInProcess.count > 0 {
 if value.count > 0 {
 value[TextKey] = textInProcess
 } else {
 //If the current element has only value, no Attributes, like <list> 1 </ list>
 //Replace the dictionary directly with string
 if let array = parent[elementName] as? NSMutableArray {
 //parent now looks like： {"list" : [1,{}]}
 //Replace the empty dictionary with a string
 array.removeLastObject()
 array.add(textInProcess)
 } else {
 //parent now looks like： {"list" : {} }
 //Replace the empty dictionary with a string
 parent[elementName] = textInProcess
 }
 }
 } else {
 //If value is empty and the element has no Attributes, delete the node
 if value.count == 0 {
 parent.removeObject(forKey: elementName)
 }
 }
 
 //reset
 textInProcess = ""
 //Finished processing the current element, pop
 dicStack.removeLast()
 }
 
 public func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
 print(parseError)
 }
 }*/

extension AdmanVastParser {
    
    func parserDidStartDocument(_ parser: XMLParser) {
        //adsList = [AdmanBannerWrapper]()
        attributes = [[String: String]]()
        rootTracking = true
        
        if PARSER_DEBUG_ENABLED != 0 { print("VAST parser started") }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.elementName = elementName
        if attributeDict.count > 0 {
            attributes.append(attributeDict)
        } else { attributes.append([String: String]()) }
        
        if elementName == "Companion" { companion(attributeDict: attributeDict) }
        if elementName == "Tracking" { tracking(attributeDict: attributeDict) }
        if elementName == "Response" { response(attributeDict: attributeDict) }
        if elementName == "Value" { value(attributeDict: attributeDict) }
        if elementName == "Responses" { responses(attributeDict: attributeDict) }
        if elementName == "Ad" { ad(attributeDict: attributeDict) }
        if elementName == "Transitions" { transitions(attributeDict: attributeDict) }
        if elementName == "Transition" { transition(attributeDict: attributeDict) }
    }
    
    func companion(attributeDict: [String: String]) {
        rootTracking = false
        staticResourceSize = (attributeDict["height"] ?? "") + "x" + (attributeDict["width"] ?? "")
    }
    
    func tracking(attributeDict: [String: String]) {
        if let event = attributeDict["event"] {
            if !rootTracking && companionTracking[event] == nil {
                companionTracking[event] = [String]()
            }
        }
    }
    
    func response(attributeDict: [String: String]) {
        if let action = attributeDict["action"],
           let type = attributeDict["type"] {
            let action = AdmanInteraction(action: action, type: type)
            action.priority = Int(attributeDict["priority"] ?? "0")!
            var response: [AdmanInteraction]? = adsList.last?.responses[attributeDict["action"]!]
            if response != nil { response?.append(action) } else {
                adsList.last?.responses[attributeDict["action"]!] = [action]
            }
            self.response = adsList.last?.responses[attributeDict["action"]!]?.last
        }
    }
    
    func value(attributeDict: [String: String]) {
        if attributeDict["type"] == "tracking_events" { return }
        if attributeDict["type"] == "media" { return }
        
        typeID = nil
        if attributeDict["type"] == "repeat" {
            if let media = adsList.last?.media {
                for i in 0...media.count-1 {
                    var item = media[i]
                    if item.stepId == stepID {
                        item.typeId = attributeDict["type"]
                        break
                    }
                }
            }
            return
        }
        if attributeDict["type"] == "link" {
            response?.values.append(AdmanResponseValue(type: attributeDict["type"]!, data: nil))
            isLink = true
            return
        }
        if attributeDict["type"] == "phone" {
            isPhone = true
            response?.values.append(AdmanResponseValue(type: attributeDict["type"]!, data: nil))
            return
        }
        if let type = attributeDict["type"] {
            response?.values.append(AdmanResponseValue(type: type, data: nil))
        }
    }
    
    func transition(attributeDict: [String: String]) {
        if let actionObj = attributeDict["action"] {
            let append = ":" + actionObj
            stepID = stepID + append
            let action = AdmanInteraction(action: actionObj, type: actionObj)
            action.priority = Int(attributeDict["priority"] ?? "0") ?? 0
            if var response = adsList.last?.responses[actionObj] {
                response.append(action)
            } else {
                adsList.last?.responses[actionObj] = [action]
            }
            response = adsList.last?.responses[actionObj]?.last
        }
    }
    
    func responses(attributeDict: [String: String]) {}

    func transitions(attributeDict: [String: String]) {}
    
    func responseUrlEnd() {
        if outString.contains("http") { return }
        let item = ResponseUrl(url: outString, stepId: stepID)
        if adsList.last?.responseUrls.count ?? 0 < 1 {
            adsList.last?.responseUrls = [ResponseUrl]()
        }
        adsList.last?.responseUrls.append(item)
    }
    
    func ad(attributeDict: [String: String]) {
        adsList.append(AdmanBannerWrapper())
        if attributeDict["id"] != nil {
            adsList.last?.adId = attributeDict["id"]
        }
    }
        
    
    // 2
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "AdSystem" { adSystemEnd() }
        if elementName == "Ad" { adEnd() }
        if elementName == "ClickTracking" { clickTrackingEnd() }
        if elementName == "CompanionClickThrough" { companionClickThroughEnd() }
        if elementName == "CompanionClickTracking" { companionClickTrackingEnd() }
        if elementName == "ClickThrough" { clickThroughEnd() }
        if elementName == "Impression" { impressionEnd() }
        if elementName == "Error" { errorEnd() }
        if elementName == "Tracking" { trackingEnd() }
        if elementName == "Duration" { durationEnd() }
        if elementName == "Extension" { extensionEnd() }
        if elementName == "Companion" { companionEnd() }
        if elementName == "Expires" { expiresEnd() }
        if elementName == "StaticResource" { staticResourceEnd() }
        if elementName == "MediaFile" { mediaFileEnd() }
        if elementName == "VASTAdTagURI" { VASTAdTagURIEnd() }
        if elementName == "Response" { responseEnd() }
        if elementName == "Value" { valueEnd() }
        if elementName == "Keyword" { keywordEnd() }
        if elementName == "Expression" { expressionEnd() }
        if elementName == "Transition" { transitionEnd() }
        if elementName == "ResponseUrl" { responseUrlEnd() }
        
        if attributes.count > 0 { attributes.removeLast() }
    }
    
    func adSystemEnd() {
        adsList.last?.adSystem = outString
    }
    
    func adEnd() {
        if adsList.last?.responses.count ?? 0 > 0 && adsList.last?.responses["unknown"] != nil {
            var responsesUnknown = [AdmanInteraction]((adsList.last?.responses["unknown"])!)
            responsesUnknown.sort(by: { $0.priority > $1.priority })
            adsList.last?.responses["unknown"] = responsesUnknown
            //adsList.last?.responses["unknown"]!.sort(using: [desc])
        }
        
        let mediaitem = adsList.last!.media.first
        adsList.last?.sourceUrl = mediaitem?.url
        //EXPERIMENT video_ad
        //adsList.last?.videoAd = true
        
        for mediaitem in adsList.last!.media {
            if mediaitem.type.contains(Adman.defaultAudioFormat) && mediaitem.bitrate == Adman.defaultBitrate {
                adsList.last?.sourceUrl = mediaitem.url
                adsList.last?.bitrate = mediaitem.bitrate
            }
        }
        if adsList.last?.sourceUrl == nil {
            let mediaitem = getDefaultMediafile(mediaFiles: adsList.last!.media)
            adsList.last?.sourceUrl = mediaitem.url
            adsList.last?.bitrate = mediaitem.bitrate
        }
            
        if adsList.last?.companionAds[Adman.defaultImageSize] != nil {
            adsList.last?.urlToNavigateOnClick = adsList.last?.companionAds[Adman.defaultImageSize]?.urlToNavigateOnClick
        }

        if adsList.last?.responseTime == 0 { adsList.last?.responseTime = -1 }
        if adsList.last?.responseDelay == 0 { adsList.last?.responseDelay = -1 }
    }
    
    func clickTrackingEnd() {
        adsList.last?.statData.save(url: outString, event: AdmanStatEventClickTracking)
    }

    func companionClickThroughEnd() {
        adsList.last?.companionAds[staticResourceSize]?.urlToNavigateOnClick = outString
    }
    
    func companionClickTrackingEnd() {
        if companionTracking["clickTracking"] == nil {
            companionTracking["clickTracking"] = [String]()
        }
        companionTracking["clickTracking"]?.append(outString)
        adsList.last?.companionAds[staticResourceSize]?.tracking = companionTracking
    }
    
    func clickThroughEnd() {
        adsList.last?.urlToNavigateOnClick = outString
    }
    
    func impressionEnd() {
        adsList.last?.statData.save(url: outString, event: AdmanStatEventImpression)
    }
    
    func errorEnd() {
        adsList.last?.statData.save(url: outString, event: AdmanStatEventError)
    }

    func trackingEnd() {
        let event = (attributes.last?["event"])!
        if rootTracking { adsList.last?.statData.save(url: outString, event: event) } else {
            companionTracking[event]?.append(outString)
        }
    }
    
    func durationEnd() {
        adsList.last?.duration = parseDuration(t: outString)
    }

    func extensionEnd() {
        let extensionType = attributes.last?["type"]
        if extensionType == nil { return }
            
        if extensionType == "ResponseTime" { adsList.last?.responseTime = Int(outString)! }
        if extensionType == "ResponseMicOnSound" { adsList.last?.assets[AdmanBannerAssetMicOnUrl] = outString }
        if extensionType == "ResponseMicOffSound" { adsList.last?.assets[AdmanBannerAssetMicOffUrl] = outString }
        if extensionType == "ResponseDelay" { adsList.last?.responseDelay = Int(outString)! }
        if extensionType == "ResponseLanguage" { adsList.last?.responseLang = outString }
        if extensionType == "IntroAudio" { adsList.last?.introUrl = outString }
        if extensionType == "ResponseUrl" { adsList.last?.responseURL = outString }
        if extensionType == "linkTxt" { adsList.last?.adText = outString }
        //else if ([extensionType isEqualToString:@"linkTxt"])
          //  [_adsList.lastObject setAdText:[_outString kv_encodeHTMLCharacterEntities]];
        if extensionType == "AdId" { adsList.last?.crossServerAdID = outString }
        if extensionType == "Expires" { adsList.last?.expirationTime = Int(outString)! }
        if extensionType == "controls" { adsList.last?.showControls = (Int(outString)! != 0) }
        if extensionType == "isClickable" { adsList.last?.isClickable = (Int(outString)! != 0) }
    }

    func companionEnd() {
        staticResourceSize = ""
        rootTracking = true
        companionTracking = [String: [String]]()
    }
    
    func expiresEnd() {
        adsList.last?.expirationTime = Int(outString)!
    }

    func staticResourceEnd() {
        adsList.last?.companionAds[staticResourceSize] = AdmanCreative()
        adsList.last?.companionAds[staticResourceSize]?.tracking = companionTracking

        adsList.last?.companionAds[staticResourceSize]?.staticResource = outString
        if staticResourceSize == Adman.defaultImageSize { adsList.last?.url = outString }
    }
    
    func mediaFileEnd() {
                
        let mediaItem = AdmanMediaFile(url: outString, type: attributes.last?["type"] ?? "", bitrate: parseBitrate(bitrate: attributes.last?["bitrate"]), mediaId: attributes.last?["id"] ?? "", stepId: stepID, orientation: attributes.last?["orientation"] ?? "", intro: attributes.last?["intro"] ?? "", outro: attributes.last?["outro"] ?? "")
                
        //EXPERIMENT
        /*
        let mediaItem = AdmanMediaFile(url: outString, type: (attributes.last?["type"])!, bitrate: parseBitrate(bitrate: (attributes.last?["bitrate"])!))
        */
        /*
        if response != nil {
            response?.values.append(AdmanResponseValue(type: "media", data: outString)) } else {
                adsList.last?.media.append(mediaItem)
        }*/
        if response != nil {
            response?.values.append(AdmanResponseValue(type: "media", data: outString))
        }
        adsList.last?.media.append(mediaItem)
    }
    
    func VASTAdTagURIEnd() {
        adsList.last?.redirectUrl = outString
    }
    
    func responseEnd() {
        response = nil
    }

    func transitionEnd() {
        let components = stepID.components(separatedBy: ":")
        stepID = ""
        for i in 0...components.count - 2 {
            let item = components[i]
            if item.count > 0 {
                let append = ":" + item
                stepID = stepID + append
            }
        }
        response = nil;
    }
    
    func valueEnd() {
        if isLink {
            for i in 0...(adsList.last?.media.count ?? 0) - 1 {
                var media = adsList.last?.media[i]
                if media?.stepId == stepID {
                    media?.link = outString
                }
            }
            isLink = false
        }
            
        if isPhone {
            for i in 0...(adsList.last?.media.count ?? 0) - 1 {
                var media = adsList.last?.media[i]
                if media?.stepId == stepID {
                    media?.phone = outString
                }
            }
            isPhone = false
        }
        if response?.values.last?.data == nil {
            var value = response?.values.last
            let countL = (response?.values.count ?? 1) - 1
            value?.data = outString
            if let value = value { response?.values[countL] = value }
        }
    }

    func keywordEnd() {
        response?.keywords.append(outString.lowercased())
    }
    
    func expressionEnd() {
        var ar = outString.lowercased().components(separatedBy: ",")
        for i in 0...ar.count-1 {
            ar[i] = ar[i].trimmingCharacters(in: NSCharacterSet.whitespaces)
        }
        if ar.count > 0 {
            response?.keywords.append(contentsOf: ar as [String])
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        attributes = [[String : String]]()
        outString = ""
        elementName = ""
        response = nil
        staticResourceSize = ""
        if PARSER_DEBUG_ENABLED == 1 { print("VAST parser stoped") }
    }
        
    // 3
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        
        outString = string
        
        //let newString = string.components(separatedBy: NSCharacterSet.newlines).joined(separator: " ")
        //outString += newString.trimmingCharacters(in: NSCharacterSet.whitespaces)
        
        
        //[_outString appendFormat:@"%@", string];
        
        
        //NSString *newString = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
          //  [_outString appendFormat:@"%@", [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        //}
        
    }
    
    
    func getDefaultMediafile(mediaFiles: [AdmanMediaFile]) -> AdmanMediaFile {
        for item in mediaFiles {
            if item.type.contains("mp3") || item.type.contains("mpeg") { return item }
        }
        return mediaFiles.first!
    }
    
    func parseDuration(t: String) -> Int {
        let digits = t.components(separatedBy: ":")
        let hours = Int(digits[0])!*3600
        let minutes = Int(digits[1])!*60
        let seconds = Double(digits[2])
        return hours + minutes + Int(seconds ?? 0.0)
    }
    
    func parseBitrate(bitrate: String?) -> Int {
        if let bitrate = bitrate {
            if (bitrate).count > 3 {
                let index = bitrate.index(bitrate.startIndex, offsetBy: 3)
                let bitrateSubStr = bitrate.prefix(upTo: index)
                return Int(bitrateSubStr) ?? 128
            }
            return Int(bitrate) ?? 0
        }
        return 0
    }
}

extension MutableCollection where Self : RandomAccessCollection {
    /// Sort `self` in-place using criteria stored in a NSSortDescriptors array
    public mutating func sort(sortDescriptors theSortDescs: [NSSortDescriptor]) {
        sort { by:
            for sortDesc in theSortDescs {
                switch sortDesc.compare($0, to: $1) {
                case .orderedAscending: return true
                case .orderedDescending: return false
                case .orderedSame: continue
                }
            }
            return false
        }

    }
}

extension Sequence where Iterator.Element : AnyObject {
    /// Return an `Array` containing the sorted elements of `source`
    /// using criteria stored in a NSSortDescriptors array.

    public func sorted(sortDescriptors theSortDescs: [NSSortDescriptor]) -> [Self.Iterator.Element] {
        return sorted {
            for sortDesc in theSortDescs {
                switch sortDesc.compare($0, to: $1) {
                case .orderedAscending: return true
                case .orderedDescending: return false
                case .orderedSame: continue
                }
            }
            return false
        }
    }
}
