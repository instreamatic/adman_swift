//
//  AdmanMediaFile.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

struct AdmanMediaFile {
    var bitrate: Int
    var type: String
    var url: String
    var orientation: String?
    var outro: String?
    var intro: String?
    var mediaId: String?
    var stepId = ""
    var typeId: String?
    var link: String?
    var phone: String?
 
    init(url: String, type: String, bitrate: Int, mediaId: String, orientation: String, intro: String, outro: String) {
        self.url = url
        self.type = type
        self.bitrate = bitrate
        self.orientation = orientation
        self.outro = outro
        self.intro = intro
        self.mediaId = mediaId
    }
    
    init(url: String, type: String, bitrate: Int, mediaId: String, stepId: String, orientation: String, intro: String, outro: String) {
        self.url = url
        self.type = type
        self.bitrate = bitrate
        self.orientation = orientation
        self.outro = outro
        self.intro = intro
        self.mediaId = mediaId
        self.stepId = stepId
    }
    
    init(url: String, type: String, bitrate: Int) {
        self.url = url
        self.type = type
        self.bitrate = bitrate
    }
}
