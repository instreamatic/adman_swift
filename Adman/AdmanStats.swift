//
//  AdmanStats.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

class AdmanStats {
    
    var isReported = false
    var links = [String]()
    var region = AdmanRegion.EU
    var items = [String: AdmanStatRequest]()
    var badRequests = [Date]()
    
    //func getServer() -> AdmanRegion {
    //    return AdmanRegion.AdmanRegionDemo
    //}
    
    func save(url: String, event: String) {
        if items[event] == nil {
            items[event] = AdmanStatRequest()
        }
        items[event]?.addURL(URL: url)
    }
    
    func setRegion(region: AdmanRegion) {
        self.region = region
    }
    
    func saveFor(event: String?, point: String?, admanId: Int?, siteId: Int?, playerId: Int?, campaignId: String?, bannerId: Int?, deviceId: String?) {
        var q = [String: String]()
        q[MODEL_KEY] = INSTREAMATIC_MOBILE_IOS_VALUE
        if let admanId = admanId { q[ADMAN_ID_KEY] = String(admanId) }
        if let siteId = siteId { q[SITE_ID_KEY] = String(siteId) }
        if let playerId = playerId { q[PLAYER_ID_KEY] = String(playerId) }
        if let campaignId = campaignId { q[CAMPAIGN_ID_KEY] = campaignId }
        if let bannerId = bannerId { q[BANNER_ID_KEY] = String(bannerId) }
        if let deviceId = deviceId { q[DEVICE_ID_KEY] = String(deviceId) }
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: q)
            let queryString = String(data: jsonData, encoding: .utf8)
            let data: Data = queryString!.data(using: .utf8)!
            let base64encodedString = data.base64EncodedString().addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            
            var regionPrefix = ""
            if region == .demo { regionPrefix = "xs/" }
            
            let url = INSTREAMATIC_STAT_URL_PART_1 + getServerStatForRegion(region: region) +
            INSTREAMATIC_STAT_URL_PART_2 + regionPrefix +
            INSTREAMATIC_STAT_URL_PART_3 + (point ?? "") +
            INSTREAMATIC_STAT_URL_PART_4 + (base64encodedString ?? "")
                        
            let components: URLComponents = URLComponents.init(string: url)!
            save(url: components.url!.absoluteString, event: event!)
        } catch {}
    }
    
    func reportEvent(eventName: String) {
        let request = items[eventName]
        print("Reporting event: " + eventName)
        request?.report()
        request?.onComplete = {
            print("libAdman.stats: event " + eventName + " reported successfully")
        }
        request?.onFail = { error in
            print("libAdman.stats: failed to report event " + eventName + ", message: " + error.localizedDescription)
        }
    }
    
    func reportEvent(event: String, companion: AdmanCreative) {
        if companion.tracking != nil && companion.tracking?[event] != nil {
            let request = AdmanStatRequest(URIs: (companion.tracking?[event])!)
            request.report()
        }
    }
}
