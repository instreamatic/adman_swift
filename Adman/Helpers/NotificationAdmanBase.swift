//
//  NotificationAdmanControl.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

struct NotificationAdmanBase {
    
    var event: AdmanMessageEventType?
    
    init(event: AdmanMessageEventType) {
        self.event = event
    }
    
    init() {}
    
    static func sendEvent(event: AdmanMessageEventType) {
        let message = NotificationAdmanBase(event: event)
        var dict = [String: Any]()
        dict[NOTIFICATION_ADMAN_MESSAGE_KEY] = message
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_ADMAN_MESSAGE), object: nil, userInfo: dict)
    }
    
    static func getEventString(event: AdmanMessageEventType) -> String {
        switch (event) {
        case .fetchingInfo:
            return "AdmanMessageEventFetchingInfo (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .preloading:
            return "AdmanMessageEventPreloading (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .none:
            return "AdmanMessageEventNone (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .error:
            return "AdmanMessageEventError (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .ready:
            return "AdmanMessageEventReady (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .started:
            return "AdmanMessageEventStarted (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .stopped:
            return "AdmanMessageEventStopped (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .completed:
            return "AdmanMessageEventCompleted (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .skipped:
            return "AdmanMessageEventSkipped (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        default:
            return "Unknown";
        }
    }
}
