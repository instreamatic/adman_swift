//
//  AdmanVastParserDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

protocol AdmanVastParserDelegate {
    //func creativeDidParsed(admanStructure: [[String: Any]])
    func creativeDidParseRedirect(redirectURL: String) -> String
    func creativeDidFail(error: Error)
    func creativeDidParsed(ads: [AdmanBannerWrapper])
}
