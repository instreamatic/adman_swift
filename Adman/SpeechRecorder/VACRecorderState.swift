//
//  VACRecorderState.swift
//  Adman_swift
//
//  Created by Barnet Birday on 25/02/2022.
//

import Foundation
import AVFAudio

class VACRecorderState {
    var dataFormat = AudioStreamBasicDescription()
    var queue: AudioQueueRef?
    var buffers = [AudioQueueBufferRef?]()
    var audioFile: AudioFileID?
    var bufferByteSize: Int64?
    var currentPacket: Int64?
    var running = false
    var recordPackets: Int64 = 0
}
