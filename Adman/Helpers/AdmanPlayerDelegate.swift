//
//  AdmanPlayerDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 01/02/2022.
//

import Foundation

protocol AdmanPlayerDelegate: NSObject {

    func itemDidLoad(adID: Int, itemType: AdmanPlayerItemType)
    func itemPlaybackDidFinish(adID: Int, itemType: AdmanPlayerItemType)
    func itemPlaybackDidFail(adID: Int, itemType: AdmanPlayerItemType, err: Error?)
    func log(data: String?)
    func timeControlStatusWaitingToPlayAtSpecifiedRate(state: Bool)
}
