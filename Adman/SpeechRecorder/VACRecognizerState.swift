//
//  VACRecognizerState.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

enum VACRecognizerState: Int {
    case none,
         ready,
         listening
}
