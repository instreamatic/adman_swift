//
//  AdmanBannerWrapper.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

open class AdmanBannerWrapper: AdmanBanner {
    var crossServerAdID: String?
    var responseDelay = 0
    var responseURL: String?
    var responseLang: String?
    var redirectUrl: String?
    var sequence = 0
    var videoAd = false
}
