//
//  Adman_swift
//
//  Created by Barnet Birday on 17/01/2022.
//

import Foundation
import UIKit
import Network
import CoreMedia
import AVFAudio
import EventKit
import AVFoundation
//import CoreTelephony

open class Adman: NSObject, VACRecognizerDelegate, AdmanDtmfDelegate, AdmanPlayerDelegate, AdmanVastParserDelegate, UIGestureRecognizerDelegate {
    
    public var delegate: AdmanDelegate?
    public var state: AdmanState?
    var fallbackState: AdmanState?
    public var error: Error?
    open var adsList = [AdmanBannerWrapper]()
    var activeAd = 0
    var mute = false
    var volume: Float = 0.0
    //static var elapsedTime: Int64?
    //static var elapsedTimeString: String?
    //static var estimatedTime: Int64?
    //static var estimatedTimeString: String?
    var progress: Double = 0.0
    var userId: String?
    var playerId = DEFAULT_PLAYER_ID
    var transcriptedPhrase: String?
    var statistic = AdmanStats()
    var admanProperties = AdmanProperties()
    var intentTapped = false
    var recognizer = VACRecognizer()
    var vastLiveDurationTimer: Timer?
    open var player = AdmanPlayer()
    var playbackProgressTimer: Timer?
    var voiceResponseDelayTimer: Timer?
    var interactionTimeoutTimer: Timer?
    var endSpeechDetectionTimer: Timer?
    var networkStatus = NWPathMonitor()
    let vastLoader = AdmanVastParser()
    var dtmfListener: AdmanWebsocketDtmfListener?
    var cache = [String: Any]()
    var unknownPhraseIteration = 0
    var allowPreload = true
    var silentPreload = false
    var isUnknown = false
    var admanError: Error?
    //static var vastStructure = [[String: Any]]()
    //static var adList: [CreativeElement]?
    //static var modelsArray = [VastElement]()
    var firstQuarterReported = false
    var secondQuarterReported = false
    var thirdQuarterReported = false
    static var defaultImageSize = "640x640"
    
    static var defaultAudioFormat = "mp3"
    
    static var defaultBitrate = 128
    var campaignId = 0
    var creativeId = 0
    var playerBufferState: Int?
    var currentStepID = ""
    
    private override init() {
        guard state != nil else {
            fatalError("Error - you must call setup init(parameters) before accessing Adman")
        }
    }
    
    //public static let shared = Adman()
    
    public init(siteId: Int) {
        super.init()
        initWith(siteId: siteId, testMode: DEFAULT_TEST_MODE, zoneId: DEFAULT_ZONE_ID, region: DEFAULT_ADMAN_REGION, playerId: DEFAULT_PLAYER_ID)
    }
    
    public init(siteId: Int, testMode: Bool) {
        super.init()
        initWith(siteId: siteId, testMode: testMode, zoneId: DEFAULT_ZONE_ID, region: DEFAULT_ADMAN_REGION, playerId: DEFAULT_PLAYER_ID)
    }
    
    public init(siteId: Int, testMode: Bool, playerId: Int) {
        super.init()
        initWith(siteId: siteId, testMode: testMode, zoneId: DEFAULT_ZONE_ID, region: DEFAULT_ADMAN_REGION, playerId: playerId)
    }
    
    public init(siteId: Int, testMode: Bool, zoneId: Int) {
        super.init()
        initWith(siteId: siteId, testMode: testMode, zoneId: zoneId, region: DEFAULT_ADMAN_REGION, playerId: playerId)
    }
    
    public init(siteId: Int, region: AdmanRegion, playerId: Int, testMode: Bool) {
        super.init()
        initWith(siteId: siteId, testMode: testMode, zoneId: DEFAULT_ZONE_ID, region: region, playerId: DEFAULT_PLAYER_ID)
    }
    
    public init(siteId: Int, region: AdmanRegion, testMode: Bool) {
        super.init()
        initWith(siteId: siteId, testMode: testMode, zoneId: DEFAULT_ZONE_ID, region: region, playerId: DEFAULT_PLAYER_ID)
    }
    
    private init(url: String) {
        super.init()
        initWith(siteId: DEFAULT_SITE_ID, testMode: DEFAULT_TEST_MODE, zoneId: DEFAULT_ZONE_ID, region: DEFAULT_ADMAN_REGION, playerId: DEFAULT_PLAYER_ID)
        admanProperties.requestURL = url
    }
    
    public init(siteId: Int, testMode: Bool, zoneId: Int, region: AdmanRegion?, playerId: Int) {
        super.init()
        initWith(siteId: siteId, testMode: testMode, zoneId: zoneId, region: region, playerId: playerId)
    }
    
    private func initWith(siteId: Int, testMode: Bool, zoneId: Int, region: AdmanRegion?, playerId: Int) {
        self.playerId = playerId;
        admanProperties.region = region ?? .EU
        admanProperties.siteId = siteId;
        admanProperties.zoneId = zoneId;
        
        if testMode { admanProperties.debugMode = .audio }
        statistic.setRegion(region: admanProperties.region)
        statistic.saveFor(event: AdmanStatEventLoad, point: "load", admanId: 3171, siteId: admanProperties.siteId, playerId: DEFAULT_PLAYER_ID, campaignId: "0", bannerId: 0, deviceId: admanProperties.idfa)
        statistic.saveFor(event: AdmanStatEventRequest, point: "req", admanId: 3171, siteId: admanProperties.siteId, playerId: 0, campaignId: "0", bannerId: 0, deviceId: admanProperties.idfa)
        statistic.saveFor(event: AdmanStatEventFetched, point: "fetched", admanId: 3171, siteId: admanProperties.siteId, playerId: 0, campaignId: "0", bannerId: 0, deviceId: admanProperties.idfa)
        if admanProperties.region != .voice {
            statistic.reportEvent(eventName: AdmanStatEventLoad)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(triggerActionAdmanControl), name: NSNotification.Name(NOTIFICATION_ADMAN_CONTROL), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(triggerActionAdmanPlayer), name: NSNotification.Name(NOTIFICATION_ADMAN_PLAYER), object: nil)
        
        player.currentItem?.preferredForwardBufferDuration = 5.0
        print("preferredForwardBufferDuration: ", player.currentItem?.preferredForwardBufferDuration.description ?? "not initialized")
        player.automaticallyWaitsToMinimizeStalling = false
        allowPreload = true
        
        setState(newState: AdmanState.initial)
        player.delegate = self
        vastLoader.delegate = self
        recognizer.delegate = self
        
        networkStatus.pathUpdateHandler = { path in
            //networkStateDidChange(path: path)
        }
        let queue = DispatchQueue(label: "NetworkStatus")
        networkStatus.start(queue: queue)
    }
    
    public func prepare() {
        if !silentPreload {
            setState(newState: .fetchingInfo)
            NotificationAdmanBase.sendEvent(event: .fetchingInfo)
        }
        intentTapped = false
        isUnknown = false
        player.isPlaying = false
        admanProperties.lastFetchTime = Date()
        let type = admanTypeString(type: admanProperties.admanType)
        let slot = admanAdSlotString(slot: admanProperties.slot)
        let params = "site_id=" + String(admanProperties.siteId) + "&player_id=" + String(playerId) + "&type=" + type + "&slot=" + slot + "&idfa=" + admanProperties.idfa + "&microphone=" + AdmanMicrophone().isMicrophoneEnabledString()
        let can_show = "https://" + getServerStatForRegion(region: statistic.region) + ".instreamatic.com/live/can_show.gif?" + params
        statistic.save(url: can_show, event: AdmanStatEventCanShow)
        requestSection(section: AdmanSectionType.sPreroll)
    }
    
    public func prepare(format: AdmanFormat) {
        prepare(format: format, type: .any, maxDuration: 0, adsCount: 0)
    }
    
    public func prepare(type: AdmanType) {
        prepare(format: .any, type: type, maxDuration: 0, adsCount: 0)
    }
    
    public func prepare(format: AdmanFormat, type: AdmanType) {
        prepare(format: format, type: type, maxDuration: 0, adsCount: 0)
    }
    
    public func prepare(maxDuration: Int, adsCount: Int) {
        prepare(format: .any, type: .any, maxDuration: maxDuration, adsCount: adsCount)
    }
    
    public func prepare(format: AdmanFormat, type: AdmanType, maxDuration: Int, adsCount: Int) {
        unknownPhraseIteration = 0
        transcriptedPhrase = ""
        admanProperties.format = format
        admanProperties.admanType = type
        if adsCount > 0 { admanProperties.maxAdsCount = adsCount } else {
            admanProperties.maxAdsCount = 1
        }
        if maxDuration < 10 { admanProperties.maxAdBlockDuration = 0 } else {
            admanProperties.maxAdBlockDuration = maxDuration
        }
        prepare()
    }
    
    public func setCampaignId(campaignId: Int) {
        self.campaignId = campaignId
        admanProperties.campaignId = campaignId
    }
    
    public func setAdSlot(slot: AdmanAdSlot) {
        admanProperties.slot = slot
    }
    
    public func getAdSlot() -> AdmanAdSlot {
        return admanProperties.slot
    }
    
    public func setCreativeId(creativeId: Int) {
        self.creativeId = creativeId
        //EXPERIMENT
        //admanProperties.creativeId = creativeId
    }
    
    func setCustomRegionServer(adServer: String) {//public
        admanProperties.region = .custom
        admanProperties.customAdServer = adServer
    }
    
    @objc func isValidToPlayback() -> Bool {
        vastLiveDurationTimer?.invalidate()
        vastLiveDurationTimer = nil
        var expires = AD_EXPIRATION_TIME_DEFAULT
        if adsList.count > 0 && adsList[activeAd].expirationTime > 0 { expires = adsList[activeAd].expirationTime }
        guard let lastFetchTime = admanProperties.lastFetchTime else { return false }
        if NSDate().timeIntervalSince(lastFetchTime) > Double(expires) {
            silentPreload = true
            prepare()
            print("adman: VAST expired, revalidation started")
            return false
        }
        return true
    }
    
    open func cancelLoading() {
        vastLoader.cancel()
        resetRecognizerState()
        setState(newState: .adNone)
        NotificationAdmanBase.sendEvent(event: .none)
    }
    
    public func play() {
        if state == .playing { return }

        if vastLiveDurationTimer != nil {
            vastLiveDurationTimer?.invalidate()
            vastLiveDurationTimer = nil
            silentPreload = false
        }

        let session = AVAudioSession.sharedInstance()
        if admanProperties.isAudioSessionSetupEnabled && session.category != AVAudioSession.Category.playAndRecord {
            if #available(iOS 10.1, *) {
                try? session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: [AVAudioSession.CategoryOptions.defaultToSpeaker, AVAudioSession.CategoryOptions.allowBluetoothA2DP, AVAudioSession.CategoryOptions.allowAirPlay])
            } else {
                try? session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: [AVAudioSession.CategoryOptions.defaultToSpeaker])
                try? session.setActive(true)
            }
        }
        if state == .readyForPlayback || state == .adChangedForPlayback {
            // TODO play intro audio
            startPlayback()
            updateProgress()
        } else if state == .speechRecognizerStarted || state == .voiceInteractionStarted {
            startListening()
        } else if state == .voiceResponsePlaying {
            player.play()
        } else {
            print("Error: Invalid internal state " + state.debugDescription + " on playback attempt")
        }
    }
    
    public func pause() {
        if state == .playing || state == .voiceInteractionStarted {
            player.pause()
            stopPlaybackProgressTimer()
            //[_uiManager showPlayButton];
            adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventPause)
            setState(newState: .paused)
            NotificationAdmanPlayer.sendEvent(event: .pause)
        }
    }
    
    public func resume() {
        if state == .paused {
            player.play()
            updateProgress()
            currentAd()?.statData.reportEvent(eventName: AdmanStatEventResume)
            setState(newState: .playing)
            //NotificationAdmanPlayer.sendEvent(event: .playing, userData: currentAd())
            NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
            //[_uiManager showPauseButton];
        }
    }

    func rewind() {
        currentAd()!.statData.reportEvent(eventName: AdmanStatEventRewind)
        player.pause()
        player.seek(to: CMTime(seconds: 0, preferredTimescale: CMTimeScale(NSEC_PER_SEC)))
        player.play()
        setState(newState: .playing)
        //NotificationAdmanPlayer.sendEvent(event: .playing, userData: currentAd())
        NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
        stopPlaybackProgressTimer()
        updateProgress()
    }
    
    func stopPlayer() {
        if currentAd()!.responses.count > 0 {
            recognizer.stop()
            interactionTimeoutTimer?.invalidate()
        }
        if state == .voiceInteractionStarted || state == .playing { player.pause() }
        stopPlaybackProgressTimer()
        //[_uiManager showPlayButton];
    }
    
    open func stop() {
        if (state == .playbackCompleted || state == .error || state == .stopped) { return }
        resetRecognizerState()
        player.pause()
        player.replaceCurrentItem(with: nil)
        setState(newState: .stopped)
        NotificationAdmanBase.sendEvent(event: .stopped)
        currentAd()!.statData.reportEvent(eventName: AdmanStatEventComplete)
    }
    
    public func reportAdEvent(eventName: String) {
        if eventName == AdmanStatEventCanShow { statistic.reportEvent(eventName: eventName) }
        else { currentAd()!.statData.reportEvent(eventName: eventName) }
    }
    
    @objc func logBadRequestCb() {
        allowPreload = true
        print("Preloading enabled")
    }
    
    func overrideIP(ip: String) { admanProperties.clientIP = ip }
    
    public func setRecognitionBackend(backend: AdmanRecognitionBackend) { admanProperties.backend = backend }

    public func setServerVoiceActivityDetection(state: Bool) { admanProperties.serverVoiceActivityDetection = state }

    public func setPreloadMode(mode: AdmanPreloadMode) { player.setPreloadMode(mode: mode) }
    
    func getActiveAd() -> AdmanBanner? { return currentAd() }
    
    open func getActivePlayer() -> AdmanPlayer { return player }
    
    func enableDtmfAdsDetectionForStation(name: String, preload: Bool, vc: UIViewController) {
        /////////// [_uiManager setBannerParentVC:vc]; NOT IMPLEMENTED IN OBJ-C
        admanProperties.dtmfAdsDetection = true
        admanProperties.dtmfStationKey = name
        dtmfListener = AdmanWebsocketDtmfListener()
        //_dtmfListener.delegate = self;
        //[_dtmfListener start:_properties.dtmfStationKey];
        if preload { prepareWithDelay() }
    }
    
    func preloadIntro() {
        //NOT IMPLEMENTED IN OBJ-C
    }
    
    func disableDtmfAdsDetection() {
        //[_properties setDtmfAdsDetection:NO];
        //[_dtmfListener stop];
        //_dtmfListener = nil;
    }
    
    public func setDefaultBannerSize(size: String) { Adman.defaultImageSize = size }
    func getDefaultBannerSize() -> String { return Adman.defaultImageSize }
    func setDefaultAudioFormat(format: String, bitrate: Int?) {
        Adman.defaultAudioFormat = format
        Adman.defaultBitrate = (bitrate ?? 0) > 0 ? (bitrate ?? 0) : 128
    }
        
    public func currentRegion() -> AdmanRegion { return admanProperties.region }
    func siteId() -> Int { return 0 }
    
    public func getVersion() -> String { return "version" }
    func setState(newState: AdmanState) {
        if newState != state {
            if (newState != .error) {
                fallbackState = newState
            }
            state = newState
            delegate?.admanStateDidChange(sender: self)
        }
    }
    
    func setMute(mute: Bool) {
        self.mute = mute
        if mute { player.volume = 0.0 } else { player.volume = Float(volume) }
    }
    
    func buildAdmanRequest(params: AdmanProperties, cache: Bool) {
        var path = String()
        var supportedVASTVersion = VAST_VERSION_SUPPORT
        if admanProperties.region == .US { supportedVASTVersion = 2 }
        if admanProperties.region != .voice && admanProperties.region != .demo && admanProperties.region != .demoEu {
            path = "https://" + getServerForRegion(region: admanProperties.region, admanProperties: admanProperties) + "/v" + String(supportedVASTVersion) + "/vast"
            path += "/" + String(admanProperties.siteId)
        } else {
            path = "https://" + getServerForRegion(region: admanProperties.region, admanProperties: admanProperties) + "/x/v" + String(supportedVASTVersion) + "/vast"
            path += ".xml"
        }
        if cache {
            self.cache["requestURL"] = path
            self.cache["requestParams"] = AdmanProperties().requestParameters(admanProperties: admanProperties)
        }
    }
    
    func requestSection(section: AdmanSectionType) {
        admanError = nil;
        if !allowPreload {
            setState(newState: .adNone)
            print("Ad requests disabled for this Adman instance")
            return
        }
        buildAdmanRequest(params: admanProperties, cache: true) //Where do we need to store the result?
        if admanProperties.region != .voice {
            statistic.reportEvent(eventName: AdmanStatEventRequest)
        }
        print("Adman version: " + AdmanVersion)
        guard let requestURL = admanProperties.requestURL else {
            log(logString: "Start fetching url: " + (cache["requestURL"] as! String) + " with params: " + cache["requestParams"].debugDescription)
            vastLoader.adsList = [AdmanBannerWrapper]()
            vastLoader.sendRequest(url: cache["requestURL"] as! String, params: cache["requestParams"] as! [String : String], isInBackground: false) { ads in
            }
            return
        }
        log(logString: "Start fetching url: " + requestURL)
        vastLoader.adsList = [AdmanBannerWrapper]()
        vastLoader.sendRequest(url: requestURL, params: [String: String](), isInBackground: false) { ads in
        }
    }
    
    func networkStateDidChange(path: NWPath) {
        if path.status != .satisfied && player.getPreloadMode()  == .progressive {
            setState(newState: .error)
            NotificationAdmanBase.sendEvent(event: .error)
        }
        
        if path.status == .satisfied && player.getPreloadMode()  == .progressive && state == .error {
            if fallbackState == .playbackCompleted {
                state = fallbackState
            } else {
                setState(newState: fallbackState!)
            }
            
            if fallbackState == .playing {
                if CMTimeCompare((player.currentItem!.duration), (player.currentItem?.currentTime())!) == 0 {
                    if (player.currentItem as! AdmanPlayerItem).isPlayed {
                        player.playbackFinished(notification: nil)
                    } else {
                        skip()
                    }
                } else {
                    player.play()
                }
            }
        }
    }
    /*
    func loadItem(adID: Int, itemType: AdmanPlayerItemType) {//itemDidLoad from Adman
        switch (itemType) {
          case .ad:
            currentAd()!.duration = Int(CMTimeGetSeconds(player.currentItem!.duration))
            player.play()
            //NotificationAdmanPlayer.sendEvent(event: .playing, userData: currentAd())
            NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
            setState(newState: .playing)
            log(logString: "Playback started")
        case .reaction, .unknown:
            recognizedIntentLoaded()
        case .micOn, .micOff:
            startPlayer()
        case .initial:
            currentAd()!.duration = Int(CMTimeGetSeconds(player.currentItem!.duration))
            log(logString: "Creative preloaded and ready for playback")
            if !silentPreload {
                setState(newState: .readyForPlayback)
                NotificationAdmanPlayer.sendEvent(event: .ready)
            }
            silentPreload = false
            enableVastExpiration()
        }
    }
    */
    
    open func subtitlesPresent() -> Bool {
        if let playerItem = player.currentItem {
                let group = playerItem.asset.mediaSelectionGroup(forMediaCharacteristic: .legible)
                return (group?.options.first != nil)
        }
        return false
    }
    
    func enableVastExpiration() {
        vastLiveDurationTimer?.invalidate()
        vastLiveDurationTimer = nil
        vastLiveDurationTimer = Timer.scheduledTimer(timeInterval: TimeInterval(currentAd()?.expirationTime ?? 0), target: self, selector: #selector(isValidToPlayback), userInfo: nil, repeats: false)
        RunLoop.main.add(vastLiveDurationTimer!, forMode: .common)
        print("adman: vast lifetime - " + String(currentAd()?.expirationTime ?? 0))
    }
    
    func startPlayer() {
        player.play() }
    
    func setVolume(volume: Float) {
        self.volume = volume
        if mute && volume > 0.0 { mute = false }
        player.volume = volume
    }
    
    func bannerTouched(urlToNavigate: String?) {
        guard let urlToNavigate = urlToNavigate else { return }
        adsList[activeAd].statData.reportEvent(event: AdmanStatEventClickTracking, companion: adsList[activeAd].companionAds[Adman.defaultImageSize]!)
        delegate?.bannerTouched(urlToNavigate: urlToNavigate)
        if !adsList[activeAd].isVoiceAd() { viewClosed() }
        if adsList[activeAd].isVoiceAd() && state == .voiceInteractionStarted {
            adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventResponseOpenBanner)
            processIntent(dict: ["transcript": "Yes", "action": "positive", "playMicOff": "No", "source": "ui"])
        } else { openUrlSafe(url: urlToNavigate, type: AdmanLinkTypeDirect) }
    }
    
    func openUrlSafe(url: String, type: Int) {
        print(TAG + ": Url should be " + url + " opened")
        AdmanBackground().isInBackground(synced: true) { state in
            if state && type != AdmanLinkTypeMobile {
                NotificationAdmanControl.sendEvent(event: .link, userData: URL(string: url))
            } else {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
    public func currentAd() -> AdmanBannerWrapper? {
        if adsList.count > 0,
            activeAd < adsList.count {
            return adsList[activeAd]
        } else {
            return nil
        }
    }
    
    func unknownActionDidPlayed() {
        log(logString: "Unknown intent played")
        playbackProgressTimer?.invalidate()
        playbackProgressTimer = nil
        unknownPhraseIteration += 1
        setState(newState: .voiceInteractionEnded)
        NotificationAdmanVoice.sendEvent(event: .interactionEnded)

        if let actionType = player.userData as? String,
            let voiceAction = currentAd()!.responses[actionType] {
            print("Current voice response iteration: " + String(unknownPhraseIteration) + ":" + String(voiceAction.count))
            
            if voiceAction.count > unknownPhraseIteration && actionType == "unknown" {
                player.userData = nil
                recognizer.start(onStart: {}, options: getWebsocketHeaderOptions())
                startListening()
            } else {
                finishAdPlayback() }
        } else { finishAdPlayback() }
    }
    
    func playUnknownAction(actionType: String) {
        print("Playing interaction for " + actionType)
        guard let ad = currentAd() else { return }
        stopPlayer()
        let isSilence = actionType == "silence"
        if !isSilence && unknownPhraseIteration >= ad.responses[actionType]?.count ?? 0 {
            finishAdPlayback()
            return
        }
        var action: AdmanInteraction?
        if !isSilence { action = ad.responses[actionType]?[unknownPhraseIteration] } else {
            action = ad.responses[actionType]?.first
        }
        if let companion = ad.companionAds[getDefaultBannerSize()] {
            statistic.reportEvent(event: "response_" + actionType, companion: companion)
        }
        player.userData = actionType
        
        var url = action?.values.first?.data
        if isUnknown { currentStepID += ":unknown2" } else
        { currentStepID += ":unknown" }
        
        player.currentStepID = currentStepID
        let media = ad.media
        var item: AdmanMediaFile?
        for i in 0...media.count-1 {
            if media[i].stepId == currentStepID {
                item = media[i]
                url = item?.url
                break
            }
        }
        guard item != nil else {
            finishAdPlayback()
            return
        }
        
        if item?.typeId == "repeat" {
            if isUnknown {
                currentStepID = currentStepID.replacingOccurrences(of: ":unknown2", with: "")
            } else {
                currentStepID = currentStepID.replacingOccurrences(of: ":unknown", with: "")
            }
            player.currentStepID = currentStepID
            isUnknown = true
        }
        
        
        setState(newState: .playing)
        NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
        stopPlaybackProgressTimer()
        updateProgress()
        player.isPlaying = true
        AdmanUIBase.isBlurEnabledValue = item?.typeId ?? "" == "repeat"
        player.addUrl(url: url ?? "", adID: activeAd, itemType: item?.typeId == "repeat" ? .initial : .unknown, isRecognize: false)
    }
    
    func recognizedIntentLoaded() {
        setState(newState: .voiceResponsePlaying)
        NotificationAdmanVoice.sendEvent(event: .responsePlaying, userData: player)
        log(logString: "Playing voice intent")
        player.play()
        updateProgress()
        RunLoop.main.add(playbackProgressTimer!, forMode: .common)
    }
    
    func recognizedIntentPlayed() {
        log(logString: "Voice intent successfully played")
        var valList = [String: Any]()
        if !(player.userData is String) { valList = player.userData as! [String: Any] }
        
        playbackProgressTimer?.invalidate()
        playbackProgressTimer = nil
        
        if let link = valList["link"] as? AdmanResponseValue,
        let linkData = link.data { openUrlSafe(url: linkData, type: AdmanLinkTypeUrl) }
        if let phone = valList["phone"] as? AdmanResponseValue,
        let phoneData = phone.data { openUrlSafe(url: "tel:" + phoneData, type: AdmanLinkTypeMobile) }
        if valList["custom_intent"] as? AdmanResponseValue != nil { delegate?.customVoiceIntentHandler() }
        player.userData = nil
        skip()
    }
    
    func reactionForInteraction(response: AdmanInteraction, source: String?) {
        print("Performing reaction for " + response.action)
        var valList = [String: AdmanResponseValue]()
        for val in response.values { valList[val.type] = val }
        player.userData = nil
        var audio = valList["audio"]
        if audio == nil { audio = valList["media"] }
        let link = valList["link"]
        let phone = valList["phone"]
        let custom_intent = valList["custom_intent"]
        guard let ad = currentAd() else { return }
        if source == nil {
            let companion = ad.companionAds[getDefaultBannerSize()] ?? AdmanCreative()
            statistic.reportEvent(event: "response_" + response.action, companion: companion)
        }
        if let audio = audio {
            stopPlayer()
            setState(newState: .voiceResponsePlaying)
            player.userData = valList
            guard var url = audio.data else { return }
            
            //EXPERIMENT video_ad - AUDIO FLOW
            if !url.hasSuffix("m3u8") {
                NotificationAdmanVoice.sendEvent(event: .responsePlaying)
                player.addUrl(url: url, adID: activeAd, itemType: .reaction, isRecognize: false)
                return
            }
            //EXPERIMENT video_ad - AUDIO FLOW
            
            let curID = currentStepID
            currentStepID += ":" + response.action
            player.currentStepID = currentStepID
            let media = ad.media
            var item: AdmanMediaFile?
            for i in 0...media.count-1 {
                if media[i].stepId == currentStepID {
                    item = media[i]
                    url = item?.url ?? ""
                    break
                }
            }
            if item == nil {
                finishAdPlayback()
                return
            }
            if item?.typeId == "repeat" {
                currentStepID = curID
                player.currentStepID = self.currentStepID
                player.isPlaying = true
                AdmanUIBase.isBlurEnabledValue = true
                player.addUrl(url: url, adID: activeAd, itemType: .initial, isRecognize: false)
            } else {
                var count = 0
                let checkID = currentStepID + ":"
                for i in 0...media.count-1 {
                    let item = media[i]
                    if item.stepId.contains(checkID) {
                        count += 1
                    }
                }
                setState(newState: .playing)
                NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
                stopPlaybackProgressTimer()
                updateProgress()
                player.isPlaying = true
                AdmanUIBase.isBlurEnabledValue = count > 0
                player.addUrl(url: url, adID: activeAd, itemType: count > 2 ? .initial : .unknown, isRecognize: false)
            }
        } else if let link = link {
            openUrlSafe(url: link.data!, type: AdmanLinkTypeUrl)
            skip()
        } else if let phone = phone {
            openUrlSafe(url: "tel:" + phone.data!, type: AdmanLinkTypeMobile)
            skip()
        } else if custom_intent != nil {
            delegate?.customVoiceIntentHandler()
        }
    }
    
    func startDefaultPlayback() {
        
        /*
         NSString *size = [AdmanVastParserDelegate getDefaultImageSize];
         [_admanStats reportEvent:AdmanStatEventStart];
         
         if ((AdmanCurrentAd).companionAds[size].tracking && (AdmanCurrentAd).companionAds[size].tracking[@"creativeView"])
             [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView forCompanion:(AdmanCurrentAd).companionAds[size]];
         else
             [(AdmanCurrentAd).statData reportEvent:AdmanStatEventCreativeView];

         [(AdmanCurrentAd).statData reportEvent:AdmanStatEventImpression];
         
         AdmanBannerWrapper *ad = (AdmanBannerWrapper *) (AdmanCurrentAd);

         if (((AdmanPlayerItem *) _player.currentItem).itemType == AdmanPlayerItemInitial)
             [self itemDidLoaded:_activeAd itemType:AdmanPlayerItemAd];
         else {
             [_player addUrl:ad.sourceUrl adID:_activeAd itemType:AdmanPlayerItemAd isRecognize:NO];
         }
         */
        
        
        
        
        adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventStart)
        let size = Adman.defaultImageSize
        if adsList[activeAd].companionAds[size]?.tracking != nil && ((adsList[activeAd].companionAds[size]?.tracking?["creativeView"]) != nil) {
            adsList[activeAd].statData.reportEvent(event: AdmanStatEventCreativeView, companion: adsList[activeAd].companionAds[size]!)
        } else {
            adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventCreativeView)
        }
        adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventImpression)
        
        let ad = currentAd()
        
        if let currentItem = player.currentItem as? AdmanPlayerItem,
            currentItem.itemType == .initial {
            itemDidLoad(adID: activeAd, itemType: .ad)
        } else {
            player.addUrl(url: ad?.sourceUrl ?? "", adID: activeAd, itemType: .ad, isRecognize: false)
        }
    }
    
    func startPlayback() {//MAYBE NOT FINISHED - NEED TO REVIEW AND TEST!!!
        AdmanBackground().isInBackground(synced: false) { isBack in
            if let currentAd = self.currentAd() {
                if currentAd.isVoiceAd() && isBack && self.networkStatus.currentPath.usesInterfaceType(.wifi) {
                    if var cache = self.cache["requestParams"] as? [String: Any] {
                        cache["type"] = "digital"
                        cache["requestParams"] = cache
                    }
                    print("Trying to play voice ad in background, start preloading digital ad")
                    self.vastLoader.adsList = [AdmanBannerWrapper]()
                    self.vastLoader.sendRequest(url: self.cache["requestURL"] as! String, params: self.cache["requestParams"] as! [String : String], isInBackground: true) { ads in
                        
                        
                        
                        if var cache = self.cache["requestParams"] as? [String: Any] {
                            cache["type"] = "any"
                            self.cache["requestParams"] = cache
                        }
                        if self.adsList.count == 0 {
                            self.startDefaultPlayback()
                            return
                        }
                        self.activeAd = 0
                        self.adsList = ads
                        self.intentTapped = false

                        let size = Adman.defaultImageSize
                        
                        self.statistic.reportEvent(eventName: AdmanStatEventStart)
                        self.statistic.reportEvent(eventName: AdmanStatEventImpression)

                        if let companion = currentAd.companionAds[size] {
                            if companion.tracking != nil && companion.tracking!["creativeView"] != nil {
                                currentAd.statData.reportEvent(event: AdmanStatEventCreativeView, companion: companion)
                            } else {
                                currentAd.statData.reportEvent(eventName: AdmanStatEventCreativeView)
                            }
                        }
                        
                        // Started playback for new digital ad in background
                        AdmanUIBase.isBlurEnabledValue = true
                        self.player.addUrl(url: (self.adsList.first?.sourceUrl)!, adID: self.activeAd, itemType: .ad, isRecognize: false)
                        return
                    }
                } else {
                    self.startDefaultPlayback()
                }
                if self.hasNextAd() {
                    self.preloadAd(index: self.activeAd + 1) { ad in
                        return
                    }
                }
            }
        }
    }

    func responseMicOffSoundPlayed() { admanProperties.clientVoiceActivityEnded = true }

    public func setResponseDelay(delay: Int) { admanProperties.voiceResponseDelay = delay }
    
    func getResponseDelay() -> Int { return admanProperties.voiceResponseDelay }
    
    func startListening() {
        if recognizer.state == .listening { return }
        let ad = adsList[activeAd]
        if ad.assets[AdmanBannerAssetMicOnCache] != nil {
            player.pause()
            player.replaceCurrentItem(with: nil)
            player.addAsset(asset: ad.assets[AdmanBannerAssetMicOnCache] as! AVAsset, adID: activeAd, itemType: .micOn)
        } else {
            startListeningCb()
        }
    }
    
    func startListeningCb() {
        if state == .error || state == .playbackCompleted || state == .readyForPlayback { return }
        setState(newState: .speechRecognizerStarted)
        NotificationAdmanVoice.sendEvent(event: .recognizerStarted)
        recognizer.startListening()
        interactionTimeoutTimer?.invalidate()
        interactionTimeoutTimer = nil
        var responseTime = currentAd()!.responseTime
        if admanProperties.customAdResponseTime > 0 { responseTime = admanProperties.customAdResponseTime }
        let delay = getResponseDelay()
        interactionTimeoutTimer = Timer(timeInterval: Double(responseTime + delay + admanProperties.serverTimeout), target: self, selector: #selector(voiceInteractionDidNotFinishedAfterTimeout), userInfo: nil, repeats: false)
        endSpeechDetectionTimer = Timer.scheduledTimer(timeInterval: Double(responseTime + delay - 1), target: self, selector: #selector(endSpeechDetectSel), userInfo: nil, repeats: false)
        RunLoop.main.add(endSpeechDetectionTimer!, forMode: .common)
        setState(newState: .voiceInteractionStarted)
        NotificationAdmanVoice.sendEvent(event: .interactionStarted)
    }
    
    @objc func endSpeechDetectSel() {
        recognizer.endSpeechDetection()
        recognizer.stopListening()
        if state != .speechRecognizerStopped {
            setState(newState: .speechRecognizerStopped)
            NotificationAdmanVoice.sendEvent(event: .recognizerStopped)
        }
        if currentAd()!.assets[AdmanBannerAssetMicOffCache] != nil {
            player.addAsset(asset: currentAd()!.assets[AdmanBannerAssetMicOffCache] as! AVAsset, adID: activeAd, itemType: .micOff)
        }
    }
    
    func skip() {
        player.pause()
        player.replaceCurrentItem(with: nil)
        resetRecognizerState()
        if hasNextAd() { playNextAd() } else {
            if state != .error {
                NotificationAdmanPlayer.sendEvent(event: .complete)
                player.removeAllItems()
                setState(newState: .playbackCompleted)
                if admanProperties.dtmfAdsDetection { prepareWithDelay() }
            }
        }
    }
        
    func errorReceived(err: Error, param: Int) {
        delegate?.errorReceived(error: err)
        self.error = err
        print("Error received: ", err.localizedDescription)
        resetRecognizerState()
        player.pause()
        player.replaceCurrentItem(with: nil)
        setState(newState: .error)
        logBadRequest()
        NotificationAdmanBase.sendEvent(event: .error)
        if param == 0 {
            skip()
        } else if param == 1 {
            processIntent(dict: ["transcript": "No", "action": "negative", "playMicOff": "No"])
        }
    }
    
    func errorReceived(err: Error) {
        errorReceived(err: err, param: 0)
    }
    
    func finishAdPlayback() {
        firstQuarterReported = false
        secondQuarterReported = false
        thirdQuarterReported = false
        if state == .voiceInteractionStarted { return }
        
        if let ad = currentAd() {
            if ad.responses.count > 0 {
                recognizer.stop()
                stopPlaybackProgressTimer()
            }
            ad.statData.reportEvent(eventName: AdmanStatEventComplete)
            if ad.bannerImage != nil { viewClosed() }
            if hasNextAd() {
                playNextAd()
            } else {
                NotificationAdmanPlayer.sendEvent(event: .complete)
                player.removeAllItems()
                setState(newState: .playbackCompleted)
                if admanProperties.dtmfAdsDetection { prepareWithDelay() }
            }
        }
    }
        
    func viewClosed() {
        if state == .paused { resume() }
        delegate?.viewClosed()
        currentAd()!.statData.reportEvent(eventName: AdmanStatEventClose)
    }
    
    func stopPlaybackProgressTimer() {
        playbackProgressTimer?.invalidate()
        playbackProgressTimer = nil
    }
    
    func getWebsocketHeaderOptions() -> [String: Any] {
        return [
            "idfa": admanProperties.idfa,
            "site_id": admanProperties.siteId,
            "vad": admanProperties.serverVoiceActivityDetection,//must be number
            "ad_id": adsList[activeAd].adId ?? "",
            "response_delay": getResponseDelay(),
            "device_info": "Adman voice sdk " + AdmanVersion + "; " + UIDevice.current.model.description + " " + ProcessInfo.processInfo.operatingSystemVersion.majorVersion.description
        ]
    }
    
    func enableVoiceSpeechDetection(ad: AdmanBannerWrapper) {
        VACNetworkManager.setWebSocketUrl(url: getSpeechRecognizerUrl(voiceServer: getVoiceServer(region: admanProperties.region, location: admanProperties.recognitionBackendLocation)))
        startListening()
        recognizer.start(onStart: {}, options: getWebsocketHeaderOptions())
    }
    
    func getSpeechRecognizerUrl(voiceServer: String) -> String {
        if voiceServer.contains("adman/") { return voiceServer }
        var url: String = voiceServer + "/adman" + getRecognitionBackend(backend: admanProperties.backend) + "/v2"
        if adsList.count > 0 {
            var crossServerAdID = ""
            let ad = adsList[activeAd]
            if let adCrossServerAdID = ad.crossServerAdID {
                crossServerAdID = "&ad_id=" + adCrossServerAdID
            }
            url = url + "?language=" + (ad.responseLang ?? "") + crossServerAdID
        }
        return url
    }
    
    func audioPlayerDidFinishPlaying() {
        log(logString: "Ad playback completed")
        playbackProgressTimer?.invalidate()
        playbackProgressTimer = nil
        guard let ad = currentAd() else { return }
        if !ad.isVoiceAd() {
            finishAdPlayback()
        } else {
            enableVoiceSpeechDetection(ad: ad) }
    }
    
    func logBadRequest() {
        statistic.badRequests.append(Date())
        if statistic.badRequests.count >= 2 && allowPreload {
            let seconds = (statistic.badRequests.last?.timeIntervalSince(statistic.badRequests.first!))!
            let timeout = 60 * 1.2
            if seconds < timeout {
                statistic.badRequests.removeAll()
                allowPreload = false
                print("Too many failed requests in short time (2 in ~70 sec), preloading disabled for current Adman instance")
                let allowPreloadTimer = Timer(timeInterval: timeout, target: self, selector: #selector(logBadRequestCb), userInfo: nil, repeats: false)
                RunLoop.main.add(allowPreloadTimer, forMode: .common)
            } else { statistic.badRequests.remove(at: 0) }
        }
    }
    
    func processIntent(dict: [String: String]) {
        resetRecognizerState()
        if state == .playing || state == .voiceResponsePlaying {
            player.pause()
            player.replaceCurrentItem(with: nil)
        }
        if state != .speechRecognizerStopped {
            setState(newState: .speechRecognizerStopped)
            NotificationAdmanVoice.sendEvent(event: .recognizerStopped)
        }
        processRecognizerResult(result: dict)
    }
    
    func requestCommonBanner(type: AdmanBannerType, success: @escaping (AdmanBanner) -> (), failure: @escaping (Error?) -> ()) {
        let slot = type == .t320x480 ? 3146 : type == .t320x50 ? 3175 : 3146
        let isTracking = admanProperties.isTrackingEnabled ? "1" : "0"
        let path = "https://ad.mail.ru/vp/" + String(slot) + "?_SITEID=" + String(admanProperties.siteId) + "&idfa=" + admanProperties.idfa + "&advertising_tracking_enabled=" + isTracking
        print("AdmanStateFetchingInfo from: " + path)
        print("Fetching " + path)
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForResource = 7.0
        
        //var error: Error
        let task = URLSession(configuration: sessionConfig).dataTask(with: URL(string: path)!) { data, response, err in
            if let err = err {
                self.error = err
                print("error " + err.localizedDescription)
                failure(err)
                return
            }
            if data?.count == 0 {
                self.setState(newState: .adNone)
                failure(err)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                let banners = json?["sections.preroll"] as? [Any]
                let a = banners?.first as? [String: Any]
                let ab = AdmanBanner()
                ab.adId = a?["bannerID"] as? String
                let width = a?["width"] as? String
                let height = a?["height"] as? String
                let companionAdSize = String(width!) + "x" + String(height!)
                ab.companionAds[companionAdSize]?.staticResource = a?["src"] as? String
                ab.url = a?["src"] as? String
                ab.urlToNavigateOnClick = a?["urlToNavigateOnClick"] as? String
                let statData = a!["statistics"] as! [[String: String]]
                if statData.count > 1 {
                    ab.statData.save(url: statData[1]["url"]!, event: AdmanStatEventCreativeView)
                }
                success(ab)
            } catch {
                let error = error
                failure(error)
            }
        }
        task.resume()
        // [netManager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    }

    public func showBannerForSpot(spot: UIView, type: AdmanBannerType) {
        print("showBannerForSpot:withType: is called")

        requestCommonBanner(type: type) { [self] banner in
            if banner.url == nil {
                print("Warning! There is no banners")
                return;
            }
            let iv: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: spot.frame.width, height: spot.frame.height))
            var image = UIImage()
            do {
                image = UIImage.init(data: try Data(contentsOf: URL(string: banner.url!)!))!
            } catch {
                print("JSONSerialization error: ", error)
            }
            iv.contentMode = .scaleAspectFit
            iv.image = image
            iv.isUserInteractionEnabled = true
                        
            let tapper = CustomTapWithUrl(target: self, action: #selector(self.handleTap(_:)))
            tapper.url = banner.url
            tapper.delegate = self
            iv.addGestureRecognizer(tapper)
            spot.addSubview(iv)
            let size = getDefaultBannerSize()
            if currentAd()!.companionAds[size]?.tracking != nil && ((currentAd()!.companionAds[size]?.tracking?["creativeView"]) != nil) {
                currentAd()!.statData.reportEvent(event: AdmanStatEventCreativeView, companion: currentAd()!.companionAds[size]!)
            } else {
                currentAd()!.statData.reportEvent(eventName: AdmanStatEventCreativeView)
            }
        } failure: { error in
            if let error = error { print("Error on loading banner: " + error.localizedDescription) } else { print("Error on loading banner") }
        }
    }
    
    @objc func handleTap(_ sender: CustomTapWithUrl? = nil) {
        bannerTouched(urlToNavigate: (sender?.url!)!)
    }
    
    func showBannerInPlaceholder(placeholder: UIView, button: UIButton) {
        if currentAd()!.url != nil {
            let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: placeholder.frame.width, height: placeholder.frame.height))
            iv.contentMode = .scaleAspectFit
            placeholder.addSubview(iv)
            placeholder.bringSubviewToFront(iv)
        } else {
            print("Warning! Banner info has not been fetched yet! Wait until state=AdmanStateReadyForPlayback")
        }
    }
    
    func preloadImage(image: UIImage) {
        let ref: CGImage = image.cgImage!
        let width: size_t = ref.width
        let height: size_t = ref.height
        let space: CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let context: CGContext =  CGContext(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: width*4, space: space, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue)!
        context.draw(ref, in: CGRect(x: Int(0.0),y: Int(0.0),width: width,height: height))
    }
    
    @objc func voiceInteractionDidNotFinishedAfterTimeout() {
        NotificationAdmanVoice.sendEvent(event: .error)
        log(logString: "504, Recognition server timeout")
        errorReceived(err: NSError(domain: InstreamaticDomain, code: 42, userInfo: [NSLocalizedDescriptionKey: "504, Recognition server timeout"]), param: 1)
    }
    
    func updateProgress() {
        if playbackProgressTimer != nil { stopPlaybackProgressTimer() }
        playbackProgressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector (updateProgressSelector), userInfo: nil, repeats: true)
        RunLoop.main.add(playbackProgressTimer!, forMode: .common)
    }
    
    @objc func updateProgressSelector(timer: Timer) {
        
        let currentTime = Int32(CMTimeGetSeconds(player.currentItem?.currentTime() ?? .zero))
        if CMTimeGetSeconds(player.currentItem?.duration ?? .zero).isNaN { return }
        let duration = Int32(CMTimeGetSeconds(player.currentItem?.duration ?? .zero))
        
        if duration > 0 { progress = Double(currentTime/duration) }
        
        
        
        //progress = CMTimeGetSeconds(player.currentItem?.currentTime() ?? CMTime.zero)/CMTimeGetSeconds(player.currentItem?.duration ?? CMTime.zero)
        //guard !(progress.isNaN || progress.isInfinite) else { return }
        let current = Int(progress*100)
        if current > 25 && !firstQuarterReported {
            adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventFirstQuartile)
            firstQuarterReported = true
        } else if current > 50 && !secondQuarterReported {
            adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventMidpoint)
            secondQuarterReported = true
            log(logString: "Playback midpoint")
        } else if current > 75 && !thirdQuarterReported {
            adsList[activeAd].statData.reportEvent(eventName: AdmanStatEventThirdQuartile)
            NotificationAdmanBase.sendEvent(event: .almostCompleted)
            thirdQuarterReported = true
        }
        
        NotificationAdmanPlayer.sendEvent(event: .progress, userData: self)
        
        if let ad = currentAd() {
            if duration - currentTime == 1 && ad.videoAd && (player.currentItem as? AdmanPlayerItem)?.itemType == .initial {
                enableVoiceSpeechDetection(ad: ad)
            }
        }
        
        
/*
        if currentAd()?.videoAd ?? false {
            NotificationAdmanPlayer.sendEvent(event: .progress, userData: self)
        } else {
            NotificationAdmanPlayer.sendEvent(event: .progress, userData: estimatedTime())
        }*/
    }
    
    func preloadAd(index: Int, onComplete: AdmanOnCompleteType) {
        log(logString: "Fetching " + String(index) + " ad from queue. Banner: " + (adsList[index].url ?? ""))
        if adsList[index].url != nil {
            let imageUrl = URL(string: adsList[index].url!)
            adsList[index].adCache = try? Data(contentsOf: imageUrl!)
            
            guard let adCache = adsList[index].adCache,
                  let img = UIImage(data: adCache) else { return }
            
            //guard let img = UIImage(data: adsList[index].adCache?) else { return }
            preloadImage(image: img)
            adsList[index].bannerImage = img
            log(logString: "Banner preloaded successfully")
        }
        
        let ad = adsList[index]
        if ad.assets[AdmanBannerAssetMicOnUrl] != nil {
            var micOnLoaded = false
            var micOffLoaded = false
            var completionSemaphore = true
            player.preloadAsset(url: ad.assets[AdmanBannerAssetMicOnUrl] as! String) { asset in
                micOnLoaded = true
                ad.assets[AdmanBannerAssetMicOnCache] = asset
                if micOffLoaded && micOnLoaded && completionSemaphore {
                    completionSemaphore = false
                    onComplete?(ad)
                }
            } onFail: { [self] error in
                micOnLoaded = true
                if micOffLoaded && micOnLoaded && completionSemaphore {
                    completionSemaphore = false
                    onComplete?(adsList[index])
                }
            }
            player.preloadAsset(url: ad.assets[AdmanBannerAssetMicOffUrl] as! String) { asset in
                micOffLoaded = true
                ad.assets[AdmanBannerAssetMicOffCache] = asset
                if micOffLoaded && micOnLoaded && completionSemaphore {
                    completionSemaphore = false
                    onComplete?(ad)
                }
            } onFail: { [self] error in
                micOffLoaded = true
                if micOffLoaded && micOnLoaded && completionSemaphore {
                    completionSemaphore = false
                    onComplete?(adsList[index])
                }
            }
        } else {
            onComplete?(ad)
        }
    }
    
    func processRecognizerResult(result: [String: Any]) {
        admanProperties.clientVoiceActivityEnded = false
        recognizer.stopListening()
        if state != .speechRecognizerStopped {
            setState(newState: .speechRecognizerStopped)
            NotificationAdmanVoice.sendEvent(event: .recognizerStopped)
        }
        let actionString = (result["action"] as? String) ?? ""
        delegate?.phraseRecognized(result: result as [String: Any])
        if actionString == "unknown" || actionString == "unknown2" {
            playUnknownAction(actionType: actionString)
            return
        }
        if actionString == "skip" {
            currentAd()!.statData.reportEvent(eventName: AdmanStatEventSkip)
            skip()
            return
        }
        if actionString == "swearing" { currentAd()!.statData.reportEvent(eventName: AdmanStatEventResponseSwearing) }
        if actionString.contains("stop") {
            stop()
            return
        }
        var action: AdmanInteraction?
        for response in currentAd()!.responses {
            for actionName in response.value {
                if actionString == actionName.type {
                    action = actionName
                    break
                }
            }
        }
        if action != nil { reactionForInteraction(response: action!, source: (result["source"] as? String) ?? "") }
        else { playUnknownAction(actionType: "unknown") }
    }
    
    func networkStateDidChanged(available: Bool) {
        if !available && player.getPreloadMode() == .progressive {
            setState(newState: .error)
            NotificationAdmanBase.sendEvent(event: .error)
        }
        if available && player.getPreloadMode() == .progressive
            && state == .error {
            if fallbackState == .playbackCompleted { state = fallbackState }
            else { setState(newState: fallbackState!) }
            if fallbackState == .playing {
                if CMTimeCompare(player.currentItem!.duration, (player.currentItem?.currentTime())!) == 0 {
                    if (player.currentItem as! AdmanPlayerItem).isPlayed { player.playbackFinished(notification: nil) }
                    else { skip() }
                } else {
                    player.play() }
            }
        }
    }
    
    func prepareWithDelay() {
        let interval = 18 + arc4random_uniform(32)
        print("Prepare delayed for ", String(interval))
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(interval)) { [self] in
            prepare()
        }
    }
    
    func resetRecognizerState() {
        recognizer.stop()
        recognizer.closeConnection()
        interactionTimeoutTimer?.invalidate()
        interactionTimeoutTimer = nil
        voiceResponseDelayTimer?.invalidate()
        voiceResponseDelayTimer = nil
        playbackProgressTimer?.invalidate()
        playbackProgressTimer = nil
        endSpeechDetectionTimer?.invalidate()
        endSpeechDetectionTimer = nil
        setState(newState: .voiceInteractionEnded)
        NotificationAdmanVoice.sendEvent(event: .interactionEnded)
    }
    
    func hasNextAd() -> Bool {
        return adsList.count - 1 > activeAd
    }
    
    func playNextAd() {
        activeAd += 1
        unknownPhraseIteration = 0
        admanProperties.voiceResponseDelay = adsList[activeAd].responseDelay
        setState(newState: .adChangedForPlayback)
        NotificationAdmanPlayer.sendEvent(event: .ready)
        play()
        recognizer.stop()
        recognizer.closeConnection()
        log(logString: "libAdman: start playing next ad(" + (adsList[activeAd].adId ?? "") + ") from sequence")
    }
    
    func log(logString: String) {
        delegate?.log(message: logString)
        print(logString)
    }
    
    func timeControlStatusWaitingToPlayAtSpecifiedRate(state: Bool) {
        if state { playerBufferState = 1 } else { playerBufferState = 0 }
        NotificationAdmanPlayer.sendEvent(event: .waitingToPlayAtSpecifiedRate, userData: self)
    }

    func buildModel(arrayModels: [[String: Any]]) -> [VastElement] {
        var models = [VastElement]()
        for model in arrayModels {
            models.append(VastElement(dict: model))
        }
        return models
    }
    
    func creativeDidParsed(ads: [AdmanBannerWrapper]) {
        print("VAST parsed successfully")
        if ads.count == 0 {
            print("No ads to playback")
            logBadRequest()
            if !silentPreload {
                setState(newState: .adNone)
                NotificationAdmanBase.sendEvent(event: .none)
            }
            silentPreload = false
            return
        }
        
        if !AdmanMicrophone().isMicrophoneEnabled() && ads.first!.isVoiceAd() {
            error = NSError.init(domain: InstreamaticDomain, code: 42, userInfo: [NSLocalizedDescriptionKey: "Microphone disabled, failed to prepare voice ad"])
            setState(newState: .error)
            NotificationAdmanBase.sendEvent(event: .error)
            print("Microphone disabled, failed to prepare voice ad")
            return
        }
        
        AdmanBackground().isInBackground(synced: false) { [self] isBack in
            if isBack && admanProperties.requestURL != nil {
                error = NSError.init(domain: InstreamaticDomain, code: 42, userInfo: [NSLocalizedDescriptionKey: "App recieved in background via custom URL, failed to preare ad"])
                setState(newState: .typeMismatch)
                NotificationAdmanBase.sendEvent(event: .error)
                print("App recieved in background via custom URL, failed to preare ad")
            } else {
                activeAd = 0
                adsList = ads
                adsList.sort(by: { $0.sequence < $1.sequence })
                intentTapped = false
                admanProperties.voiceResponseDelay = currentAd()!.responseDelay
                if admanProperties.region != AdmanRegion.voice {
                    statistic.reportEvent(eventName: AdmanStatEventFetched)
                }
                log(logString: "Data from ad server parsed successfully, preloading ad: " + currentAd()!.adId!)
                if !silentPreload {
                    setState(newState: .preloading)
                    NotificationAdmanBase.sendEvent(event: .preloading)
                }
                
                preloadAd(index: activeAd) { [self] ad in
                    if let asset = self.player.currentItem?.asset as? AVURLAsset {
                        //if asset.url != nil { return }
                        if asset.url != URL(string: "") { return }
                    }
                    
                    self.player.media = self.adsList.last!.media
                    let media: [AdmanMediaFile] = (ad as! AdmanBanner).media
                    
                    for i in 0...media.count-1 {
                        let item = media[i]
                        if item.url == (ad as! AdmanBanner).sourceUrl {
                            self.currentStepID = item.stepId
                            self.player.currentStepID = self.currentStepID
                            break
                        }
                    }
                    if player.getPreloadMode() == .progressive {
                        player.isPlaying = false
                        AdmanUIBase.isBlurEnabledValue = true
                        player.addUrl(url: (ad as! AdmanBanner).sourceUrl ?? "", adID: activeAd, itemType: .initial, isRecognize: false)
                    } else {
                        player.preloadAsset(url: currentAd()!.sourceUrl!) { [self] asset in
                            if let currentItem = player.currentItem,
                               let currentAsset = currentItem.asset as? AVURLAsset {
                                if (currentAd()!.sourceUrl ?? "") == currentAsset.url.absoluteString {
                                    return
                                }
                            }
                            player.addAsset(asset: asset as! AVAsset, adID: activeAd, itemType: .initial)
                        } onFail: { [self] error in
                            log(logString: "Failed to preload audio ad, " + error!.debugDescription)
                            errorReceived(err: error as! Error, param: 0)
                        }

                    }
                }
            }
        }
    }
    
    func elapsedTime() -> Int {
        if player.currentItem == nil { return 0 }
        let i = CMTimeGetSeconds((player.currentItem?.currentTime())!)
        if i < 0 { return 0 }
        if i > 65 { return 0 }
        return Int(i)
    }
    
    func estimatedTime() -> Int {
        if player.currentItem == nil { return currentAd()!.duration! }
        let i = CMTimeGetSeconds((player.currentItem?.duration)!) - CMTimeGetSeconds((player.currentItem?.currentTime())!)
        if i < 0 { return currentAd()!.duration! }
        if i > 65 { return currentAd()!.duration! }
        return Int(i)
    }
    
    func elapsedTimeString() -> String { return formatTime(seconds: elapsedTime()) }

    func estimatedTimeString() -> String { return formatTime(seconds: estimatedTime()) }

    func formatTime(seconds: Int) -> String {
        let mins = (seconds % 3600) / 60
        let secs = seconds % 60
        return String(mins) + ":" + String(secs)
    }
    
    public func setRecognitionBackendLocation(location: AdmanRecognitionBackendLocation) {
        admanProperties.recognitionBackendLocation = location
    }
    
    public func recognitionBackendLocation() -> AdmanRecognitionBackendLocation {
        return admanProperties.recognitionBackendLocation
    }
    
    public func setServerTimeout(delay: Int) {
        admanProperties.serverTimeout = delay
    }
    
    public func setResponseTime(duration: Int) {
        admanProperties.setResponseTime(duration: Float(duration))
    }
    
    public func setSiteVariable(key: String, value: String) {
        admanProperties.setSiteVariable(key: key, value: value)
    }
}
