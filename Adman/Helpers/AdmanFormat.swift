//
//  AdmanFormat.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation


public enum AdmanFormat: Int {
    case any = 1,
         swiftSponsor = 2,
         gameVibe = 3
}
