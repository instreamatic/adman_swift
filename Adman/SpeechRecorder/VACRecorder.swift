//
//  VACRecorder.swift
//  Adman_swift
//
//  Created by Barnet Birday on 21/02/2022.
//

import Foundation
import CoreAudio
import AVFAudio

class VACRecorder: NSObject {
    var delegate: VACRecorderDelegate?
    var running = false
    static let VACRecorderNumberBuffers = 3
    static let kNumberOfChanels = 1
    static let kSampleRate = 16000
    static let kBitsPerChannel = 32
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var state: VACRecorderState?
    var audioFile: AudioFileID?
    var recordPackets: Int64 = 0
    var dataFormat: AudioStreamBasicDescription!
    var queue: AudioQueueRef!
    var buffers: AudioQueueBufferRef!
    var bufferByteSize: UInt32!
    var currentPacket: Int64!
    
    public func isRunning() -> Bool { return running }
    
    public func startRecording() {
        currentPacket = 0
        var recordFormat = setupAudioFormat16bit()
        var propSize = UInt32(MemoryLayout<AudioStreamBasicDescription>.size)
        AudioFormatGetProperty(kAudioFormatProperty_FormatInfo, 0, nil, &propSize, &recordFormat)
        var status = AudioQueueNewInput(&recordFormat,
                           VACRecorderAudioInputCallback,
                           Unmanaged.passUnretained(self).toOpaque(), nil, nil, 0, &queue)
        let bufferByteSize = VACRecorderCalculateBufferSize(&recordFormat, queue, 0.02)
        guard let unWrappedQueue = queue else { return }
        if status == 0 {
            for _ in 0...VACRecorder.VACRecorderNumberBuffers-1 {
              var buffer: AudioQueueBufferRef?
              AudioQueueAllocateBuffer(unWrappedQueue, bufferByteSize, &buffer)
              guard let unWrappedBuffer = buffer else { return }
              AudioQueueEnqueueBuffer(unWrappedQueue, unWrappedBuffer, 0, nil)
            }
            running = true
            status = AudioQueueStart(unWrappedQueue, nil)
        }
    }
    
    public func stopRecording() {
        running = false
        if let queue = queue { AudioQueueStop(queue, true) }
        if let audioFile = audioFile { AudioFileClose(audioFile) }
        
        for i in 0...VACRecorder.VACRecorderNumberBuffers-1 {
            if buffers != nil { AudioQueueFreeBuffer(queue, &buffers[i]) }
        }
        if let queue = queue { AudioQueueDispose(queue, true) }
        if let audioFile = audioFile { AudioFileClose(audioFile) }
    }
    
    func setupAudioFormat() -> AudioStreamBasicDescription {
        var recordFormat = AudioStreamBasicDescription()
        memset(&recordFormat, 0, MemoryLayout<AudioStreamBasicDescription>.size)
        recordFormat.mFormatID = kAudioFormatLinearPCM
        recordFormat.mFormatFlags = kAudioFormatFlagsNativeFloatPacked
        recordFormat.mBitsPerChannel = UInt32(VACRecorder.kBitsPerChannel)
        recordFormat.mChannelsPerFrame = UInt32(VACRecorder.kNumberOfChanels)
        recordFormat.mBytesPerPacket = UInt32(4*MemoryLayout<Int16>.stride)
        recordFormat.mBytesPerFrame = UInt32(4*MemoryLayout<Int16>.stride)
        recordFormat.mFramesPerPacket = 1
        recordFormat.mSampleRate = Float64(VACRecorder.kSampleRate)
        return recordFormat
    }
    
    func setupAudioFormat16bit() -> AudioStreamBasicDescription {
        var recordFormat = AudioStreamBasicDescription()
        //memset(&recordFormat, 0, MemoryLayout<AudioStreamBasicDescription>.size)
        recordFormat.mFormatID = kAudioFormatLinearPCM
        recordFormat.mFormatFlags = kAudioFormatFlagIsSignedInteger
        recordFormat.mBitsPerChannel = 16
        recordFormat.mChannelsPerFrame = 1
        recordFormat.mBytesPerPacket = 2
        recordFormat.mBytesPerFrame = 2
        recordFormat.mFramesPerPacket = 1
        recordFormat.mSampleRate = 16000
        return recordFormat
    }
    
    static func wavHeader() -> Data {
        let headerSize = 44
        let totalAudioLen = 0 //use dummy for stream
        let totalDataLen = 0 //use dummy for stream
        let longSampleRate = kSampleRate
        let channels = kNumberOfChanels
        let byteRate = 2 * Int(longSampleRate) * channels
        var header = [Int8]()
        
        //var header: [UnsafeMutablePointer<UInt8>?] = Array(repeating: nil, count: 44)
        
        header.append(Int8("R".utf8.first!)) // RIFF/WAVE header
        header.append(Int8("I".utf8.first!))
        header.append(Int8("F".utf8.first!))
        header.append(Int8("F".utf8.first!))
        header.append(Int8(totalDataLen & 0xff))
        header.append(Int8((totalDataLen >> 8) & 0xff))
        header.append(Int8((totalDataLen >> 16) & 0xff))
        header.append(Int8((totalDataLen >> 24) & 0xff))
        header.append(Int8("W".utf8.first!))
        header.append(Int8("A".utf8.first!))
        header.append(Int8("V".utf8.first!))
        header.append(Int8("E".utf8.first!))
        header.append(Int8("f".utf8.first!))  // 'fmt ' chunk
        header.append(Int8("m".utf8.first!))
        header.append(Int8("t".utf8.first!))
        header.append(Int8(" ".utf8.first!))
        header.append(16)  // 4 bytes: size of 'fmt ' chunk
        header.append(0)
        header.append(0)
        header.append(0)
        header.append(1)  // format = 1
        header.append(0)
        header.append(Int8(channels))
        header.append(0)
        
        let longSampleRateInt: Int = longSampleRate
        
        let array = withUnsafeBytes(of: longSampleRateInt.littleEndian, Array.init)
        header.append(Int8(truncatingIfNeeded: array[0] & 0xff))
        header.append(Int8(">".utf8.first!))//header.append(Int8(array[1]))
        header.append(Int8(array[2]))
        header.append(Int8(array[3]))
        header.append(Int8((byteRate >> 0) & 0xff))
        header.append(Int8("}".utf8.first!))//header.append(Int8((byteRate >> 8) & 0xff))
        header.append(Int8((byteRate >> 16) & 0xff))
        header.append(Int8((byteRate >> 24) & 0xff))
        header.append(Int8(kNumberOfChanels * 16 / 8))  // block align
        header.append(0)
        header.append(16)  // bits per sample
        header.append(0)
        header.append(Int8("d".utf8.first!))
        header.append(Int8("a".utf8.first!))
        header.append(Int8("t".utf8.first!))
        header.append(Int8("a".utf8.first!))
        header.append((Int8)(totalAudioLen & 0xff))
        header.append((Int8)((totalAudioLen >> 8) & 0xff))
        header.append((Int8)((totalAudioLen >> 16) & 0xff))
        header.append((Int8)((totalAudioLen >> 24) & 0xff))
        
        /*
        
        let strHeader = "UklGRgAAAABXQVZFZm10IBAAAAABAAEAgD4AAAB9AAACABAAZGF0YQAAAAA="
        //NSData *dataAA = [[NSData alloc]initWithBase64EncodedString:base64EncodedKey options:NSDataBase64DecodingIgnoreUnknownCharacters];
        
        let dataHeader = Data(base64Encoded: strHeader, options: .ignoreUnknownCharacters)!
        
        return dataHeader
        */
        return Data(bytes: header, count: headerSize)
    }
}

func VACRecorderAudioInputCallback(inUserData: UnsafeMutableRawPointer?,
                             inAQ: AudioQueueRef,
                             inBuffer: AudioQueueBufferRef,
                             inStartTime: UnsafePointer<AudioTimeStamp>,
                             inNumberPackets: UInt32,
                                   inPacketDescs: UnsafePointer<AudioStreamPacketDescription>?) {
    guard let inUserData = inUserData else { return }
    let recorder = Unmanaged<VACRecorder>.fromOpaque(inUserData).takeUnretainedValue()
    guard let queue = recorder.queue else { return }

    let audioData = NSData(bytesNoCopy: inBuffer.pointee.mAudioData, length: Int(inBuffer.pointee.mAudioDataByteSize), freeWhenDone: false)
    recorder.delegate?.processSampleWithCapacity(audioDataBytesCapacity: UInt32(inBuffer.pointee.mAudioDataByteSize), audioData: audioData)
    recorder.recordPackets = recorder.recordPackets + Int64(inNumberPackets)
    if recorder.running {
        AudioQueueEnqueueBuffer(queue, inBuffer, 0, nil)
    }
}

func VACRecorderCalculateBufferSize (_ audioStreamDescription: UnsafePointer<AudioStreamBasicDescription>,
                                     _ audioQueue: AudioQueueRef, _ seconds: Float64) -> UInt32 {
    let maxBufferSize: Int = 0x50000
    var maxPacketSize = audioStreamDescription~>.mBytesPerFrame
    if maxPacketSize == 0 {
        var maxVBRPacketSize: UInt32 = UInt32(MemoryLayout.size(ofValue: maxPacketSize))
        AudioQueueGetProperty ( audioQueue, kAudioQueueProperty_MaximumOutputPacketSize, &maxPacketSize, &maxVBRPacketSize)
    }
    let numBytesForTime: Float64 = audioStreamDescription~>.mSampleRate * Double(maxPacketSize) * seconds
    return (UInt32)(Int(numBytesForTime) < maxBufferSize
                    ? Int(numBytesForTime)
                    : maxBufferSize)
}

extension Int {
    func toUInt32() -> UInt32 {
        return UInt32(self)
    }
    
    func toFloat64() -> Float64 {
        return Float64(self)
    }
    
    func toDouble() -> Double {
        return Double(self)
    }
}

extension UInt32 {
    func toInt64() -> Int64 {
        return Int64(self)
    }
    func toInt() -> Int {
        return Int(self)
    }
}

extension Float {
    func toFloat64() -> Float64 {
        return Float64(self)
    }
}

extension Double {
    func toInt() -> Int {
        guard !(self.isNaN || self.isInfinite) else { return 0 }
        return Int(self)
    }
    
    func toUInt32() -> UInt32 {
        return UInt32(self)
    }
}

postfix operator ~>
extension UnsafePointer where Pointee == AudioStreamBasicDescription {
    static postfix func ~> (pointer: UnsafePointer<AudioStreamBasicDescription>) -> AudioStreamBasicDescription {
        return pointer.pointee
    }
}

extension UnsafeMutablePointer where Pointee == VACRecorderState {
    static postfix func ~> (pointer: UnsafeMutablePointer<VACRecorderState>) -> VACRecorderState {
        return pointer.pointee
    }
}

extension UnsafeMutablePointer where Pointee == AudioQueueBuffer {
    static postfix func ~> (pointer: UnsafeMutablePointer<AudioQueueBuffer>) -> AudioQueueBuffer {
        return pointer.pointee
    }
}
