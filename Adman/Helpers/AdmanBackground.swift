//
//  AdmanBackground.swift
//  Adman_swift
//
//  Created by Barnet Birday on 05/02/2022.
//

import Foundation
import UIKit

struct AdmanBackground {
    func isInBackground(synced: Bool, cb: AdmanOnFulfill) {
        if synced {
            if Thread.isMainThread {
                cb?(false)
            } else {
                DispatchQueue.main.sync {
                    let state = UIApplication.shared.applicationState == UIApplication.State.background
                    cb?(state)
                }
            }
        } else {
            DispatchQueue.main.async {
                let state = UIApplication.shared.applicationState == UIApplication.State.background
                DispatchQueue.global().async {
                    cb?(state)
                }
            }
        }
    }
}
