//
//  AdmanPlayerItemType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 31/01/2022.
//

import Foundation

enum AdmanPlayerItemType: Int {
    case ad,
         unknown,
         reaction,
         micOn,
         micOff,
         initial
}
