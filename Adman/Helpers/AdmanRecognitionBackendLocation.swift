//
//  AdmanRecognitionBackendLocation.swift
//  Adman_swift
//
//  Created by Barnet Birday on 03/02/2022.
//

import Foundation

public enum AdmanRecognitionBackendLocation: Int {
    case auto,
         EU,
         USA,
         demo
}
