//
//  AdmanPlayerItem.swift
//  Adman_swift
//
//  Created by Barnet Birday on 31/01/2022.
//

import Foundation
import AVFoundation

class AdmanPlayerItem: AVPlayerItem {
    var adID: Int?//readonly
    var itemType: AdmanPlayerItemType?
    var isLoaded = false
    var isPlayed = false
    
     
    init(url: String, adID: Int, itemType: AdmanPlayerItemType) {
        super.init(asset: AVAsset(url: URL(string: url)!), automaticallyLoadedAssetKeys: nil)
        self.adID = adID
        self.itemType = itemType
    }
        
    init(asset: AVAsset, adID: Int, itemType: AdmanPlayerItemType) {
        super.init(asset: asset, automaticallyLoadedAssetKeys: nil)
        self.adID = adID
        self.itemType = itemType
    }
}
