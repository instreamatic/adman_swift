//
//  AdmanResponseValue.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

struct AdmanResponseValue {
    var type: String
    var data: String?
    var stepId: String?
    
    init(type: String, data: String?) {
        self.type = type
        self.data = data
    }
}
