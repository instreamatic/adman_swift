//
//  AdmanState.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/01/2022.
//

import Foundation

public enum AdmanState: Int {
    case initial = 0,
         /** preloading and parse creative from vast/json source */
         fetchingInfo,
         /** ad can be played */
         readyForPlayback,
         /** If creative contain more than one ad, ad changed when playback completed */
         adChangedForPlayback,
         playing,
         paused,
         playbackCompleted,
         stopped,
         /** only for voice ads */
         voiceResponsePlaying,
         voiceInteractionStarted,
         voiceInteractionEnded,
         /** error occurred during loading/playback, 11 */
         error,
         typeMismatch,
         preloading,
         speechRecognizerStarted,
         speechRecognizerStopped,
         /** no ads for this siteId, region, audio format or other params */
         adNone
}
