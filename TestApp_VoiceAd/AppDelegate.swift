//
//  AppDelegate.swift
//  TestApp_VoiceAd
//
//  Created by Barnet Birday on 10/03/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        mainFunc()
        UserDefaults.standard.synchronize()
        if UserDefaults.standard.string(forKey: "lang") == nil && UserDefaults.standard.string(forKey: "lang")?.count == 0 {
            UserDefaults.standard.set(Locale.current.languageCode, forKey: "lang")
            UserDefaults.standard.synchronize()
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    func registerDefaultsFromSettingsBundle() {
        let settingsBundle = Bundle.main.path(forResource: "Root_Settings", ofType: "plist")
        let settings =  NSDictionary(contentsOfFile: settingsBundle!)
        let preferences = settings!["PreferenceSpecifiers"] as! [[String: Any]]

        var defaultsToRegister = [String: Any]()
        for prefSpecification in (preferences ) {
            let key = prefSpecification["Key"]
            if key != nil && prefSpecification.keys.contains("DefaultValue") {
                defaultsToRegister[key as! String] = prefSpecification["DefaultValue"]
            }
        }
        UserDefaults.standard.register(defaults: defaultsToRegister)
    }

    func mainFunc() {
        registerDefaultsFromSettingsBundle();
        // setup lang from settings.bundle
        if UserDefaults.standard.string(forKey: "lang") != nil && UserDefaults.standard.string(forKey: "lang")!.count > 0 {
            let languages = [UserDefaults.standard.string(forKey: "lang")]
            UserDefaults.standard.set(languages, forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        }
    }
}
