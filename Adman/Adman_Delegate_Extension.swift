//
//  Adman_Callbacks.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation
import UIKit
import Network
import CoreMedia

extension Adman {
    func recognizerDidChangeState(recognizer: VACRecognizer, state: VACRecognizerState) {}
    
    func recognizerDidRecognizeCommand(recognizer: VACRecognizer, command: [String: String]?) {}
    
    func recognizerDidSessionEnded(recognizer: VACRecognizer, result: [String: Any]?) {
        if state == .playbackCompleted || state == .error || state == .stopped || intentTapped { return }
        interactionTimeoutTimer?.invalidate()
        interactionTimeoutTimer = nil
        guard let result = result else {
            log(logString: "Transcripted phrase: empty; reaction: empty")
            skip()
            return
        }
        log(logString: "adman.websocket: phrase - " + (result["transcript"] as? String ?? "") + ", reaction - " + (result["action"] as? String ?? ""))
        if currentAd()!.assets[AdmanBannerAssetMicOnCache] == nil {
            admanProperties.clientVoiceActivityEnded = true
        }
        if admanProperties.clientVoiceActivityEnded {
            processRecognizerResult(result: result)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [self] in
                admanProperties.clientVoiceActivityEnded = true
                processRecognizerResult(result: result)
            }
        }
    }
    
    func onSessionEndedAfterPlayback() {
        processRecognizerResult(result: (player.userData as! [String: String]))
    }
    
    func recognizerDidFailWithError(recognizer: VACRecognizer, error: Error) {
        interactionTimeoutTimer?.invalidate()
        log(logString: "Recognition error: " + error.localizedDescription)
        NotificationAdmanVoice.sendEvent(event: .error)
        errorReceived(err: error, param: 1)
    }
    
    func recognizerDidConnectionOpened(recognizer: VACRecognizer, state: VACRecognizerState) {
        
    }
    
    func dtmfIn() {
        delegate?.dtmfIn()
        if state == .readyForPlayback { play() }
    }
    
    func dtmfOut() {
        delegate?.dtmfOut()
    }
    
    func itemDidLoad(adID: Int, itemType: AdmanPlayerItemType) {
        switch (itemType) {
          case .ad:
            if let currentAd = currentAd(),
                let currentItem = player.currentItem {
                currentAd.duration = CMTimeGetSeconds(currentItem.duration).toInt()
            }
            player.play()
            //NotificationAdmanPlayer.sendEvent(event: .playing, userData: adsList[activeAd])
            NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
            setState(newState: .playing)
            log(logString: "Playback started")
        case .reaction, .unknown:
            NotificationAdmanPlayer.sendEvent(event: .playing, userData: self)
            recognizedIntentLoaded()
        case .micOn, .micOff:
            startPlayer()
        case .initial:
            if let currentAd = currentAd(),
                let currentItem = player.currentItem {
                currentAd.duration = CMTimeGetSeconds(currentItem.duration).toInt()
            }
            log(logString: "Creative preloaded and ready for playback")
            if !silentPreload {
                setState(newState: .readyForPlayback)
                NotificationAdmanPlayer.sendEvent(event: .ready)
            }
            silentPreload = false
            enableVastExpiration()
        }
    }
    
    func itemPlaybackDidFinish(adID: Int, itemType: AdmanPlayerItemType) {
        switch (itemType) {
        case .ad, .initial:
            audioPlayerDidFinishPlaying()
        case .reaction:
            recognizedIntentPlayed()
        case .unknown:
            unknownActionDidPlayed()
        case .micOn:
            startListeningCb()
        case .micOff:
            responseMicOffSoundPlayed()
        }
    }
    
    func itemPlaybackDidFail(adID: Int, itemType: AdmanPlayerItemType, err: Error?) {
        switch itemType {
        case .ad, .initial:
            errorReceived(err: err!)
        case .reaction, .unknown:
            skip()
            log(logString: err!.localizedDescription)
        case .micOn:
            startListeningCb()
        case .micOff:
            responseMicOffSoundPlayed()
        }
    }
        
    func log(data: String?) {
        delegate?.log(message: data ?? "")
        print(data ?? "")
    }
    
    func creativeDidParsed(ads: [AdmanBanner]) {
        
    }
    
    func creativeDidParseRedirect(redirectURL: String) -> String {
        return redirectURL
    }
    
    func creativeDidFail(error: Error) {
        print("fetching error (source instreamatic): " + error.localizedDescription)
        delegate?.errorReceived(error: error)
        if vastLiveDurationTimer != nil {
            log(logString: "error received on revalidating VAST: " + error.localizedDescription)
            vastLiveDurationTimer?.invalidate()
            vastLiveDurationTimer = nil
            return
        } else {
            log(logString: "error received on preloading VAST: " + error.localizedDescription)
            errorReceived(err: error)
        }
    }
}
