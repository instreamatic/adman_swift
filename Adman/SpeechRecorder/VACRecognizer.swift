//
//  VACRecognizer.swift
//  Adman_swift
//
//  Created by Barnet Birday on 19/01/2022.
//

import Foundation

class VACRecognizer: VACRecorderDelegate, VACNetworkManagerDelegate {
    var samplescounter: Int?
    var dirPaths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    var state = VACRecognizerState.none
    var delegate: VACRecognizerDelegate?
    var networkManager = VACNetworkManager()
    var samplesPartialBuffer = [Any]()
    //var recorder = AdmanRecorder()
    var recorder = VACRecorder()
    var docsDir = ""
    
    init() {
        recorder.delegate = self
        networkManager.delegate = self
        docsDir = dirPaths[0]
    }
    
    func start(onStart: @escaping ()->(), options: [String: Any]) {
        samplescounter = 0
        state = .ready
        print("libAdman: network recognizer started")
        networkManager.openConnection(options: options)
        onStart()
        state = .listening
    }

    func stop() {
        if state == .none { return }
        state = .none
        recorder.stopRecording()
        endSpeechDetection()
        samplesPartialBuffer.removeAll()
        print("libAdman: network recognizer stopped")
    }
    
    func startListening() {
        print("libAdman: voice recording started")
        samplesPartialBuffer.removeAll()
        if !recorder.isRunning() {
            recorder.startRecording()
        }
        state = .listening
    }
    
    func stopListening() {
        print("libAdman: voice recording stopped")
        if recorder.isRunning() { recorder.stopRecording() }
        state = .none
    }
    
    func endSpeechDetection() {
        networkManager.send(data: networkManager.toMessage(msg: "content-type: audio/wav\r\npath: audio.end\r\n", samples: nil))
    }
    
    func closeConnection() {
        networkManager.closeConnection()
    }
    
    func setState(state: VACRecognizerState) {
        if self.state != state {
            self.state = state
            delegate?.recognizerDidChangeState(recognizer: self, state: state)
        }
    }
    
    func processSampleWithCapacity(audioDataBytesCapacity: UInt32, audioData: NSData) {
        let sampleData = audioData//Data(bytes: audioData, count: audioDataBytesCapacity)
        if !networkManager.isOpen() || samplesPartialBuffer.count > 0 {
            samplesPartialBuffer.append(sampleData)
        } else {
            self.networkManager.sendData(data: sampleData as Data)
        }
    }
    
    func networkManagerConnectionDidOpened(manager: VACNetworkManager) {
        if samplesPartialBuffer.count > 0 {
            while samplesPartialBuffer.count > 0 {
                let sample = samplesPartialBuffer[0]
                samplesPartialBuffer.remove(at: 0)
                networkManager.sendData(data: sample as! Data)
            }
        }
        print("Connection opened: " + VACNetworkManager.getWebSocketUrl())
        delegate?.recognizerDidConnectionOpened(recognizer: self, state: state)
    }
    
    func networkManagerResponseReceived(manager: VACNetworkManager, response: [String : Any]) {
        
    }
    
    func networkManagerDidFail(manager: VACNetworkManager, error: NSError) {
        if state == .none {
            print("Connection closed with reason: ", Int(error.code))
        } else {
            print("libAdman.webSocket: didFailWithError " + error.description)
            if error.code == 1015 {
                delegate?.recognizerDidRecognizeCommand(recognizer: self, command: nil)
            } else if state != .none {
                stop()
                delegate?.recognizerDidFailWithError(recognizer: self, error: error)
            }
        }
    }
    
    func networkManagerSessionEnded(manager: VACNetworkManager, response: [String : Any]) {
        delegate?.recognizerDidSessionEnded(recognizer: self, result: response)
        samplesPartialBuffer.removeAll()
    }
    
    func networkManagerCommandReceived(manager: VACNetworkManager, command: String) {
        if command == "audio.stop" {
            stopListening()
        }
    }
}
