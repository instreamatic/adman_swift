//
//  AdmanUIBase.swift
//  Adman_swift
//
//  Created by Barnet Birday on 01/03/2022.
//

import Foundation
import UIKit
import SafariServices
import AVFoundation

open class AdmanUIBase: UIViewController, SFSafariViewControllerDelegate, UIGestureRecognizerDelegate {

    var rootVC = UIViewController()

    var microphoneStatus = UIView()
    var voiceRequestAnimation = UIImageView()

    open var bannerView = UIImageView()
    var closeView = UIImageView()
    var playView = UIImageView()
    open var rewindView = UIImageView()

    open var remainingTime = UILabel()

    open var intentLabel = UILabel()
    open var positiveView = UIImageView()
    open var negativeView = UIImageView()
    open var ad = AdmanBannerWrapper()
    
    var videoLayer: AVPlayerLayer?
    var videoFrame: UIImageView?
    var videoBlurFrame: UIImageView?
    var responsePreloadTimer: Timer?
    var visualEffectView: UIVisualEffectView?
    var isBlur = false
    var isSubPresent = false
    var isSubOn = true
    var curentFrame: CGRect?
    var topAreaHeightValue: CGFloat?
    var bottomAreaHeightValue: CGFloat?
    var activityIndicator: UIActivityIndicatorView?
    var speakNowButton = UIImageView()
    var subButton = UIImageView()
    var manager: Adman?
    var videoTypeValue: Int?
    static var isBlurEnabledValue: Bool?

    func videoType() -> AdmanUIEndVideoType {
        return AdmanUIEndVideoType(rawValue: videoTypeValue ?? 0) ?? .mediaSilence
    }

    func setVideoType(val: Int) { videoTypeValue = val }

    func isBlurEnabled() -> Bool { return AdmanUIBase.isBlurEnabledValue ?? false }

    func setIsBlurEnabled(val: Bool) { AdmanUIBase.isBlurEnabledValue = val }

    func topAreaHeight() -> CGFloat {
        return UIApplication
        .shared
        .connectedScenes
        .compactMap { $0 as? UIWindowScene }
        .flatMap { $0.windows }
        .first { $0.isKeyWindow }?.safeAreaInsets.top ?? 0
    }
    
    func bottomAreaHeight() -> CGFloat {
        return UIApplication
        .shared
        .connectedScenes
        .compactMap { $0 as? UIWindowScene }
        .flatMap { $0.windows }
        .first { $0.isKeyWindow }?.safeAreaInsets.bottom ?? 0
    }
    
    public init() {
        super.init(nibName: nil, bundle: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(admanPlayerNotification), name: NSNotification.Name(NOTIFICATION_ADMAN_PLAYER), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(admanControlNotification), name: NSNotification.Name(NOTIFICATION_ADMAN_CONTROL), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(admanVoiceNotification), name: NSNotification.Name(NOTIFICATION_ADMAN_VOICE), object: nil)
        
        ///////////////////CHANGE video_ad
        NotificationCenter.default.addObserver(self, selector: #selector(appEnterForeground), name: NSNotification.Name(UIApplication.willEnterForegroundNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appEnterBackground), name: NSNotification.Name(UIApplication.didEnterBackgroundNotification.rawValue), object: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        activityIndicator?.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
    }
    
    public func addSelector(sel: Selector?, view: UIView, target: UIGestureRecognizerDelegate) {
        let tapper = UITapGestureRecognizer(target: target, action: sel)
        tapper.numberOfTapsRequired = 1
        tapper.numberOfTouchesRequired = 1
        tapper.delegate = target
        view.addGestureRecognizer(tapper)
        view.isUserInteractionEnabled = true
    }

    public func loadImageFrom(base64: String) -> UIImage {
        return try! UIImage(data: Data(contentsOf: URL(string: base64)!))!
    }

    public func setVisible(visibility: Bool, listView: [UIView]) {
        for viewItem in listView {
            viewItem.isHidden = !visibility
        }
    }
    
    open func initBaseView() {
        view.backgroundColor = UIColor.black
        let bounds = UIScreen.main.bounds
        view.frame = bounds
        rootVC.view.addSubview(view)
        rootVC.addChild(self)
        didMove(toParent: rootVC)
        
        if rootVC.navigationController != nil {
            let gesture = rootVC.navigationController?.view.gestureRecognizers?.first
            if gesture != nil {
                gesture?.addTarget(self, action: #selector(onSwipeBack(sender:)))
            }
        }
        initBaseComponents()
        
        addSelector(sel: #selector(playStateChanged), view: playView, target: self)
        view.addSubview(playView)
        
        addSelector(sel: #selector(subStateChanged), view: subButton, target: self)
        view.addSubview(subButton)
        
        addSelector(sel: #selector(playbackRewindEvent), view: rewindView, target: self)
        view.addSubview(rewindView)
        
        addSelector(sel: #selector(onClose(tapGestureRecognizer:)), view: closeView, target: self)
        view.addSubview(closeView)
        closeView.isHidden = true
        
        remainingTime.textColor = UIColor.white
        remainingTime.backgroundColor = UIColor.clear
        view.addSubview(remainingTime)
        remainingTime.font = UIFont(name: "Arial", size: 25)
        if let duration = ad.duration { setTime(time: duration) }
            
        if ad.isVoiceAd() { initVoiceView() }
        view.addSubview(bannerView)
        if (ad.url != nil) {
            update(ad: ad) }
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChangedEvent), name: UIDevice.orientationDidChangeNotification, object: UIDevice.current)
        microphoneStatus.backgroundColor = UIColor.red
        view.addSubview(microphoneStatus)
        microphoneStatus.isHidden = true
    }
    
    func initBaseComponents() {
        playView = UIImageView(image: loadImageFrom(base64: PlayButtonString))
        rewindView = UIImageView(image: loadImageFrom(base64: BackButtonString))
        bannerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 640, height: 640))
        bannerView.contentMode = .scaleAspectFill
        bannerView.autoresizingMask = UIView.AutoresizingMask(rawValue:( UIView.AutoresizingMask.flexibleHeight.rawValue | UIView.AutoresizingMask.flexibleWidth.rawValue))
        bannerView.clipsToBounds = true
        closeView = UIImageView(image: loadImageFrom(base64: CloseButtonString))
        microphoneStatus = UIView(frame: CGRect(x: 0, y: 0, width: rootVC.view.frame.size.width, height: statusBarHeight()))
        subButton = UIImageView(image: loadImageFrom(base64: SubtitlesButtonSelectedString))

        if #available(iOS 13.0, *) {
            let activityIndicator = UIActivityIndicatorView(style: .large)
            let color = UIColor(red: 85.0/255.0, green: 46.0/255.0, blue: 195.0/255.0, alpha: 1.0)
            activityIndicator.color = color
            view.addSubview(activityIndicator)
            self.activityIndicator = activityIndicator
            activityIndicator.isHidden = true
        }
    }
    
    open func initVoiceView() {
        var imgAnimate: UIImage?
        var animation = [UIImage]()//41
        
        for i in 1...40 {
            if i < 10 {
                if let img = UIImage(named: "loading_1000" + String(i)) { imgAnimate = img }
            }
            else {
                if let img = UIImage(named: "loading_100" + String(i)) { imgAnimate = img }
            }
            if let imgAnimate = imgAnimate { animation.append(imgAnimate) }
        }
        initVoiceComponents()
        voiceRequestAnimation.animationImages = animation
        voiceRequestAnimation.animationDuration = 1.0
        voiceRequestAnimation.animationRepeatCount = 0
        voiceRequestAnimation.startAnimating()
        view.addSubview(voiceRequestAnimation)
        voiceRequestAnimation.isHidden = true
    
        let locale = ad.responseLang != nil ? ad.responseLang : Locale.preferredLanguages.first
        if locale!.contains("ru") {
            intentLabel.text = "Хотите узнать больше?"
            intentLabel.font = UIFont(name: "Arial", size: 17)
        } else if locale!.contains("ja") {
            intentLabel.text = "興味がありますか"
            intentLabel.font = UIFont(name: "Arial", size: 23)
        } else {
            intentLabel.text = "Interested?"
            intentLabel.font = UIFont(name: "Arial", size: 27)
        }
        intentLabel.textColor = UIColor.white
        intentLabel.backgroundColor = UIColor.clear
        view.addSubview(intentLabel)
        intentLabel.isHidden = true
        addSelector(sel: #selector(negativeIntent(tapGestureRecognizer:)), view: negativeView, target: self)
        view.addSubview(negativeView)
        negativeView.isHidden = true
        addSelector(sel: #selector(positiveIntent(tapGestureRecognizer:)), view: positiveView, target: self)
        view.addSubview(positiveView)
        positiveView.isHidden = true
    }
    
    func initVoiceComponents() {
        let img = UIImage(named: "loading_10001.gif")
        if ad.videoAd {
            playView.image = loadImageFrom(base64: VideoPlayButtonString)
            self.speakNowButton = UIImageView(image: loadImageFrom(base64: VideoSpeakNowString))
            speakNowButton.isHidden = true
            view.addSubview(speakNowButton)
        }
        voiceRequestAnimation = UIImageView(frame: CGRect(x: 0, y: 0, width: img?.size.width ?? 0, height: img?.size.height ?? 0))
        intentLabel = UILabel()
        let locale = ad.responseLang != nil ? ad.responseLang : Locale.preferredLanguages.first
        var buttonLocal = NegativeButtonString
        if locale!.contains("ru") { buttonLocal = NegativeButtonStringRu }
        else if locale!.contains("ja") { buttonLocal = NegativeButtonStringJa }
        negativeView = UIImageView(image: loadImageFrom(base64: buttonLocal))
        buttonLocal = PositiveButtonString
        if locale!.contains("ru") { buttonLocal = PositiveButtonStringRu }
        else if locale!.contains("ja") { buttonLocal = PositiveButtonStringJa }
        positiveView = UIImageView(image: loadImageFrom(base64: buttonLocal))
    }
    
    open func update(ad: AdmanBannerWrapper) {
        self.ad = ad
        if ad.videoAd {
            rewindView.removeFromSuperview()
            remainingTime.removeFromSuperview()
        }
        if ad.isVoiceAd() { initVoiceView() }
        if ad.bannerImage != nil {
            bannerView.image = ad.bannerImage
            addSelector(sel: #selector(bannerTapped(tapGestureRecognizer:)), view: bannerView, target: self)
            bannerView.isHidden = false
            rotateByOrientation()
        } else if ad.videoAd {
            bannerView.isHidden = false
            rotateByOrientation()
        } else {
            bannerView.image = nil
            bannerView.isHidden = true
        }
        closeView.isHidden = true
    }
    
    open func preloadSplashScreen() {}
    
    
    //////#pragma mark common methods

    open func show(rootVC: UIViewController) {
        self.rootVC = rootVC
        
        initBaseView()
        self.rootVC.view.addSubview(view)
        rotateByOrientation()
    }

    open func hide() {
        if !Thread.isMainThread {
            DispatchQueue.main.async {
                self.view.removeFromSuperview()
                self.removeFromParent()
                self.view = nil
            }
        } else {
            view.removeFromSuperview()
            removeFromParent()
            view = nil
        }
        intentLabel = UILabel()
        ad = AdmanBannerWrapper()
    }

    func setTime(time: Int) {
        let v = time < 1 ? 0 : time
        if time > 9 { remainingTime.text = "00:" + String(v) }
        else { remainingTime.text = "00:0" + String(v) }
    }
    
    func showCloseButton() {
        if ad.isVoiceAd() { return }
        closeView.isHidden = false
    }

    func showPauseButton(tapGestureRecognizer: UITapGestureRecognizer?) {
        DispatchQueue.main.async {
            self.playView.isHidden = false
            if let tapGestureRecognizer = tapGestureRecognizer {
                self.playView.removeGestureRecognizer(tapGestureRecognizer)
            } else {
                if self.playView.gestureRecognizers?.first != nil {
                    self.playView.removeGestureRecognizer((self.playView.gestureRecognizers?.first)!)
                }
            }
            if self.ad.videoAd {
                self.playView.image = self.loadImageFrom(base64: VideoPauseButtonString)
            } else {
                self.playView.image = self.loadImageFrom(base64: PauseButtonString) }
            self.addSelector(sel: #selector(self.pauseStateChanged(tapGestureRecognizer:)), view: self.playView, target: self)
        }
    }
    
    open func showPlayButton(tapGestureRecognizer: UITapGestureRecognizer?) {
        DispatchQueue.main.async {
            self.playView.isHidden = false
            if let tapGestureRecognizer = tapGestureRecognizer {
                self.playView.removeGestureRecognizer(tapGestureRecognizer)
            } else {
                if self.playView.gestureRecognizers?.first != nil {
                    self.playView.removeGestureRecognizer((self.playView.gestureRecognizers?.first)!)
                }
            }
            if self.ad.videoAd {
                self.playView.image = self.loadImageFrom(base64: VideoPlayButtonString)
            } else {
                self.playView.image = self.loadImageFrom(base64: PlayButtonString) }
            self.addSelector(sel: #selector(self.playStateChanged(tapGestureRecognizer:)), view: self.playView, target: self)
        }
    }

    @objc func orientationChangedEvent(notify: Notification) {
        rotateByOrientation()
    }
    
    func rotateByOrientation() {
        if UIDevice.current.orientation.isLandscape { rotateHorizontal() }
        else { rotateVertical() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            let player = manager?.getActivePlayer()
            player?.rotationByOrientation(toInterfaceOrientation: UIDevice.current.orientation)
        }
    }
    
    func rotateVertical() {
        let bounds = UIScreen.main.bounds
        let screenWidth = bounds.width
        let screenHeight = bounds.height
        let spacerSize = screenWidth * UIControlsSpacerRatio;
        let screenCenter = CGPoint(x: screenWidth/2, y: screenHeight/2)
        
        if ad.url != nil {
            var side = screenWidth - spacerSize
            if ad.isVoiceAd() { side = screenWidth }
            let centerPoint = CGPoint(x: side / 2, y: side / 2)
            var bannerPoint = CGRect(x: screenCenter.x - centerPoint.x, y: screenCenter.y - centerPoint.y, width: side, height: side)
            if ad.isVoiceAd() { bannerPoint.origin.y -= spacerSize * 2 }
            bannerView.contentMode = .scaleAspectFill
            bannerView.frame = bannerPoint
         }
 
        var mainControlsRect = CGRect()
        mainControlsRect = CGRect(x: screenCenter.x - (playView.frame.size.width ) / 2,
                                  y: screenHeight - ((playView.frame.size.height ) * CGFloat(2)),
                                  width: playView.frame.size.width ,
                                  height: playView.frame.size.height )
                                  playView.frame = mainControlsRect
         
        var rewind = CGRect(x: mainControlsRect.origin.x, y: mainControlsRect.origin.y,
                            width: rewindView.image?.size.width ?? 0, height: rewindView.image?.size.height ?? 0)
        rewind.origin.x -= (rewindView.image?.size.width ?? 0) + (playView.image?.size.width ?? 0) * 1.3
        rewindView.frame = rewind
        if !ad.isVoiceAd() {
            let close = CGRect(x: screenWidth - (closeView.image?.size.width ?? 0) * 1.6,
                               y: statusBarHeight() * 1.1,
                               width: closeView.image?.size.width ?? 0,
                               height: closeView.image?.size.height ?? 0)
            closeView.frame = close
        } else {
             let buttonWidth = screenWidth / 2 - spacerSize * 3
             let x = screenWidth / 2
             // x, y, sizeX, sizeY
             mainControlsRect = CGRect(x: x - spacerSize - buttonWidth, y: screenHeight - spacerSize - buttonWidth / 2.19,
                                       width: buttonWidth, height: buttonWidth / 2.19)
             negativeView.frame = mainControlsRect
             closeView.frame = CGRect(x: screenWidth - spacerSize - 16, y: bannerView.frame.origin.y + spacerSize, width: 16, height: 16)

             let locale = ad.responseLang != nil ? ad.responseLang : Locale.preferredLanguages.first
             if locale!.contains("ru") || locale!.contains("ja") {
                 intentLabel.frame = CGRect(x: screenWidth / 2 - 90, y: negativeView.frame.origin.y - (spacerSize * 2) - 32, width: 190, height: 32)
             } else {
                 intentLabel.frame = CGRect(x: screenWidth / 2 - 70, y: negativeView.frame.origin.y - (spacerSize * 2) - 32, width: 140, height: 32)
             }
             mainControlsRect.origin.x = x + spacerSize;
             positiveView.frame = mainControlsRect
             
             if ad.videoAd {
                 bannerView.frame = bounds
                 visualEffectView?.frame = bounds
                 playView.frame = CGRect(x: screenCenter.x - (playView.image?.size.width ?? 0) * 1.4 / 2,
                                         y: screenHeight - (playView.image?.size.height ?? 0) * 2,
                                         width: (playView.image?.size.width ?? 0) * 1.4,
                                         height: (playView.image?.size.height ?? 0) * 1.4)
                 subButton.frame = CGRect(x: screenWidth - (playView.image?.size.width ?? 0) * 1.4 - 27,
                                          y: screenHeight - (playView.image?.size.height ?? 0) * 2,
                                          width: (playView.image?.size.width ?? 0) * 1.4,
                                          height: (playView.image?.size.height ?? 0) * 1.4)
                 speakNowButton.frame = CGRect(x: screenCenter.x - (speakNowButton.image?.size.width ?? 0) / 2,
                                               y: screenHeight - (speakNowButton.image?.size.height ?? 0) * 2,
                                               width: speakNowButton.image?.size.width ?? 0,
                                               height: speakNowButton.image?.size.height ?? 0)
                 microphoneStatus.frame = CGRect(x: 0, y: 0, width: rootVC.view.frame.size.width, height: statusBarHeight())
             }
         }

         // timer
        if !ad.isVoiceAd() {
            mainControlsRect.origin.x += (playView.image?.size.width ?? 0) * 2
        } else {
            mainControlsRect.origin.y = spacerSize * 2;
            mainControlsRect.origin.x = screenWidth / 2 - 25;
            voiceRequestAnimation.frame = CGRect(x: screenWidth / 2 - voiceRequestAnimation.frame.size.width / 2 + spacerSize / 3,
                                                 y: mainControlsRect.origin.y + spacerSize * 2 + remainingTime.frame.size.height,
                                                  width: voiceRequestAnimation.frame.size.width,
                                                  height: voiceRequestAnimation.frame.size.height)
        }
        mainControlsRect.size = CGSize(width: 100, height: 32)
        remainingTime.frame = mainControlsRect
    }
    
    func rotateHorizontal() {
        //print("ORIENTATION: rotateHorizontal")
        let bounds = UIScreen.main.bounds
        let screenWidth = bounds.width
        let screenHeight = bounds.height
        let spacing = screenHeight * (UIControlsSpacerRatio * 2)

        // banner
        let side = screenHeight - spacing
        var bannerPoint = CGRect(x: 0 + spacing / 2, y: 0 + spacing / 2, width: side, height: side)
        if ad.url != nil {
            if screenHeight / 2 < bannerPoint.size.width {
                let d = (bannerPoint.size.width - screenWidth / 2) / 2
                bannerPoint.size.width -= d
                bannerPoint.size.height = bannerPoint.size.width
                bannerPoint.origin.y += d / 2
            }
            bannerView.frame = bannerPoint
        }

        var control = CGRect()
        if !ad.isVoiceAd() {
            // play/pause
            var xPos = 0
            if ad.url != nil {
                xPos = Int(screenWidth - (screenWidth - (bannerPoint.size.width + spacing)) / 1.8)
            } else { xPos = Int(screenWidth / 2) }
            control = CGRect(x: CGFloat(xPos),
                             y: screenHeight - playView.image!.size.height * 2,
                             width: playView.image!.size.width,
                             height: playView.image!.size.height)
            playView.frame = control

            // back button
            var rewind = CGRect(x: control.origin.x, y: control.origin.y + 4, width: rewindView.image!.size.width, height: rewindView.image!.size.height);
            rewind.origin.x -= (rewindView.image!.size.width + rewindView.image!.size.width * 1.2)
            rewindView.frame = rewind
        }

        // close button
        let r = CGRect(x: screenWidth - closeView.image!.size.width * 1.7,
                       y: statusBarHeight() * 1.1,
                       width: closeView.frame.size.width,
                       height: closeView.frame.size.height)
        closeView.frame = r

        // timer
        if !ad.isVoiceAd() {
            control.origin.x += playView.image!.size.width * 2 }
        control.size = CGSize(width: 100, height: 32)
        remainingTime.frame = control
    }

    ///#pragma mark UI events

    @objc func positiveIntent(tapGestureRecognizer: UITapGestureRecognizer) {
        NotificationAdmanControl.sendEvent(event: .positive)
    }

    @objc func negativeIntent(tapGestureRecognizer: UITapGestureRecognizer) {
        NotificationAdmanControl.sendEvent(event: .negative)
    }

    @objc func bannerTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if ad.isClickable {
            NotificationAdmanControl.sendEvent(event: .click)
        }
    }

    @objc func playStateChanged(tapGestureRecognizer: UITapGestureRecognizer) {
        NotificationAdmanControl.sendEvent(event: .start)
        showPauseButton(tapGestureRecognizer: tapGestureRecognizer)
    }
    
    @objc func subStateChanged(tapGestureRecognizer: UITapGestureRecognizer) {
        isSubOn = !isSubOn
        subButton.image = isSubOn ? loadImageFrom(base64: SubtitlesButtonSelectedString) : loadImageFrom(base64: SubtitlesButtonUnselectedString)
        NotificationAdmanControl.sendEvent(event: isSubOn ? .subShow : .subHide)
        let player = manager?.getActivePlayer()
        player?.isSubtitles = isSubOn
    }

    @objc func pauseStateChanged(tapGestureRecognizer: UITapGestureRecognizer) {
        NotificationAdmanControl.sendEvent(event: .pause)
        showPlayButton(tapGestureRecognizer: tapGestureRecognizer)
    }

                                    
    @objc func playbackRewindEvent(tapGestureRecognizer: UITapGestureRecognizer) {
        NotificationAdmanControl.sendEvent(event: .resume)
    }

    @objc func onSwipeBack(sender: Any) {
        hide()
        NotificationAdmanControl.sendEvent(event: .swipeBack)
    }

    @objc func onClose(tapGestureRecognizer: UITapGestureRecognizer) {
        if ad.isVoiceAd() {
            setVisible(visibility: false, listView: [closeView, bannerView])
        } else {
            NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: UIDevice.current)
            NotificationAdmanControl.sendEvent(event: .skip)
            hide()
        }
    }

    @objc func admanPlayerNotification(notification: NSNotification) {
        let dict = notification.userInfo
        let message = dict?[NOTIFICATION_ADMAN_PLAYER_KEY] as? NotificationAdmanPlayer
        guard let message = message else { return }
        switch message.event {
            
        case .loadImage:
            DispatchQueue.main.async {
                let player = message.userData as! AdmanPlayer
                if UIDevice.current.orientation.isPortrait {
                    self.bannerView.contentMode = .scaleAspectFill
                    self.bannerView.image = player.introImageP
                }
                else {
                    self.bannerView.image = player.introImageL }
                self.clearBlurEffectWithCompletion {}
            }
        case .playing:
            print(TAG_UI + " triggerActionAdmanPlayer: " +
                  NotificationAdmanPlayer.getEventString(event: message.event!))
            DispatchQueue.main.async { [self] in
                let mgr = message.userData as! Adman
                manager = mgr
                if videoLayer == nil {
                    videoLayer = AVPlayerLayer(player: mgr.getActivePlayer())
                }
                isSubPresent = mgr.subtitlesPresent()
                subButton.isHidden = !isSubPresent
                subButton.image = isSubOn ? loadImageFrom(base64: SubtitlesButtonSelectedString) : loadImageFrom(base64: SubtitlesButtonUnselectedString)
            
                update(ad: mgr.getActiveAd() as! AdmanBannerWrapper)
                if ad.videoAd {
                    videoLayer = AVPlayerLayer(player: mgr.getActivePlayer())
                    videoLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    videoLayer?.frame = bannerView.bounds
                    bannerView.layer.addSublayer(videoLayer ?? AVPlayerLayer())
                }
                playView.isHidden = false
                //subButton.isHidden = false
                view.sendSubviewToBack(bannerView)
                view.bringSubviewToFront(playView)
                view.bringSubviewToFront(subButton)
                view.bringSubviewToFront(speakNowButton)
                showPauseButton(tapGestureRecognizer: nil)
                setVisible(visibility: false, listView: [negativeView, positiveView, intentLabel, microphoneStatus, voiceRequestAnimation])
            }
            if let ad = message.userData as? AdmanBannerWrapper {
                DispatchQueue.main.async { [self] in
                    update(ad: ad)
                    showPauseButton(tapGestureRecognizer: nil)
                    setVisible(visibility: false, listView: [negativeView, positiveView, intentLabel, microphoneStatus, voiceRequestAnimation])
                }
            }
        case .play:
            showPauseButton(tapGestureRecognizer: nil)
        case .ready:
            if let mgr = message.userData as? Adman {
                if mgr.state == .adChangedForPlayback {
                    bannerView.image = nil
                }
            }
        case .pause:
            showPlayButton(tapGestureRecognizer: nil)
        case .progress:
            if let mgr = message.userData as? Adman {
                if mgr.estimatedTime() < 4 && closeView.isHidden { showCloseButton() }
                if mgr.estimatedTime() == 1 && (mgr.getActiveAd() as! AdmanBannerWrapper).videoAd && isBlurEnabled() {
                    setBlurEffect()
                } else if mgr.estimatedTime() == 0 && (mgr.getActiveAd() as! AdmanBannerWrapper).videoAd {
                    if isBlurEnabled() {
                        setBlurEffect()
                    }
                    playView.isHidden = true
                    subButton.isHidden = true
                }
                
                setTime(time: mgr.estimatedTime())
                if mgr.estimatedTime() < 4 { self.showCloseButton() }
            }
        case .complete:
            videoLayer?.removeFromSuperlayer()
            videoLayer = nil
            bannerView.image = nil
            visualEffectView?.removeFromSuperview()
            visualEffectView = nil
            videoFrame?.removeFromSuperview()
            videoBlurFrame?.removeFromSuperview()
            videoFrame = nil
            videoBlurFrame = nil
            hide()
        case .waitingToPlayAtSpecifiedRate:
            DispatchQueue.main.async { [self] in
                if let mgr = message.userData as? Adman {
                    activityIndicator?.isHidden = mgr.playerBufferState == 0 ? true : false
                    view.bringSubviewToFront(activityIndicator ?? UIActivityIndicatorView())
                    if mgr.playerBufferState == 1 ? true : false {
                        activityIndicator?.startAnimating()
                    } else {
                        activityIndicator?.stopAnimating()
                    }
                }
            }
        default:
            break
        }
    }

    @objc open func admanVoiceNotification(notification: NSNotification) {
        let dict = notification.userInfo;
        let message = dict?[NOTIFICATION_ADMAN_VOICE_KEY] as? NotificationAdmanVoice
        guard let message = message else { return }

        print(TAG_UI + " triggerActionAdmanVoice: " + NotificationAdmanVoice.getEventString(event: message.event!))
        switch message.event {
        case .responsePlaying, .error:
            DispatchQueue.main.async { [self] in
                if let mgr = message.userData as? Adman {
                    isSubPresent = mgr.subtitlesPresent()
                    subButton.isHidden = !isSubPresent
                    subButton.image = isSubPresent ? loadImageFrom(base64: SubtitlesButtonSelectedString) : loadImageFrom(base64: SubtitlesButtonUnselectedString)
                }
                voiceRequestAnimation.isHidden = true
            }
        case .interactionStarted, .interactionEnded:
            break
        case .recognizerStarted:
            let player = manager?.getActivePlayer()
            bannerView.clipsToBounds = true
            bannerView.contentMode = .scaleAspectFill
            if UIDevice.current.orientation.isPortrait,
                let outroImageP = player?.outroImageP {
                bannerView.image = outroImageP
            } else if let outroImageL = player?.outroImageL {
                bannerView.image = outroImageL
            }
            DispatchQueue.main.async { [self] in
                playView.isHidden = true
                subButton.isHidden = true
                rewindView.isHidden = true
                remainingTime.isHidden = true
                view.sendSubviewToBack(bannerView)
                view.sendSubviewToBack(visualEffectView ?? UIVisualEffectView())
                setVisible(visibility: true, listView: [intentLabel, positiveView, negativeView, microphoneStatus, speakNowButton])
            }
        case .recognizerStopped:
            DispatchQueue.main.async { [self] in
                ///////////////////CHANGE video_ad
                playView.isHidden = false
                subButton.isHidden = true
                rewindView.isHidden = false
                remainingTime.isHidden = false
                setVisible(visibility: false, listView: [intentLabel, positiveView, negativeView, microphoneStatus, speakNowButton])
            }
        default:
                break
        }
    }

    @objc func admanControlNotification(notification: NSNotification) {
        let dict = notification.userInfo
        let message = dict![NOTIFICATION_ADMAN_CONTROL_KEY] as? NotificationAdmanControl
        if message == nil { return }
        switch message?.event {
        case .link: do {
            let svc: SFSafariViewController = SFSafariViewController(url: message!.userData as! URL)
            rootVC.present(svc, animated: true, completion: nil)
        }
        default:
            return
        }
    }
    
    func statusBarHeight() -> CGFloat {
        return UIApplication.shared.connectedScenes
            .filter {$0.activationState == .foregroundActive }
            .map {$0 as? UIWindowScene }
            .compactMap { $0 }
            .first?.windows
            .filter({ $0.isKeyWindow }).first?
            .windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    }
    
    @objc func appEnterForeground(note: Notification) {
        if videoLayer == nil {
            videoLayer = AVPlayerLayer(player: manager?.getActivePlayer())
        }
        if let ad = manager?.getActiveAd() as? AdmanBannerWrapper {
            update(ad: ad)
        }
        if ad.videoAd {
            videoLayer?.player = manager?.getActivePlayer()
            videoLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoLayer?.frame = bannerView.bounds
            if let videoLayer = videoLayer {
                bannerView.layer.addSublayer(videoLayer)
            }
        }
    }

    @objc func appEnterBackground(note: Notification) {
        videoLayer?.removeFromSuperlayer()
        videoLayer = nil
    }
    
    func clearBlurEffectWithCompletion(completion: @escaping ()->()) {
        UIView.transition(with: view, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve) {
            self.visualEffectView?.removeFromSuperview()
        } completion: { finished in
            completion()
        }
    }
    
    func setBlurEffect() {
        let player = manager?.getActivePlayer()
        if UIDevice.current.orientation.isPortrait { bannerView.image = player?.outroImageP }
        else { bannerView.image = player?.outroImageL }
        visualEffectView?.removeFromSuperview()
        visualEffectView = nil
        
        let blurEffect = UIBlurEffect(style: .light)
        visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView?.frame = UIScreen.main.bounds
        visualEffectView?.autoresizingMask = UIView.AutoresizingMask(rawValue:( UIView.AutoresizingMask.flexibleHeight.rawValue | UIView.AutoresizingMask.flexibleWidth.rawValue))
        
        UIView.transition(with: view, duration: 0.6, options: UIView.AnimationOptions.transitionCrossDissolve) {
            self.bannerView.addSubview(self.visualEffectView ?? UIView())
        } completion: { finished in
            self.isBlur = true
        }
    }
}
/*
extension UIWindow {
    static var isLandscape: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows
                .first?
                .windowScene?
                .interfaceOrientation
                .isLandscape ?? false
        } else {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }
}
*/
extension UIImage {

    convenience init?(contentsOfFile name: String, ofType: UIImageType) {
        guard let bundlePath = Bundle.main.path(forResource: name, ofType: ofType.rawValue) else {
            return nil
        }
        self.init(contentsOfFile: bundlePath)!
    }

}

enum UIImageType: String {
    case PNG = "png"
    case JPG = "jpg"
    case JPEG = "jpeg"
}
