//
//  NotificationAdmanControl.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

struct NotificationAdmanPlayer {
    
    var event: AdmanPlayerEventType?
    var userData: Any?

    init(event: AdmanPlayerEventType, userData: Any?) {
        self.event = event
        self.userData = userData
    }
    
    init() {}
    
    static func sendEvent(event: AdmanPlayerEventType) {
        sendEvent(event: event, userData: nil)
    }
    
    static func sendEvent(event: AdmanPlayerEventType, userData: Any?) {
        let message = NotificationAdmanPlayer(event: event, userData: userData)
        var dict = [String: Any]()
        dict[NOTIFICATION_ADMAN_PLAYER_KEY] = message
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_ADMAN_PLAYER), object: nil, userInfo: dict)
    }
    
    static func getEventString(event: AdmanPlayerEventType) -> String {
        switch (event) {
        case .prepare:
            return "AdmanPlayerEventPrepare (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .ready:
            return "AdmanPlayerEventReady (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .failed:
            return "AdmanPlayerEventFailed (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .playing:
            return "AdmanPlayerEventPlaying (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .play:
            return "AdmanPlayerEventPlay (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .pause:
            return "AdmanPlayerEventPause (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .progress:
            return "AdmanPlayerEventProgress (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .complete:
            return "AdmanPlayerEventComplete (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .closeable:
            return "AdmanPlayerEventCloseable (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .skippable:
            return "AdmanPlayerEventSkippable (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .waitingToPlayAtSpecifiedRate:
            return "AdmanPlayerEventWaitingToPlayAtSpecifiedRate (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .loadImage:
            return "AdmanPlayerEventLoadImage (" + String(AdmanPlayerEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        }
    }
}
