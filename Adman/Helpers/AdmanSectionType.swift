//
//  AdmanSectionType.swift
//  Adman_swift
//
//  Created by Barnet Birday on 20/01/2022.
//

import Foundation

enum AdmanSectionType: Int {
    case sPreroll = 0,
         sMidroll,
         sPostroll
}
