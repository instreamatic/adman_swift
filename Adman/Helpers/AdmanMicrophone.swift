//
//  AdmanMicrophone.swift
//  Adman_swift
//
//  Created by Barnet Birday on 05/02/2022.
//

import Foundation
import AVKit


struct AdmanMicrophone {
    func isMicrophoneEnabled() -> Bool {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .denied, .undetermined:
            return false
        default: return true
        }
    }
    
    func isMicrophoneEnabledString() -> String {
        if isMicrophoneEnabled() { return "1" }
        return "0"
    }
}
