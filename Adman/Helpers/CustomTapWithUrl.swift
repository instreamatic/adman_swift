//
//  CustomTapWithUrl.swift
//  Adman_swift
//
//  Created by Barnet Birday on 18/02/2022.
//

import Foundation
import UIKit

class CustomTapWithUrl: UITapGestureRecognizer {
  var url: String?
}
