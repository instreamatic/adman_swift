//
//  AdmanUIExtension.swift
//  TestApp_VoiceAd
//
//  Created by Barnet Birday on 10/03/2022.
//

import Foundation
import Adman_swift
import UIKit

protocol AdmanUIExtensionDelegate: NSObject {
    func refresh()
}

class AdmanUIExtension: AdmanUIBase {
    
    var delegate: AdmanUIExtensionDelegate?
    var devMode: Bool?
    
    let REFRESH_BTN_SOURCE = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA/1BMVEUAAAD//////Pz//f3//Pz//Pz//Pz//Pz/+/v//Pz//Pz/+/v//////////f3//Pz//Pz/+/v//////f3//Pz//Pz//f3//////Pz//////Pz//Pz//////////Pz/+vr//////Pz//Pz/////+/v//Pz/+/v//Pz/+vr/+vr//Pz/+/v//Pz//Pz//Pz/+/v/+/v//f3//Pz//Pz//Pz//f3//Pz//Pz//////Pz//Pz//Pz//Pz//f3/+/v//Pz//Pz//////////f3//Pz//Pz//f3//Pz/+/v//Pz//Pz//////Pz/////////+fn//f3/+/v//Pz//PwAAACwjZ4rAAAAU3RSTlMABk94n8Xs89rBlUUFCW7k75AVdujyfhpKC7fDFxv+OBTJVx3YxINbMzZjkchWvjxGzlX8YWzgVA/n+VJTZkD12wESe1HG1FBIu/0qTRwpLmc+uiOLZd0AAAABYktHRACIBR1IAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4goZDyQa9y9PfAAAAWZJREFUOMvNU9eCgjAQxIK9YO+9gKKeDXvvXe/y//9ySSAYkHu/eWF2Z9gkmyzDqDCZLVbWZnc4XW4P8wmvzw9UcIGgTvaEwkCDSBRWicWJnkiCD6TSGS6bk/V8ARjAzwJQxHqprKQqVV6o1cXG29TEhpYcJL/Iiu0OMXRRyGPa61MHlgaKYQij0RixyZQ6k6guMVOjOX3oxZIY0G8rRAprTVs227Fs2MFgjzYg6Btr6h+Q4YjqRQCQDHrPnM7gckUkeLszxng8mf+C5+MP4X7Dz+Z6AeeTkS7BZ7OA3yNqyKFv0utCD+b3kOzkpo63G42+xo9oBdmUXMxyQRvmOCdCNlPvVqT04QSXHSGqyAOJ2sbrgnM84l1SoNMm8vePnGnhqPl+og2xXhP4akUJyyVsKALAUkP1RiEv18tluUw69aknE2TFeAyOXjSilcMh/QAHAxw1VD6vQe89bpfTYbexVouZOvAvsbqBlmHbnCwAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMTAtMjVUMTU6MzY6MjYrMDI6MDCXGLG3AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTEwLTI1VDE1OjM2OjI2KzAyOjAw5kUJCwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII="

    override init() {
        super.init()
        if UserDefaults.standard.bool(forKey: "debug") == true { devMode = true }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initBaseView() {
        super.initBaseView()
        rewindView.image = loadImageFrom(base64: REFRESH_BTN_SOURCE)
        for rec in (rewindView.gestureRecognizers ?? [UIGestureRecognizer]()) {
            rewindView.removeGestureRecognizer(rec)
        }
        addSelector(sel: #selector(refreshDelegate(uiGestureRecognizer:)), view: rewindView, target: self)
    }
    
    override func initVoiceView() {
        super.initVoiceView()
        positiveView.image = nil
        negativeView.image = nil
        intentLabel.text = ""
    }
    
    override func show(rootVC: UIViewController) {
        super.show(rootVC: rootVC)
        if devMode ?? false {
            remainingTime.isHidden = true
            view.backgroundColor = nil
        }
    }
    
    override func update(ad: AdmanBannerWrapper) {
        super.update(ad: ad)
        if devMode ?? false { bannerView.image = nil }
    }

    @objc func refreshDelegate(uiGestureRecognizer: UIGestureRecognizer) {
        delegate?.refresh()
    }
    
    @objc override func admanVoiceNotification(notification: NSNotification) {
        super.admanVoiceNotification(notification: notification)
        let dict = notification.userInfo
        guard let message = dict?[NOTIFICATION_ADMAN_VOICE_KEY] as? NotificationAdmanVoice else { return }
        switch message.event {
        case .recognizerStarted: break
            default: break
        }
    }
}
