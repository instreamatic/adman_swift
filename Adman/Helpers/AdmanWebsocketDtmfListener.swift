//
//  AdmanWebsocketDtmfListener.swift
//  Adman_swift
//
//  Created by Barnet Birday on 19/01/2022.
//

import Foundation

class AdmanWebsocketDtmfListener: NSObject, URLSessionWebSocketDelegate, URLSessionTaskDelegate {
    let urlSession = URLSession(configuration: .default)
    var connection: URLSessionWebSocketTask?
    var delegate: AdmanDtmfDelegate?
    
    func start(station_key: String) {
        connection = urlSession.webSocketTask(with:
              URL(string: AdmanDtmfWebsocketDomain + "/" + station_key)!)
        connection?.resume()
        readMessage()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) { [self] in
            if connection != nil && (connection?.state != .running && connection?.state != .completed) {
                print("Failed to open websocket")
                connection?.cancel()
                connection = nil
                start(station_key: station_key)
            }
        }
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(pingConnection), userInfo: nil, repeats: true)
    }
    
    func readMessage()  {
        connection?.receive { [self] result in
            readMessage()
            switch result {
            case .failure(_):
                break
            case .success(let message):
                switch message {
                case .string(let text):
                    self.receivedMessage(message: text)
                case .data(_):
                    break
                @unknown default:
                    fatalError()
                }
            }
            if connection?.state == .canceling {}
        }
    }
    
    func receivedMessage(message: String) {
        print("libAdman: dtmf recived: " + message)
        if let data = message.data(using: .utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                    if json?["label"] as! String == "IN" { delegate?.dtmfIn() }
                    else if json?["label"] as! String == "OUT" { delegate?.dtmfOut() }
                } catch {
                    print("Something went wrong")
                }
            }
    }
    
    @objc func pingConnection() {
        if connection != nil { return }
        if connection?.state == .running || connection?.state == .completed {
            connection?.sendPing(pongReceiveHandler: { error in })
        }
    }
    
    func stop() {
        connection?.cancel()
        connection = nil
    }
}
