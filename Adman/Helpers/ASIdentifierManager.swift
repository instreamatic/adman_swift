//
//  ASIdentifierManager.swift
//  Adman_swift
//
//  Created by Barnet Birday on 03/02/2022.
//

import Foundation

struct ASIdentifierManager {
    
    var advertisingIdentifier: NSUUID
    var advertisingTrackingEnabled = false

    //- (void)clearAdvertisingIdentifier API_UNAVAILABLE(ios, macos, tvos);
}
