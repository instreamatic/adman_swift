//
//  NotificationAdmanControl.swift
//  Adman_swift
//
//  Created by Barnet Birday on 28/01/2022.
//

import Foundation

public struct NotificationAdmanVoice {
    
    public var event: AdmanVoiceEventType?
    public var userData: Any?
    
    init(event: AdmanVoiceEventType) {
        self.event = event
    }
    
    init(event: AdmanVoiceEventType, userData: Any?) {
        self.event = event
        self.userData = userData
    }
    
    init() {}
    
    static func sendEvent(event: AdmanVoiceEventType) {
        sendEvent(event: event, userData: nil)
    }
    
    static func sendEvent(event: AdmanVoiceEventType, userData: Any?) {
        let message = NotificationAdmanVoice(event: event, userData: userData)
        var dict = [String: Any]()
        dict[NOTIFICATION_ADMAN_VOICE_KEY] = message
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_ADMAN_VOICE), object: nil, userInfo: dict)
    }
    
    static func getEventString(event: AdmanVoiceEventType) -> String {
        switch (event) {
        case .responsePlaying:
            return "AdmanVoiceEventResponsePlaying (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .interactionStarted:
            return "AdmanVoiceEventInteractionStarted (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .interactionEnded:
            return "AdmanVoiceEventInteractionEnded (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .error:
            return "AdmanVoiceEventError (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .recognizerStarted:
            return "AdmanVoiceEventRecognizerStarted (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        case .recognizerStopped:
            return "AdmanVoiceEventRecognizerStopped (" + String(AdmanMessageEventType(rawValue: event.rawValue)?.rawValue ?? 0) + ")"
        }
    }
}
