//
//  VACRecognizerDelegate.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

protocol VACRecognizerDelegate {
    func recognizerDidChangeState(recognizer: VACRecognizer, state: VACRecognizerState)
    func recognizerDidRecognizeCommand(recognizer: VACRecognizer, command: [String: String]?)
    func recognizerDidSessionEnded(recognizer: VACRecognizer, result: [String: Any]?)
    func recognizerDidFailWithError(recognizer: VACRecognizer, error: Error)
    func recognizerDidConnectionOpened(recognizer: VACRecognizer, state: VACRecognizerState)
}
