//
//  AdmanInteraction.swift
//  Adman_swift
//
//  Created by Barnet Birday on 02/02/2022.
//

import Foundation

class AdmanInteraction {
    var priority = 0
    var action: String
    var type: String
    var values = [AdmanResponseValue]()
    var keywords = [String]()
    
    init(action: String, type: String) {
        self.action = action
        self.type = type
    }
}
